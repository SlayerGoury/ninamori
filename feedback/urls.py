from django.conf.urls import include, url
import views


urlpatterns = [
	url(r'^$', views.index, name='feedback index'),
	url(r'send$', views.send, name='feedback send'),
]
