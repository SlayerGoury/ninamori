# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.conf import settings
from django.urls import reverse
from mailqueue.models import MailerMessage
from django.shortcuts import render
from django.template.loader import render_to_string
from django.utils.translation import ugettext as _
from models import Channel
from ninamori.utils import validate_email, require_POST
from textwrap import wrap
from utils import get_feedback_channels


def index (request):
	if request.user.is_authenticated():
		email = request.user.email
	else:
		email = ''
	return render(request, 'feedback/index.html', {
		'app_send': reverse(send),
		'channels': get_feedback_channels(request.LANGUAGE_CODE),
		'email': email,
	})


@require_POST
def send (request):
	if not request.POST.get('message'):
		# TODO too dirty, form error handler must be here instead
		return render(request, 'feedback/index.html', {'message': _('Message should not be blank')}, status=400)
	if not (request.POST.get('email') and validate_email(request.POST.get('email'))):
		return render(request, 'feedback/index.html', {'message': _('I don´t like this email')}, status=400)

	if request.POST.get('channel') and Channel.objects.filter(id=request.POST.get('channel')):
		channel = Channel.objects.get(id=request.POST.get('channel'))
		support_email = channel.support_email
		title = channel.localized_title(request.LANGUAGE_CODE)
	else:
		support_email = settings.ADMINS[0][1]
		title = _('channel is not specified')

	if not request.META.get('HTTP_USER_AGENT'):
		request.META['HTTP_USER_AGENT'] = 'blank agent'
	message_data = {
		'site_name': settings.SITE_NAME[request.LANGUAGE_CODE],
		'sender_email': request.POST.get('email'),
		'channel': title,
		'message': '\n'.join(wrap(request.POST.get('message'))),
		'ip_address': request.META.get('REMOTE_ADDR'),
		'language_code': request.LANGUAGE_CODE,
		'user_agent': '\n'.join(wrap(request.META.get('HTTP_USER_AGENT'), width=800)),
		'is_duplicate_requested': request.POST.get('duplicate_to_email'),
	}

	recipient_email = request.POST.get('email')

	if request.user.is_authenticated():
		message_data['username'] = request.user.username
		message_data['userlink'] = reverse('admin:auth_user_change', args=(request.user.id,))
		message_data['userprofilelink'] = reverse('admin:accounts_userprofile_change', args=(request.user.userprofile.id,))
		message_data['protocol'] = settings.PROTOCOL_SCHEME
		message_data['site_domain'] = settings.SITE_DOMAIN
		message_data['email_valid'] = request.user.userprofile.validation_email_valid
		recipient_email = request.user.email

	template = 'feedback/support_email.txt'
	email_message = MailerMessage()
	email_message.subject       = '[' + settings.SITE_NAME[request.LANGUAGE_CODE] + '] ' + title
	email_message.content       = render_to_string (template, message_data)
	email_message.from_address  = settings.DEFAULT_FROM_EMAIL
	email_message.to_address    = support_email
	email_message.reply_to      = request.POST.get('email')
	email_message.app           = 'feedback'
	email_message.save()

	if request.POST.get('duplicate_to_email'):
		template = 'feedback/send_copy_to_email.txt'
		email_message = MailerMessage()
		email_message.subject       = '[' + settings.SITE_NAME[request.LANGUAGE_CODE] + '] ' + _('requested feedback copy')
		email_message.content       = render_to_string (template, message_data)
		email_message.from_address  = settings.DEFAULT_FROM_EMAIL
		email_message.to_address    = recipient_email
		email_message.app           = 'feedback copy'
		email_message.save()

	return render (request, 'feedback/confirmation.html', {'message': request.POST.get('message')})
