from django.conf import settings
from django.urls import reverse
from django.middleware.csrf import get_token
from django.template.loader import render_to_string
from models import Channel


def get_feedback_channels (language_code):
	feedback_channels = Channel.objects.all()
	channels = []
	for channel in feedback_channels:
		channels.append({
			'id': channel.id,
			'title': channel.localized_title(language_code),
		})
	return channels


def get_feedback_form_html (request):
	if request.user.is_authenticated():
		email = request.user.email
	else:
		email = ''
	return render_to_string("feedback/form.html", {
			'csrf_token': get_token(request),
			'app_send': reverse('feedback send'),
			'channels': get_feedback_channels(request.LANGUAGE_CODE),
			'email': email,
		})
