# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.auth.models import AnonymousUser, User
from django.http import HttpRequest
from django.test import TestCase, RequestFactory
from models import Channel
import utils, views


class FeedbackTests (TestCase):
	def setUp (self):
		self.factory = RequestFactory()
		self.user = User.objects.create_user(username='meow', email='m@e.ow', password='purr')

	def test_models_channel (self):
		Channel.objects.create(
			title_en = 'meow',
			title_ru = 'мяу',
			support_email = 'a@dm.in',
		)
		channel = Channel.objects.get(support_email='a@dm.in')
		self.assertEqual(channel.localized_title('en'), 'meow')
		self.assertEqual(channel.localized_title('ru'), 'мяу')
		channel.title_ru = ''
		channel.save()
		channel = Channel.objects.get(support_email='a@dm.in')
		self.assertEqual(channel.localized_title('ru'), 'meow')
		channel.title_en = ''
		channel.save()
		channel = Channel.objects.get(support_email='a@dm.in')
		self.assertEqual(channel.localized_title('en'), '')
		self.assertEqual(channel.localized_title('ru'), '')


	def test_utils_get_feedback_channels (self):
		Channel.objects.create(title_en = 'meow_1', support_email = 'a@dm.in')
		Channel.objects.create(title_en = 'meow_2', support_email = 'a@dm.in')
		Channel.objects.create(title_en = 'meow_3', support_email = 'a@dm.in')
		channels = utils.get_feedback_channels('en')
		titles = [channel['title'] for channel in channels]
		self.assertIn('meow_1', titles)
		self.assertIn('meow_2', titles)
		self.assertIn('meow_3', titles)

	def test_get_feedback_form_html (self):
		Channel.objects.create(title_en = 'meow', support_email = 'a@dm.in')
		request = self.factory.get('/')
		request.user = AnonymousUser()
		request.LANGUAGE_CODE = 'en'
		self.assertIn('<form ', utils.get_feedback_form_html(request))
		request.user = self.user
		self.assertIn('<form ', utils.get_feedback_form_html(request))

	def test_views_index (self):
		request = self.factory.get('/')
		request.user = AnonymousUser()
		request.LANGUAGE_CODE = 'en'
		response = views.index(request)
		self.assertEqual(response.status_code, 200)
		request.user = self.user
		response = views.index(request)
		self.assertEqual(response.status_code, 200)

	def test_receive (self):
		request = self.factory.get('/')
		request.user = AnonymousUser()
		request.LANGUAGE_CODE = 'en'
		response = views.send(request)
		self.assertEqual(response.status_code, 405)
		request.method = 'POST'
		request.POST = {'csrfmiddlewaretoken': 'meow'}
		response = views.send(request)
		self.assertEqual(response.status_code, 400)
		request.POST = {'csrfmiddlewaretoken': 'meow', 'message': 'purr', 'channel': '1', 'duplicate_to_email': 'yes', 'email': 'm.e.o.w'}
		response = views.send(request)
		self.assertEqual(response.status_code, 400)
		request.POST = {'csrfmiddlewaretoken': 'meow', 'message': 'purr', 'channel': '1', 'duplicate_to_email': 'yes', 'email': 'm@e.ow'}
		response = views.send(request)
		self.assertEqual(response.status_code, 200)
		Channel.objects.create(title_en = 'meow', support_email = 'a@dm.in')
		response = views.send(request)
		self.assertEqual(response.status_code, 200)
		request.user = self.user
		response = views.send(request)
		self.assertEqual(response.status_code, 200)
