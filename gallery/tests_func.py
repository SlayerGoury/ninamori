# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.conf import settings
from django.core.files import File
from django.contrib.auth.models import Permission, User, AnonymousUser, Group
from django.test import TestCase
import os
from shutil import copy2
from wand.image import Image
from models import OverwriteStorage, Album, Picture
import utils, views


class galleryTests (TestCase):
	def setUp (self):
		if os.path.isfile( os.path.join(settings.STATICFILES_DIRS[0], 'test_image.png') ):
			os.remove( os.path.join(settings.STATICFILES_DIRS[0], 'test_image.png') )
		if os.path.isfile( os.path.join(settings.MEDIA_ROOT, 'gallery/test_album/test.png') ):
			os.remove( os.path.join(settings.MEDIA_ROOT, 'gallery/test_album/test.png') )
		if os.path.isfile( os.path.join(settings.MEDIA_ROOT, 'gallery/_small/test_album/test.png.jpg') ):
			os.remove( os.path.join(settings.MEDIA_ROOT, 'gallery/_small/test_album/test.png.jpg') )
		if os.path.isfile( os.path.join(settings.MEDIA_ROOT, 'gallery/_thumbs/test_album/test.png.jpg') ):
			os.remove( os.path.join(settings.MEDIA_ROOT, 'gallery/_thumbs/test_album/test.png.jpg') )
		if os.path.isfile( os.path.join(settings.MEDIA_ROOT, 'gallery/_micros/test_album/test.png.jpg') ):
			os.remove( os.path.join(settings.MEDIA_ROOT, 'gallery/_micros/test_album/test.png.jpg') )

		self.user = User.objects.create_user(username='meow1', email='1m@e.ow', password='purr')
		self.album = Album.objects.create(owner=self.user, alias='test_album')

	def test_storage_overwriting (self):
		pic = Picture.objects.create(album=self.album)
		img = os.path.join(settings.STATICFILES_DIRS[0], 'img/icon.png')
		copy2 (img, os.path.join(settings.MEDIA_ROOT, 'gallery/_micros/test_album/test.png.jpg'))
		with open(img, 'r') as file:
			file = File(file)
			file.name='test.png'
			pic.picture = file
			pic.save()
		utils.make_smaller_ones(pic)
		pic.delete()
		self.assertFalse( os.path.isfile( os.path.join(settings.MEDIA_ROOT, 'gallery/_micros/test_album/test.png.jpg') ) )  # files was deleted

	def test_big_image (self):
		# This test is required for coverage
		temp = settings.GALLERY_PICSETTINGS['limit']
		settings.GALLERY_PICSETTINGS['limit'] = (1000, 1000)
		with Image(width=1001, height=1001) as img:
			img.save(filename=os.path.join(settings.STATICFILES_DIRS[0], 'test_image.png'))

		pic = Picture.objects.create(album=self.album)
		img = os.path.join(settings.STATICFILES_DIRS[0], 'test_image.png')
		with open(img, 'r') as file:
			file = File(file)
			file.name='test.png'
			pic.picture = file
			pic.save()
		utils.make_smaller_ones(pic)
		pic.delete()

		os.remove( os.path.join(settings.STATICFILES_DIRS[0], 'test_image.png') )
		settings.GALLERY_PICSETTINGS['limit'] = temp
