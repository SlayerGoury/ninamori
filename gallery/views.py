# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.urls import reverse
from django.shortcuts import render, redirect, get_object_or_404
from ninamori.utils import require_POST
from models import Album, Picture


# TODO do cache this
def index (request):
	albums = []
	for album in Album.objects.all():
		if album.published:
			albums.append(album)
	return render(request, 'gallery/index.html', {'albums': albums})


# TODO cache this one aswell
# TODO order should be custamizable or selectable
# TODO paginations is required
def album_by_alias (request, alias):
	album = get_object_or_404(Album, alias=alias)
	pictures = Picture.objects.filter(album=album).order_by('date_uploaded', 'uuid')
	buttons = {}
	if album.owner == request.user or request.user.is_staff or request.user.has_perm('gallery.change_album'):
		buttons['gallery_album'] = True
	return render(request, 'gallery/album.html', {'album': album, 'pictures': pictures, 'buttons': buttons})


# TODO make real filmstrip
def image_by_uuid (request, alias, uuid):
	picture = get_object_or_404(Picture, uuid=uuid)

	try: picture_prev = Picture.get_previous_by_date_uploaded(picture)
	except Picture.DoesNotExist: picture_prev = None

	try: picture_next = Picture.get_next_by_date_uploaded(picture)
	except Picture.DoesNotExist: picture_next = None

	return render(request, 'gallery/picture.html', {
		'picture': picture,
		'picture_prev': picture_prev,
		'picture_next': picture_next,
		'enlarged': request.GET.get('enlarge')
	})


@require_POST
def delete (request):
	uuid = request.POST.get('uuid')
	picture = get_object_or_404(Picture, uuid=uuid)
	if picture.album.owner == request.user or request.user.is_staff or request.user.has_perm('gallery.change_album'):
		if not request.POST.get('confirm'):
			return render(request, 'gallery/picture_delete.html', {'picture': picture})
		else:
			alias = picture.album.alias
			picture.delete()
			return redirect(reverse(album_by_alias, kwargs={'alias': alias}))
