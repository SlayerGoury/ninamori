# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.auth.models import User, Group
from django.core.files.storage import FileSystemStorage
from django.db import models
from django.db.models.signals import pre_delete, post_save
from django.dispatch import receiver
from django.utils.html import format_html
from jsonfield import JSONField
import os
import uuid


class OverwriteStorage(FileSystemStorage):
	def _save(self, name, content):
		if self.exists(name):
			self.delete(name)
		return super(OverwriteStorage, self)._save(name, content)

	def get_available_name(self, name, max_length=None):
		return name


class Album (models.Model):
	owner                        = models.ForeignKey(User, on_delete=models.CASCADE)
	alias                        = models.CharField(max_length=254, unique=True, blank=False)
	allowed_groups               = models.ManyToManyField(Group, db_table='gallery_album_allowed_groups', blank=True)
	published                    = models.BooleanField(default=False)

# Not aplicable because of Nginx way
#	security_mode                = models.CharField(max_length=2, choices=(
#		('PB', 'Public'),
#		('GR', 'Groups only'),
#		('PW', 'Password protected'),
#		('PR', 'Private'),
#	), default='PR')

	upload_permissions           = models.CharField(max_length=2, choices=(
		('PB', 'Public'),
		('GR', 'Groups only'),
		('PW', 'Password protected'),
		('PR', 'Private'),
	), default='PR')

	def __unicode__ (self):
		return self.alias


def get_path_for_picture (instance, filename):
	return 'gallery/' + instance.album.alias + '/' + filename


def get_path_for_small (instance, filename):
	return 'gallery/_small/' + instance.album.alias + '/' + filename


def get_path_for_thumb (instance, filename):
	return 'gallery/_thumbs/' + instance.album.alias + '/' + filename


def get_path_for_micro (instance, filename):
	return 'gallery/_micros/' + instance.album.alias + '/' + filename


class Picture (models.Model):
	uuid                         = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
	album                        = models.ForeignKey(Album, on_delete=models.CASCADE)
	date_uploaded                = models.DateTimeField(auto_now_add=True)
	picture                      = models.ImageField(upload_to=get_path_for_picture)
	picture_small                = models.ImageField(default=None, blank=True, null=True, upload_to=get_path_for_small, storage=OverwriteStorage())
	picture_thumb                = models.ImageField(default=None, blank=True, null=True, upload_to=get_path_for_thumb, storage=OverwriteStorage())
	picture_micro                = models.ImageField(default=None, blank=True, null=True, upload_to=get_path_for_micro, storage=OverwriteStorage())
	meta                         = JSONField(default={}, blank=True)

	@property
	def filename (self):
		return os.path.basename(self.picture.name)

	def picture_micro_admin (self):
		if self.picture_micro:
			return format_html('<a href="' + self.picture.url + '" target="_blank"><img src="' + self.picture_micro.url + '"/></a>')
		else:
			return 'No thumbnail yet'

	def __unicode__ (self):
		return self.filename


@receiver(pre_delete, sender=Picture)
def mymodel_delete(sender, instance, **kwargs):
	instance.picture.delete(False)
	instance.picture_small.delete(False)
	instance.picture_thumb.delete(False)
	instance.picture_micro.delete(False)
