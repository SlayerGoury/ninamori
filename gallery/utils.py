# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.conf import settings
from django.core.files.uploadedfile import InMemoryUploadedFile
from StringIO import StringIO
from wand.image import Image


def wand_resize (image_blob, filename, width, height, crop=False):
	result_aspect = float(width)/float(height)
	io = StringIO()

	with image_blob.clone() as image:
		# This thing crops image to aspect of desired result with preserving its original pixel aspect
		# Because I want cute thumbnails
		if crop:
			image_aspect = float(image.width)/float(image.height)
			if image_aspect > result_aspect:
				image.crop(
					width   = int( image.width - (image_aspect-result_aspect)*image.height ),
					height  = int( image.height ),
					gravity = 'center'
				)
			else:
				image.crop(
					width   = int( image.width ),
					height  = int( image.height - (result_aspect-image_aspect)*image.height ),
					gravity = 'center'
				)
			image.transform( resize = str(width) + 'x' + str(height) + '^' )

		image.transform( resize = str(width) + 'x' + str(height) )
		image.compression_quality = settings.GALLERY_PICSETTINGS['jpg_quality']
		image.format = 'jpeg'
		image.save(io)

	return InMemoryUploadedFile(io, None, filename+'.jpg', 'image/jpeg', io.len, None)


def make_smaller_ones (picture):
	filename = picture.filename
	if filename[-4:].lower() not in ['.jpg', '.jif', '.jfi', '.jpe'] and filename[-5:].lower() not in ['.jpeg', '.jfif']:
		filename += '.jpg'

	s = settings.GALLERY_PICSETTINGS
	with Image(blob=picture.picture.file.read()) as pic:

		if pic.width > s['limit'][0] or pic.height > s['limit'][1]:
			resized = wand_resize(pic, filename, s['limit'][0], s['limit'][1])
			picture.picture.save(filename, resized, False)

		if pic.width > s['small'][0] or pic.height > s['small'][1]:
			resized = wand_resize(pic, filename, s['small'][0], s['small'][1])
			picture.picture_small.save(filename, resized, False)

		resized = wand_resize(pic, filename, s['thumb'][0], s['thumb'][1], crop=True)
		picture.picture_thumb.save(filename, resized, False)

		resized = wand_resize(pic, filename, s['micro'][0], s['micro'][1], crop=True)
		picture.picture_micro.save(filename, resized, False)

	picture.save()
