# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.conf.urls import include, url
import views


urlpatterns = [
	url(r'^album-(?P<alias>.+)/(?P<uuid>.+)$', views.image_by_uuid, name='gallery image by uuid'),
	url(r'^album-(?P<alias>.+)$', views.album_by_alias, name='gallery album by alias'),
	url(r'^delete$', views.delete, name='delete picture'),
	url(r'^$', views.index, name='gallery index'),
]
