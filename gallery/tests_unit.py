# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.conf import settings
from django.core.files import File
from django.contrib.auth.models import Permission, User, AnonymousUser, Group
from django.contrib.sessions.middleware import SessionMiddleware
from django.test import TestCase, RequestFactory
from models import OverwriteStorage, Album, Picture
import utils, views
import os


class galleryTests (TestCase):
	def setUp (self):
		if os.path.isfile( os.path.join(settings.MEDIA_ROOT, 'gallery/test_album/test.png') ):
			os.remove( os.path.join(settings.MEDIA_ROOT, 'gallery/test_album/test.png') )
		if os.path.isfile( os.path.join(settings.MEDIA_ROOT, 'gallery/_small/test_album/test.png.jpg') ):
			os.remove( os.path.join(settings.MEDIA_ROOT, 'gallery/_small/test_album/test.png.jpg') )
		if os.path.isfile( os.path.join(settings.MEDIA_ROOT, 'gallery/_thumbs/test_album/test.png.jpg') ):
			os.remove( os.path.join(settings.MEDIA_ROOT, 'gallery/_thumbs/test_album/test.png.jpg') )
		if os.path.isfile( os.path.join(settings.MEDIA_ROOT, 'gallery/_micros/test_album/test.png.jpg') ):
			os.remove( os.path.join(settings.MEDIA_ROOT, 'gallery/_micros/test_album/test.png.jpg') )

		self.factory = RequestFactory()
		self.session = SessionMiddleware()
		self.user = User.objects.create_user(username='meow1', email='1m@e.ow', password='purr')
		self.album = Album.objects.create(owner=self.user, alias='test_album', published=True)

	def test_models_album (self):
		album = Album.objects.create(owner=self.user, alias='meow')
		self.assertEqual(album.__unicode__(), 'meow')

	def test_models_picture (self):
		pic = Picture.objects.create(album=self.album)
		img = os.path.join(settings.STATICFILES_DIRS[0], 'img/icon.png')
		with open(img, 'r') as file:
			file = File(file)
			file.name='test.png'
			pic.picture = file
			pic.save()
		self.assertTrue( os.path.isfile( os.path.join(settings.MEDIA_ROOT, 'gallery/test_album/test.png') ) )  # file is there
		pic.delete()
		self.assertFalse( os.path.isfile( os.path.join(settings.MEDIA_ROOT, 'gallery/test_album/test.png') ) )  # file was deleted

	def test_utils_make_smaller_ones (self):
		pic = Picture.objects.create(album=self.album)
		img = os.path.join(settings.STATICFILES_DIRS[0], 'img/http404_1280.png')  # because we want to test creating small one, so we need something big, otherwise it ould be skipped
		with open(img, 'r') as file:
			file = File(file)
			file.name='test.png'
			pic.picture = file
			pic.save()
		utils.make_smaller_ones(pic)
		self.assertTrue( os.path.isfile( os.path.join(settings.MEDIA_ROOT, 'gallery/test_album/test.png') ) )
		self.assertTrue( os.path.isfile( os.path.join(settings.MEDIA_ROOT, 'gallery/_small/test_album/test.png.jpg') ) )
		self.assertTrue( os.path.isfile( os.path.join(settings.MEDIA_ROOT, 'gallery/_thumbs/test_album/test.png.jpg') ) )
		self.assertTrue( os.path.isfile( os.path.join(settings.MEDIA_ROOT, 'gallery/_micros/test_album/test.png.jpg') ) )  # files is there
		pic.delete()
		self.assertFalse( os.path.isfile( os.path.join(settings.MEDIA_ROOT, 'gallery/test_album/test.png') ) )
		self.assertFalse( os.path.isfile( os.path.join(settings.MEDIA_ROOT, 'gallery/_small/test_album/test.png.jpg') ) )
		self.assertFalse( os.path.isfile( os.path.join(settings.MEDIA_ROOT, 'gallery/_thumbs/test_album/test.png.jpg') ) )
		self.assertFalse( os.path.isfile( os.path.join(settings.MEDIA_ROOT, 'gallery/_micros/test_album/test.png.jpg') ) )  # files was deleted

	def test_models_picture_subfunctions (self):
		pic = Picture.objects.create(album=self.album)
		img = os.path.join(settings.STATICFILES_DIRS[0], 'img/icon.png')
		with open(img, 'r') as file:
			file = File(file)
			file.name='test.png'
			pic.picture = file
			pic.save()
		self.assertEqual(pic.picture_micro_admin(), 'No thumbnail yet')
		self.assertEqual(pic.__unicode__(), 'test.png')
		utils.make_smaller_ones(pic)
		self.assertIn('test.png', pic.picture_micro_admin())
		pic.delete()

	def test_views_index (self):
		request = self.factory.get('/')
		request.user = AnonymousUser()
		request.LANGUAGE_CODE = 'en'
		result = views.index(request)
		self.assertEqual(result.status_code, 200)
		self.assertIn('test_album', result.content.decode('utf8'))

	def test_views_album_by_alias (self):
		request = self.factory.get('/')
		request.user = self.user
		request.LANGUAGE_CODE = 'en'
		result = views.album_by_alias(request, 'test_album')
		self.assertEqual(result.status_code, 200)

	def test_views_image_by_uuid (self):
		request = self.factory.get('/')
		request.user = self.user
		request.LANGUAGE_CODE = 'en'

		pic = Picture.objects.create(album=self.album)
		img = os.path.join(settings.STATICFILES_DIRS[0], 'img/icon.png')
		with open(img, 'r') as file:
			file = File(file)
			file.name='test.png'
			pic.picture = file
			pic.save()
		utils.make_smaller_ones(pic)

		result = views.image_by_uuid(request, 'test_album', str(pic.uuid))
		self.assertEqual(result.status_code, 200)
		pic.delete()

	def test_views_delete (self):
		request = self.factory.post('/')
		request.POST.__dict__['_mutable'] = True
		request.user = self.user
		request.LANGUAGE_CODE = 'en'

		pic = Picture.objects.create(album=self.album)
		img = os.path.join(settings.STATICFILES_DIRS[0], 'img/icon.png')
		with open(img, 'r') as file:
			file = File(file)
			file.name='test.png'
			pic.picture = file
			pic.save()
		utils.make_smaller_ones(pic)

		request.POST['uuid'] = str(pic.uuid)
		result = views.delete(request)
		self.assertEqual(result.status_code, 200)
		self.assertTrue( os.path.isfile( os.path.join(settings.MEDIA_ROOT, 'gallery/test_album/test.png') ) )  # files was not deleted yet
		request.POST['confirm'] = '1'
		result = views.delete(request)
		self.assertEqual(result.status_code, 302)
		self.assertFalse( os.path.isfile( os.path.join(settings.MEDIA_ROOT, 'gallery/test_album/test.png') ) )  # files was deleted
