# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib import admin
from django.contrib.auth.models import User
from models import Album, Picture
from utils import make_smaller_ones


class PictureInline (admin.TabularInline):
	model = Picture
	fieldsets = [
		(None, {'fields': ['picture']}),
	]
	extra = 5


class AlbumAdmin (admin.ModelAdmin):
	fieldsets = [
		(None, {'fields': ['owner', 'alias', 'allowed_groups', 'upload_permissions']}),
	]
	list_display = ('alias', 'owner', 'published')
	list_editable = ('published',)
	inlines = [PictureInline]

	def formfield_for_foreignkey (self, db_field, request, **kwargs):
		if db_field.name == 'owner':
			kwargs['initial'] = request.user.id
		return super(AlbumAdmin, self).formfield_for_foreignkey(
			db_field, request, **kwargs
		)


def prepare_miniatures (modeladmin, request, queryset):
	for item in queryset:
		make_smaller_ones(item)
	prepare_miniatures.short_description = 'Prepare thumbnails and smaller versions'


class PictureAdmin (admin.ModelAdmin):
	fieldsets = [
		(None, {'fields': ['album', 'picture']}),
	]
	list_display = ('filename', 'album', 'picture_micro_admin')
	actions = [prepare_miniatures]


admin.site.register(Album, AlbumAdmin)
admin.site.register(Picture, PictureAdmin)
