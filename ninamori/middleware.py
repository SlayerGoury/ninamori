# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.conf import settings
from django.urls import reverse
from django.utils.deprecation import MiddlewareMixin
import logging
import re
import time
from utils import get_name

logger = logging.getLogger('access_log')
timelogger = logging.getLogger('timer')


class ForceBrowserToUseCacheMiddleware (MiddlewareMixin):
	def process_response (self, request, response):
		if not response.get('Cache-Control'):
			response['Cache-Control'] = 'no-cache, must-revalidate, max-age=0'
		return response


class ForceDefaultLanguageMiddleware (MiddlewareMixin):
	def process_request (self, request):
		if request.META.has_key('HTTP_ACCEPT_LANGUAGE'):
			del request.META['HTTP_ACCEPT_LANGUAGE']
		try:
			request.LANGUAGE_CODE
		except AttributeError as e:
			if e.message == "'WSGIRequest' object has no attribute 'LANGUAGE_CODE'":
				request.LANGUAGE_CODE = settings.LANGUAGE_CODE


class XrealIpToRemoteaddr (MiddlewareMixin):
	def process_request (self, request):
		if request.META.has_key('HTTP_X_REAL_IP'):
			request.META['REMOTE_ADDR'] = request.META['HTTP_X_REAL_IP']


class AccessLogging (MiddlewareMixin):
	def process_request (self, request):
		log_message = (
			'"%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s"'
		) % (
			str(request.META.get('REMOTE_ADDR')),
			str(request.META.get('HTTP_USER_AGENT')),
			str(request.COOKIES.get('sessionid')),
			request.method,
			request.build_absolute_uri(),
			str(request.GET.urlencode()),
			str(request.POST.urlencode()),
			str(request.LANGUAGE_CODE),
		)
		logger.info(log_message)


class Stats (MiddlewareMixin):
	def process_request (self, request):
		request.META['RENDER_TIME'] = time.time()

	def process_view (self, request, view_func, view_args, view_kwargs):
		request.META['RENDER_TIME'] = time.time() - request.META['RENDER_TIME']
		timelogger.debug(request.META['RENDER_TIME'])


class StripWhitespaceMiddleware(object):
	"""
	Tightens up response content by removed superflous line breaks and whitespace.
	By Doug Van Horn and Cal Leeming

	---- TODO ----
	* Ensure whitespace isn't stripped from within <code> or <textarea> tags.
	"""
	def __init__(self, get_response):
		self.get_response = get_response
		self.stripper = re.compile('(?ims)(?!<pre[^>]*?>)(\s*\n+\s*)(?![^<]*?</pre>)')

	def __call__(self, request):
		response = self.get_response(request)
		content_type = response.get('Content-Type') if response else None
		if content_type and 'text' in response.get('Content-Type'):
			if 'javascript' not in content_type and 'css' not in content_type and 'text/plain' not in content_type:
				response.content = self.stripper.sub('', response.content.decode('utf-8'))
		return response


def global_template_constants (request):
	language = request.LANGUAGE_CODE if request.LANGUAGE_CODE else settings.LANGUAGE_CODE
	try:
		user_name = get_name(request.user)
	except AttributeError:
		user_name = ''
	constants = {}
	constants['SITE_NAME'] = settings.SITE_NAME[language]
	constants['PROTOCOL_SCHEME'] = settings.PROTOCOL_SCHEME
	constants['SITE_DOMAIN'] = settings.SITE_DOMAIN
	constants['COLORS'] = settings.THEME_COLORS
	constants['DISABLED_FEATURES'] = settings.DISABLED_FEATURES
	constants['url_admin'] = reverse('admin:index')

	if not 'accounts' in settings.DISABLED_FEATURES:
		constants.update({
			'URL_ACCOUNTS':          reverse('accounts index'),
			'URL_ACCOUNTS_RESTORE':  reverse('accounts restore'),
			'URL_ACCOUNTS_ADMIN':    reverse('admin_index'),
			'URL_LOGIN':             reverse('login'),
			'URL_LOGOUT':            reverse('logout'),
			'USER_NAME':             user_name,
		})
	return constants
