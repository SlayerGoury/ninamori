from django.contrib.auth.models import User
from django.core.management import call_command
from django.test import TestCase, Client
import views
from accounts.utils import email_access_validate
from mailer.models import List
from cms.models import Page

c = Client()


class NinamoriTests (TestCase):
	def test_404_render (self):
		"""
		Can I render 404 page?
		"""
		response = c.get('/404')
		self.assertEqual(response.status_code, 404)

	def test_robots (self):
		"""
		Can I get robots.txt?
		"""
		response = c.get('/robots.txt')
		self.assertEqual(response.status_code, 200)

	def test_sitemap (self):
		"""
		Can I get sitemap?
		"""
		response = c.get('/sitemap.xml')
		self.assertEqual(response.status_code, 200)
		user = User.objects.create_user(username='meow')
		Page.objects.create(alias='index', author=user, is_published=True)
		Page.objects.create(alias='meow', author=user, is_published=True)
		Page.objects.create(alias='purr', author=user, is_published=True)
		response = c.get('/sitemap.xml')
		self.assertEqual(response.status_code, 200)

	def test_scripts (self):
		"""
		Can I get scripts?
		"""
		response = c.get('/scripts.js')
		self.assertEqual(response.status_code, 200)

	def test_stylesheet (self):
		"""
		Can I get stylesheet?
		"""
		response = c.get('/stylesheet.css')
		self.assertEqual(response.status_code, 200)

	def test_adminhelp (self):
		"""
		Can I access adminhelp?
		"""
		response = c.get('/adminhelp')
		self.assertEqual(response.status_code, 403)
		user = User.objects.create_superuser('meow', 'm@e.ow', 'meow')
		c.login(username='meow', password='meow')
		response = c.get('/adminhelp')
		self.assertEqual(response.status_code, 200)

	def test_command_generate_mailer_tokens (self):
		"""
		Testing command generate_mailer_tokens for coverage purpose
		"""
		l = List.objects.create(name='meow')
		User.objects.create(username='meow', email='meow@purr.murr')
		email_access_validate('meow@purr.murr')
		call_command('generate_mailer_tokens', token_count='1', list_uuid=str(l.uuid), qr=True, qr6=True, qr24=False, email='meow@purr.murr', purpose='testing', supersilent=True)
		call_command('generate_mailer_tokens', token_count='1', list_uuid=str(l.uuid), qr=True, qr6=False, qr24=True, email='meow@purr.murr', purpose='testing', supersilent=True)
		call_command('generate_mailer_tokens', token_count='1', list_uuid=str(l.uuid), qr=True, qr6=False, qr24=False, email='meow@purr.murr', purpose='testing', supersilent=True)

	def test_command_generate_polls_tokens (self):
		"""
		Testing command generate_polls_tokens for coverage purpose
		"""
		User.objects.create(username='meow', email='meow@purr.murr')
		email_access_validate('meow@purr.murr')
		call_command('generate_polls_tokens', token_count='1', redirect='/', qr=True, qr6=True, qr24=False, email='meow@purr.murr', purpose='testing', supersilent=True)
		call_command('generate_polls_tokens', token_count='1', redirect='/', qr=True, qr6=False, qr24=True, email='meow@purr.murr', purpose='testing', supersilent=True)
		call_command('generate_polls_tokens', token_count='1', redirect='/', qr=True, qr6=False, qr24=False, email='meow@purr.murr', purpose='testing', supersilent=True)
