# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.conf import settings
from ninamori.utils import get_name
import logging
import requests
import threading
from Queue import Queue

BLOG = settings.PROTOCOL_SCHEME + '://' + settings.SITE_DOMAIN
KEY = settings.AKISMET_API_KEY
logger = logging.getLogger(__name__)


# TODO remove alias arg because it is now part of comment object


# This class made with help of Anurag Uniyal form stackoverflow
class SubmitAkismetCommentsThread (threading.Thread):
	def __init__(self, queue, silent, verbose):
		super(SubmitAkismetCommentsThread, self).__init__()
		self.daemon = True
		self.queue = queue
		self.silent = silent
		self.verbose = verbose

	def run(self):
		while True:
			payload = self.queue.get()
			try:
				result = self.submit_akismet(payload)
				self.queue.result[str(payload['comment'].uuid)] = result
			except Exception, e:
				logger.warning('Exception: %s' % e)
			self.queue.task_done()

	def submit_akismet (self, payload):
		if self.verbose:
			print payload['comment'].content + '\n'
		spam = is_comment_a_spam(payload['comment'], payload['user'])
		if spam:
			if not self.silent: print 'Spam     [' + str(payload['comment'].uuid) + ']'
			return True
		else:
			if not self.silent: print 'Not spam [' + str(payload['comment'].uuid) + ']'
			return False


class AkismetError (Exception):
	def __init__ (self, text, status):
		self.text = text
		self.status = status
	def __str__ (self):
		return '[' + self.text + ' | ' + str(self.status) + ']'


def threaded_akismet (comments_list, silent, verbose):
	queue = Queue()
	queue.result = {}

	for comment in comments_list:
		payload = {'comment': comment, 'user': comment.user}
		queue.put(payload)

	for i in xrange(len(comments_list)):
		t = SubmitAkismetCommentsThread(queue, silent, verbose)
		t.start()

	queue.join()
	return queue.result


def verify_key ():
	payload = {'key': KEY, 'blog': BLOG}
	url = 'https://' + settings.AKISMET_DOMAIN + '/1.1/verify-key'
	r = requests.post(url, data = payload)
	if r.text == 'valid':
		return True
	if r.text == 'invalid':
		return False
	raise AkismetError(r.text, r.status_code)


def prepare_comment_payload (comment, user, test):
	payload = {
		'blog': BLOG,
		'user_ip': comment.user_ip,
		'user_agent': comment.user_agent,
		'referrer': comment.http_referrer,
		'permalink': BLOG + comment.group_alias + '#comment_' + str(comment.uuid),
		'comment_type': 'comment',
		'comment_author': get_name(user),
		'comment_author_email': comment.email,
		'comment_author_url': '',
		'comment_content': comment.content,
		'comment_date_gmt': comment.date_added,
		'comment_post_modified_gmt': '',
		'blog_lang': settings.LANGUAGE_CODE,
		'blog_charset': 'UTF-8',
		'user_role': '',
		'is_test': test,
	}
	return payload


def is_comment_a_spam (comment, user, blog=BLOG, test=settings.TESTING):
	'''
	This thing returns False for good comments and True for spam
	'''
	payload = prepare_comment_payload(comment, user, test)
	url = 'https://' + KEY + '.' + settings.AKISMET_DOMAIN + '/1.1/comment-check'
	r = requests.post(url, data = payload)
	if r.text == 'true':
		return True
	if r.text == 'false':
		return False
	raise AkismetError(r.text, r.status_code)


def submit_false_comment (comment, alias, ham=False, spam=False, blog=BLOG, test=settings.TESTING):
	if spam:
		api = '/1.1/submit-spam'
	elif ham:
		api = '/1.1/submit-ham'
	else:
		return False
	payload = prepare_comment_payload(comment, comment.user, test)
	url = 'https://' + KEY + '.' + settings.AKISMET_DOMAIN + api
	r = requests.post(url, data = payload)
	if r.status_code != 200:
		raise AkismetError(r.text, r.status_code)
	return True
