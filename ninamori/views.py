# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.conf import settings
from django.http import Http404
from django.shortcuts import render
from django.urls import reverse
from django.utils.translation import ugettext as _
from django.views.decorators.http import last_modified
from datetime import datetime, timedelta
from cms.models import Page
from ninamori.utils import get_last_restart

def not_found (request):
	raise Http404(_('Page does not exist'))


def robots (request):
	return render(request, 'robots.txt', content_type='text/plain')


def sitemap (request):
	urlset = []

	try:
		index_page = Page.objects.get(alias='index')
		if index_page.is_published:
			urlset.append ((reverse('cms index'), index_page.date_modified))
	except Page.DoesNotExist:
		pass

	for page in Page.objects.exclude(alias='index'):
		if page.is_published and not page.allowed_groups.count():
			urlset.append((reverse('cms page by alias', kwargs={'alias': page.alias}), page.date_modified))

	return render(request, 'sitemap.xml', {'urlset': urlset}, content_type='text/xml')


@last_modified(get_last_restart)
def scripts (requset):
	url_check_username = reverse('check_username') if not 'accounts' in settings.DISABLED_FEATURES else None
	response = render(requset, 'scripts.js', {
		'url_check_username': {'url_check_username': url_check_username},  # because it should be injected into minifier
	},
	content_type='application/javascript')
	return response


@last_modified(get_last_restart)
def stylesheet (request):
	response = render(request, 'stylesheet.css', content_type='text/css')
	return response


def adminhelp (request):
	if request.user.is_staff:
		return render(request, 'adminhelp.html')
	else:
		return render(request, '403.html', {'message': _('Staff only')}, status=403)
