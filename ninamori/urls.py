from django.conf import settings
from django.conf.urls import include, url
from django.contrib import admin
from django.views import static
import views
from cms import views as cms_views


urlpatterns = [
	url(r'^admin/',           admin.site.urls),
	url(r'^translator/',      include('django.conf.urls.i18n')),
	url(r'^tinymce/',         include('tinymce.urls')),
	url(r'^api/',             include('api.urls')),
	url(r'^cms/',             include('cms.urls')),
	url(r'^chat/',            include('chat.urls')),
	url(r'^comment/',         include('comments.urls')),
	url(r'^feedback/',        include('feedback.urls')),
	url(r'^gallery/',         include('gallery.urls')),
	url(r'^id/',              include('accounts.urls')),
	url(r'^mailer/',          include('mailer.urls')),
	url(r'^polls/',           include('polls.urls')),
	url(r'404$',             views.not_found, name='404'),
	url(r'robots.txt$',      views.robots, name='robots'),
	url(r'sitemap.xml$',     views.sitemap, name='sitemap'),
	url(r'scripts.js$',      views.scripts, name='scripts'),
	url(r'stylesheet.css$',  views.stylesheet, name='stylesheet'),
	url(r'adminhelp$',       views.adminhelp, name='adminhelp'),
	url(r'^$',               cms_views.index, name='index'),
	url(r'^media/(?P<path>.*)$', static.serve, {'document_root': settings.MEDIA_ROOT,}),
	url(r'^(?P<alias>.+)$', cms_views.superalias, name='page from superalias'),
]
