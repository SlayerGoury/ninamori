# -*- coding: utf-8 -*-

#####################################################################################
#                                                                                   #
#  Do not edit this file.                                                           #
#  Should you need to change some default setting, redefine it in your settings.py  #
#                                                                                   #
#####################################################################################

from __future__ import unicode_literals
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
import os
import sys

PROJECT_DIR = os.path.dirname(__file__)

AKISMET_DOMAIN = 'rest.akismet.com'
AKISMET_USERAGENT = 'Ninamori CMS/0.6 Nighly | comments/0.6 Nightly'
ALLOWED_HOSTS = '*'
APPEND_SLASH = True
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
CSRF_COOKIE_SECURE = True
DEBUG = False
DATA_UPLOAD_MAX_MEMORY_SIZE = 52428800  # 50 MB
GALLERY_PICSETTINGS = {
	'jpg_quality': 75,
	'limit': (8192, 8192),
	'small': (800, 800),
	'thumb': (150, 150),
	'micro': (40, 40),
}
GALLERY_ALBUMSETTINGS ={
	'fimstrip': 7,
}
GRAVATAR_SIZES = [32, 40, 70, 80]
GRAVATAR_RATING = 'x'
GRAVATAR_DEFAULT = 'monsterid'
GRAVATAR_URL_PREFIX = 'https://www.gravatar.com/avatar/'
LANGUAGE_COOKIE_NAME = '_language'
LAST_RESTART = timezone.now()
LOCKING = {'time_until_expiration': 630, 'time_until_warning': 600}
MAILQUEUE_STORAGE = True
MAILQUEUE_ROOT = os.path.join(PROJECT_DIR, '../mailqueue')
MEDIA_ROOT = os.path.join(PROJECT_DIR, '../media')
MEDIA_URL = '/media/'
QR_TEMP = os.path.join(PROJECT_DIR, '../mailqueue/qr_temp')
ROOT_URLCONF = 'ninamori.urls'
SESSION_COOKIE_SECURE = True
STATIC_ROOT = os.path.join(PROJECT_DIR, '../static')
STATIC_URL = '/static/'
TESTING = sys.argv[1:2] == ['test']
TINYMCE_COMPRESSOR = True
USE_I18N = True
USE_L10N = True
USE_TZ = True
WSGI_APPLICATION = 'ninamori.wsgi.application'

INSTALLED_APPS = (
	'flat',
	'mailqueue',
	'missing',
	'tinymce',

	'django.contrib.admin',
	'django.contrib.auth',
	'django.contrib.contenttypes',
	'django.contrib.sessions',
	'django.contrib.messages',
	'django.contrib.staticfiles',

	'accounts',
	'api',
	'chat',
	'cms',
	'comments',
	'feedback',
	'gallery',
	'mailer',
	'ninamori',
	'polls',
)

MIDDLEWARE = [
	'django.contrib.sessions.middleware.SessionMiddleware',
	'django.middleware.common.CommonMiddleware',
	'django.middleware.csrf.CsrfViewMiddleware',
	'django.contrib.auth.middleware.AuthenticationMiddleware',
	'django.contrib.messages.middleware.MessageMiddleware',
	'django.middleware.clickjacking.XFrameOptionsMiddleware',
	'django.middleware.security.SecurityMiddleware',
	'django.middleware.locale.LocaleMiddleware',
	'ninamori.middleware.AccessLogging',
	'ninamori.middleware.ForceDefaultLanguageMiddleware',
	'ninamori.middleware.XrealIpToRemoteaddr',
	'ninamori.middleware.ForceBrowserToUseCacheMiddleware',
	'ninamori.middleware.StripWhitespaceMiddleware',
]

TEMPLATES = [
	{
		'BACKEND': 'django.template.backends.django.DjangoTemplates',
		'DIRS': [BASE_DIR + '/templates',],
		'APP_DIRS': True,
		'OPTIONS': {
			'context_processors': [
				'django.template.context_processors.debug',
				'django.template.context_processors.request',
				'django.contrib.auth.context_processors.auth',
				'django.contrib.messages.context_processors.messages',
				'ninamori.middleware.global_template_constants',
				'polls.utils.count_cow_powers',
			],
		},
	},
]

LOCALE_PATHS = [
	os.path.join(BASE_DIR, 'locale'),
]

LOGGING = {
	'version': 1,
	'disable_existing_loggers': False,
	'formatters': {
		'verbose': {'format': '[%(asctime)s] %(levelname)s [%(name)s:%(lineno)s] %(message)s', 'datefmt': '%d-%m-%Y %H:%M:%S'},
		'normal':  {'format': '[%(asctime)s] %(message)s', 'datefmt': '%d-%m-%Y %H:%M:%S'},
		'simple':  {'format': '%(asctime)s %(message)s', 'datefmt': '%d-%m-%Y %H:%M:%S'},
		'csv':     {'format': '"%(asctime)s", %(message)s', 'datefmt': '%d-%m-%Y %H:%M:%S'}
	},
	'handlers': {
		'access':           {'level': 'INFO',     'class': 'logging.FileHandler', 'filename': BASE_DIR + '/log/access.log.csv',   'formatter': 'csv'},
		'accounts':         {'level': 'INFO',    'class': 'logging.FileHandler', 'filename': BASE_DIR + '/log/accounts.log',     'formatter': 'simple'},
		'file':             {'level': 'DEBUG',    'class': 'logging.FileHandler', 'filename': BASE_DIR + '/log/default.log',      'formatter': 'verbose'},
		'api_debug':        {'level': 'DEBUG',    'class': 'logging.FileHandler', 'filename': BASE_DIR + '/log/debug_api.log',    'formatter': 'verbose'},
		'cms_info':         {'level': 'INFO',     'class': 'logging.FileHandler', 'filename': BASE_DIR + '/log/log_cms.log',      'formatter': 'normal'},
		'cms_warning':      {'level': 'WARNING',  'class': 'logging.FileHandler', 'filename': BASE_DIR + '/log/log_cms.log',      'formatter': 'normal'},
		'mailer_info':      {'level': 'INFO',     'class': 'logging.FileHandler', 'filename': BASE_DIR + '/log/log_mailer.log',   'formatter': 'normal'},
		'ninamori_errors':  {'level': 'WARNING',  'class': 'logging.FileHandler', 'filename': BASE_DIR + '/log/log_ninamori.log', 'formatter': 'verbose'},
		'polls_info':       {'level': 'INFO',     'class': 'logging.FileHandler', 'filename': BASE_DIR + '/log/log_polls.log',    'formatter': 'normal'},
		'timer':            {'level': 'DEBUG',    'class': 'logging.FileHandler', 'filename': BASE_DIR + '/log/timer.log',        'formatter': 'simple'},
	},
	'loggers': {
		'access_log':        {'handlers': ['access'],          'level': 'INFO'},
		'accounts':          {'handlers': ['accounts'],        'level': 'INFO'},
		'default_debug':     {'handlers': ['file'],            'level': 'DEBUG'},
		'api':               {'handlers': ['api_debug'],       'level': 'DEBUG'},
		'cms':               {'handlers': ['cms_warning'],     'level': 'WARNING'},
		'mailer':            {'handlers': ['mailer_info'],     'level': 'INFO'},
		'mailqueue.models':  {'handlers': ['ninamori_errors'], 'level': 'WARNING'},
		'ninamori.akismet':  {'handlers': ['ninamori_errors'], 'level': 'WARNING'},
		'ninamori':          {'handlers': ['ninamori_errors'], 'level': 'WARNING'},
		'polls':             {'handlers': ['polls_info'],      'level': 'INFO'},
		'timer':             {'handlers': ['timer'],           'level': 'DEBUG'},
	},
}

LANGUAGES = (
	('ru', _('Russian')),
	('en', _('English')),
)
