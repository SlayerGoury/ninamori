# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from api.models import YandexBalance
from django.contrib.auth.models import AnonymousUser, User, Group
from django.conf import settings
from django.http import HttpRequest, Http404
from django.urls import reverse
from django.test import TestCase, RequestFactory
from models import Page, MenuLink, LeafletMap, Superalias
import datetime
import views, utils, templatetags.mainmenu


class CmsTests (TestCase):
	def setUp (self):
		self.factory = RequestFactory()
		self.user = User.objects.create_user(username='meow', email='m@e.ow', password='purr')

	def test_models_page (self):
		user = self.user
		page = Page.objects.create(
			title_en = 'meow',
			alias = 'meow',
			content_en = 'meow',
			author = user,
		)
		page.is_published = True
		page.is_in_menu = True
		page.save()
		page = Page.objects.get(alias = 'meow')
		self.assertEqual(page.is_published, True)
		self.assertEqual(page.is_in_menu, True)

	def test_models_page_localized_title (self):
		user = self.user
		page = Page.objects.create(
			title_en = 'meow',
			title_ru = 'мяу',
			alias = 'meow',
			content_en = 'purr',
			content_ru = 'мурр',
			author = user,
		)
		page.is_published = True
		page.is_in_menu = True
		page.save()
		page = Page.objects.get(alias = 'meow')
		self.assertEqual(page.localized_title('en'), 'meow')
		self.assertEqual(page.localized_title('ru'), 'мяу')
		page.title_ru = ''
		page.save()
		page = Page.objects.get(alias = 'meow')
		self.assertEqual(page.localized_title('en'), 'meow')
		self.assertEqual(page.localized_title('ru'), 'meow')
		page.title_ru = 'мяу'
		page.title_en = ''
		page.save()
		page = Page.objects.get(alias = 'meow')
		self.assertEqual(page.localized_title('en'), 'мяу')
		self.assertEqual(page.localized_title('ru'), 'мяу')
		language_code = settings.LANGUAGE_CODE
		settings.LANGUAGE_CODE = 'xx'
		self.assertEqual(page.localized_title('xx'), 'мяу')
		page.title_ru = ''
		page.save()
		self.assertEqual(page.localized_title('xx'), '')
		settings.LANGUAGE_CODE = language_code

	def test_models_page_localized_content (self):
		user = self.user
		page = Page.objects.create(
			title_en = 'meow',
			title_ru = 'мяу',
			alias = 'meow',
			content_en = 'purr',
			content_ru = 'мурр',
			author = user,
		)
		page.is_published = True
		page.is_in_menu = True
		page.save()
		page = Page.objects.get(alias = 'meow')
		self.assertEqual(page.localized_content('en'), 'purr')
		self.assertEqual(page.localized_content('ru'), 'мурр')
		page.content_ru = ''
		page.save()
		page = Page.objects.get(alias = 'meow')
		self.assertEqual(page.localized_content('en'), 'purr')
		self.assertEqual(page.localized_content('ru'), 'purr')
		page.content_ru = 'мяу'
		page.content_en = ''
		page.save()
		page = Page.objects.get(alias = 'meow')
		self.assertEqual(page.localized_content('en'), 'мяу')
		self.assertEqual(page.localized_content('ru'), 'мяу')

	def test_models_page_list_title (self):
		user = self.user
		page = Page.objects.create(
			title_en = 'meow',
			alias = 'meow',
			content_en = 'purr',
			author = user,
		)
		page.is_published = True
		page.is_in_menu = True
		page.save()
		page = Page.objects.get(alias = 'meow')
		self.assertEqual(page.list_title(), 'meow')

	def test_models_page_unicode (self):
		user = self.user
		page = Page.objects.create(
			title_en = 'meow',
			title_ru = '',
			alias = 'meow',
			content_en = 'purr',
			content_ru = '',
			author = user,
		)
		page.is_published = True
		page.is_in_menu = True
		page.save()
		page = Page.objects.get(alias = 'meow')
		self.assertEqual(page.__unicode__(), 'meow')

	def test_menulink (self):
		MenuLink.objects.create(
			title_en = 'purr',
			menu_target = '',
			order = 10,
			url = '/meow',
		)
		link = MenuLink.objects.get(url = '/meow')
		self.assertEqual(link.title_en, 'purr')

	def test_menulink_localized_title (self):
		MenuLink.objects.create(
			title_en = 'purr',
			title_ru = 'мурр',
			menu_target = '',
			order = 10,
			url = '/meow',
		)
		link = MenuLink.objects.get(url = '/meow')
		self.assertEqual(link.localized_title('en'), 'purr')
		self.assertEqual(link.localized_title('ru'), 'мурр')
		link.title_ru = ''
		link.save()
		link = MenuLink.objects.get(url = '/meow')
		self.assertEqual(link.localized_title('ru'), 'purr')
		link.title_ru = 'мурр'
		link.title_en = ''
		link.save()
		language_code = settings.LANGUAGE_CODE
		settings.LANGUAGE_CODE = 'xx'
		self.assertEqual(link.localized_title('xx'), 'мурр')
		link.title_ru = ''
		link.save()
		self.assertEqual(link.localized_title('xx'), '')
		settings.LANGUAGE_CODE = language_code

	def test_views_hooks (self):
		YandexBalance.objects.create()
		LeafletMap.objects.create(title='meow', center=[0.1, 0.1], marker=[0.1, 0.1], waypoints=[[0.1, 0.1]])
		request = self.factory.get('/')
		request.user = AnonymousUser()
		request.session = {}
		request.LANGUAGE_CODE = 'en'
		request.META = {'CSRF_COOKIE': 'meow'}
		self.assertEqual(type(views.hooks('next_2nd_friday_or_last_saturday', request)), unicode)
		self.assertIn('<form ', views.hooks('yandex_money_form', request))
		self.assertIn('<form ', views.hooks('poll_form|meow|meow', request))
		self.assertIn('<form ', views.hooks('subscribe_form', request))
		self.assertFalse('<form ' in views.hooks('poll_form|meow', request))
		self.assertFalse('<form ' in views.hooks('poll_form', request))
		self.assertIn('<form ', views.hooks('poll_results|meow', request))
		self.assertFalse('<form ' in views.hooks('poll_results', request))
		self.assertIn('random invalid hook', views.hooks('random invalid hook', request))
		self.assertIn('misconfigured pool results hook', views.hooks('poll_results|||', request))
		self.assertIn('<form ', views.hooks('feedback_form', request))
		self.assertIn('leaflet', views.hooks('leaflet_map|meow', request)[0])
		self.assertIn('misconfigured leaflet map hook', views.hooks('leaflet_map||', request))
		self.assertIn('<form ', views.hooks('comments', request)[0])

	def test_views_index (self):
		user = self.user
		Page.objects.create(alias='index', is_published = True, author=user)
		request = HttpRequest()
		request.LANGUAGE_CODE = 'en'
		response = views.index(request)
		self.assertEqual(response.status_code, 200)

	def test_views_page_by_alias (self):
		user = self.user
		group = Group.objects.create(name='meow')
		Page.objects.create(alias='index', is_published = True, author=user, content_en='{{ comments }}')
		Page.objects.create(alias='meow_1', is_published = True, author=user)
		Page.objects.create(alias='meow_2', is_published = False, author=user)
		page3 = Page.objects.create(alias='meow_3', is_published = True, author=user)
		page3.allowed_groups.add(group)

		request = HttpRequest()
		request.LANGUAGE_CODE = 'en'
		request.user = user
		request.session = {}
		response = views.page_by_alias(request, 'index')
		self.assertEqual(response.status_code, 200)
		response = views.page_by_alias(request, '')
		self.assertEqual(response.status_code, 302)
		response = views.page_by_alias(request, 'meow_1')
		self.assertEqual(response.status_code, 200)
		try: response = views.page_by_alias(request, 'meow_2')
		except Http404: pass
		try: response = views.page_by_alias(request, 'meow_97')
		except Http404: pass
		response = views.page_by_alias(request, 'meow_3')
		self.assertEqual(response.status_code, 200)

	def test_views_scripts_for_page_by_alias (self):
		user = self.user
		request = HttpRequest()
		request.LANGUAGE_CODE = 'en'
		request.user = user
		request.session = {}
		Page.objects.create(alias='meow_1', is_published = False, author=user, content_en='{{ comments }}')
		Page.objects.create(alias='meow_2', is_published = True, author=user, content_en='{{ comments }}')
		try:
			type(views.scripts_for_page_by_alias(request, 'meow_1'))
		except Http404:
			http404 = True
		self.assertTrue(http404)
		self.assertEqual(views.scripts_for_page_by_alias(request, 'meow_2').status_code, 200)

	def test_views_superalias (self):
		user = self.user
		Page.objects.create(alias='meow', is_published = True, author=user)
		Superalias.objects.create(alias='meow', target='/cms/meow')
		request = HttpRequest()
		request.LANGUAGE_CODE = 'en'
		response = views.superalias(request, 'meow')
		self.assertEqual(response.status_code, 200)

	def test_utils_week_of_month (self):
		self.assertEqual(type(utils.week_of_month(datetime.datetime.now())), int)

	def test_utils_nth_weekday (self):
		self.assertEqual(type(utils.nth_weekday(datetime.datetime.now(), 2, 3)), datetime.datetime)
		self.assertEqual(utils.nth_weekday(datetime.datetime(2015, 1, 1), 1, 0), datetime.datetime(2015, 1, 5))
		self.assertEqual(utils.nth_weekday(datetime.datetime(2015, 2, 1), 2, 4), datetime.datetime(2015, 2, 13))
		self.assertEqual(utils.nth_weekday(datetime.datetime(2015, 9, 1), 5, 6), datetime.datetime(2015, 10, 4))

	def test_utils_get_next_2nd_friday_or_last_saturday (self):
		self.assertEqual(type(utils.get_next_2nd_friday_or_last_saturday('ru', 'l, j E Y')), unicode)
		self.assertEqual(type(utils.get_next_2nd_friday_or_last_saturday('en', 'l, j E Y')), unicode)

	def test_utils_next_2nd_friday_or_last_saturday (self):
		for year in xrange(2015, 2038):
			for day in xrange(1, 366):
				date = datetime.datetime.strptime(str(year)+' '+str(day), '%Y %j')
				self.assertEqual(type(utils.next_2nd_friday_or_last_saturday(date)), datetime.datetime)

	def test_utils_get_hooks (self):
		self.assertEqual(
			utils.get_hooks(
				'{{meow}} {{ meow }}{{ moew}} moew {{meow }} { { m e o w } }'
			),
			['{{meow}}', '{{ meow }}', '{{ moew}}', '{{meow }}']
		)

	def test_utils_get_leaflet_map (self):
		LeafletMap.objects.create(title='meow', center=[0.1, 0.1], marker=[0.1, 0.1], waypoints=[[0.1, 0.1]])
		LeafletMap.objects.create(title='murr', center='purr')
		self.assertEqual(type(utils.get_leaflet_map('meow')), tuple)
		self.assertEqual(type(utils.get_leaflet_map('purr')), unicode)
		self.assertEqual(type(utils.get_leaflet_map('murr')), str)

	def test_utils_get_page_last_modified (self):
		request = HttpRequest()
		try:
			self.assertEqual(type(utils.get_page_last_modified(request, 'meow')), datetime.datetime)
		except Http404:
			pass

	def test_templatetags_get_items_for_menu (self):
		user = self.user
		Page.objects.create(alias='meow_1', title_en= 'meow first', is_published = True, is_in_menu=True, author=user, order = 20)
		Page.objects.create(alias='meow_2', title_en= 'meow second', is_published = True, is_in_menu=True, author=user, order = 40)
		MenuLink.objects.create(title_en = 'purr_1', order = 10, url = '/')
		MenuLink.objects.create(title_en = 'purr_2', order = 30, url = '/')
		request = HttpRequest()
		request.LANGUAGE_CODE = 'en'
		items_for_menu = templatetags.mainmenu.get_items_for_menu({'request': request})
		cms = reverse(views.page_by_alias)
		items_expected = [
			('/', 'purr_1', '', 10),
			(cms + 'meow_1', 'meow first', 'cms page', 20),
			('/', 'purr_2', '', 30),
			(cms + 'meow_2', 'meow second', 'cms page', 40),
		]
		self.assertEqual(items_for_menu, items_expected)
