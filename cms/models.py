# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.conf import settings
from django.contrib.auth.models import User, Group
from django.db import models
from django.utils.translation import ugettext as _
from jsonfield import JSONField
from tinymce import models as tinymce_models
import datetime
import uuid

HTML = '<p></p>'


class Page (models.Model):
	title_en         = models.CharField(max_length=254, default="Yet another untitled page", blank=True)
	title_ru         = models.CharField(max_length=254, default="Ещё одна страница", blank=True)
	order            = models.IntegerField(default=0)
	alias            = models.CharField(max_length=254, default="", unique=True)
	content_en       = tinymce_models.HTMLField(default=HTML, blank=True)
	content_ru       = tinymce_models.HTMLField(default=HTML, blank=True)
	header_includes  = models.TextField(default="", blank=True)
	footer_includes  = models.TextField(default="", blank=True)
	author           = models.ForeignKey(User, on_delete=models.CASCADE)
	date_created     = models.DateTimeField(auto_now_add=True)
	date_modified    = models.DateTimeField(auto_now=True)
	is_published     = models.BooleanField(default=False)
	is_in_menu       = models.BooleanField(default=False)
	allowed_groups   = models.ManyToManyField(Group, db_table="cms_page_allowed_groups", blank=True)

	def localized_title (self, value):
		if value == 'en' and self.title_en: return self.title_en
		if value == 'ru' and self.title_ru: return self.title_ru
		if settings.LANGUAGE_CODE == 'en' and self.title_en: return self.title_en
		if settings.LANGUAGE_CODE == 'ru' and self.title_ru: return self.title_ru
		if self.title_en: return self.title_en
		if self.title_ru: return self.title_ru
		return ''

	def localized_content (self, value):
		if value == 'en' and self.content_en and self.content_en != HTML: return self.content_en
		if value == 'ru' and self.content_ru and self.content_ru != HTML: return self.content_ru
		if settings.LANGUAGE_CODE == 'en' and self.content_en and self.content_en != HTML: return self.content_en
		if settings.LANGUAGE_CODE == 'ru' and self.content_ru and self.content_ru != HTML: return self.content_ru
		if self.content_en and self.content_en != HTML: return self.content_en
		if self.content_ru and self.content_ru != HTML: return self.content_ru
		return HTML

	def list_title (self):
		return self.title_en
	list_title.short_description = _('Title')

	def __unicode__ (self):
		return self.title_en


class LeafletMap (models.Model):
	title            = models.CharField(max_length=254, default="Yet another map", unique=True)
	center           = JSONField(default=[0,0])
	marker           = JSONField(default=[0,0])
	waypoints        = JSONField(default=[[0,0]])
	initial_zoom     = models.IntegerField(default=17)


class MenuLink (models.Model):
	title_en         = models.CharField(max_length=254, default="Yet another menu item", blank=True)
	title_ru         = models.CharField(max_length=254, default="Ещё один элемент меню", blank=True)
	menu_target      = models.CharField(max_length=254, default="", blank=True)
	order            = models.IntegerField(default=0)
	url              = models.CharField(max_length=2083)

	def localized_title (self, value):
		if value == 'en' and self.title_en: return self.title_en
		if value == 'ru' and self.title_ru: return self.title_ru
		if settings.LANGUAGE_CODE == 'en' and self.title_en: return self.title_en
		if settings.LANGUAGE_CODE == 'ru' and self.title_ru: return self.title_ru
		if self.title_en: return self.title_en
		if self.title_ru: return self.title_ru
		return ''


class Superalias (models.Model):
	alias            = models.CharField(max_length=254, default="", unique=True)
	target           = models.CharField(max_length=254, default="")
