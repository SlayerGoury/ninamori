# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.auth.models import User
from django.urls import reverse
from django.test import TestCase, Client
from api.models import YandexBalance
from models import Page
import views

c = Client(enforce_csrf_checks=True)


class CmsTests (TestCase):
	def test_index_render (self):
		"""
		Can I render index page?
		"""
		user = User.objects.create(username='meow')
		page = Page.objects.create(
			alias = 'index',
			content_en = '<p>Meow meow!</p>',
			author = user,
			is_published = True,
		)
		response = c.get(reverse(views.index))
		self.assertEqual(response.status_code, 200)

	def test_hook_next_2nd_friday_or_last_saturday (self):
		"""
		Can I get next_2nd_friday_or_last_saturday hook?
		"""
		user = User.objects.create(username='meow')
		page = Page.objects.create(
			alias = 'meow',
			content_en = '<p>Meow {{ next_2nd_friday_or_last_saturday }} meow!</p>',
			author = user,
			is_published = True,
		)
		response = c.get(reverse(views.page_by_alias) + 'meow')
		self.assertEqual(response.status_code, 200)

	def test_hook_yandex_money_form (self):
		"""
		Can I get yandex_money_form hook?
		"""
		yandex = YandexBalance.objects.create()
		yandex.save()
		user = User.objects.create(username='meow')
		page = Page.objects.create(
			alias = 'meow',
			content_en = '<p>Meow {{ yandex_money_form }} meow!</p>',
			author = user,
			is_published = True,
		)
		response = c.get(reverse(views.page_by_alias) + 'meow')
		self.assertEqual(response.status_code, 200)

	def test_hook_subscribe_form (self):
		"""
		Can I get subscribe_form hook?
		"""
		user = User.objects.create(username='meow')
		page = Page.objects.create(
			alias = 'meow',
			content_en = '<p>Meow {{ subscribe_form }} meow!</p>',
			author = user,
			is_published = True,
		)
		response = c.get(reverse(views.page_by_alias) + 'meow')
		self.assertEqual(response.status_code, 200)

	def test_hook_poll_form (self):
		"""
		Can I get poll_form hook?
		"""
		user = User.objects.create(username='meow')
		page = Page.objects.create(
			alias = 'meow',
			content_en = '<p>Meow {{ poll_form|meow|meow }} meow!</p>',
			author = user,
			is_published = True,
		)
		response = c.get(reverse(views.page_by_alias) + 'meow')
		self.assertEqual(response.status_code, 200)

	def test_hook_poll_results (self):
		"""
		Can I get poll_results hook?
		"""
		user = User.objects.create(username='meow')
		page = Page.objects.create(
			alias = 'meow',
			content_en = '<p>Meow {{ poll_results|meow }} meow!</p>',
			author = user,
			is_published = True,
		)
		response = c.get(reverse(views.page_by_alias) + 'meow')
		self.assertEqual(response.status_code, 200)

	def test_hook_meow (self):
		"""
		Can I get just random hook?
		"""
		user = User.objects.create(username='meow')
		page = Page.objects.create(
			alias = 'meow',
			content_en = '<p>Meow {{ meow }} meow!</p>',
			author = user,
			is_published = True,
		)
		response = c.get(reverse(views.page_by_alias) + 'meow')
		self.assertEqual(response.status_code, 200)

	def test_ton_of_hooks (self):
		"""
		Can I get a lot of hooks?
		"""
		yandex = YandexBalance.objects.create()
		yandex.save()
		user = User.objects.create(username='meow')
		page = Page.objects.create(
			alias = 'meow',
			content_en = '<p>Meow {{ next_2nd_friday_or_last_saturday }}{{ yandex_money_form }}{{ subscribe_form }}{{ next_2nd_friday_or_last_saturday }}{{ yandex_money_form }}{{ subscribe_form }}{{ poll_form|meow|meow }}{{ poll_results|meow }}{{ poll_form|meow|meow }}{{ poll_results|meow }} meow!</p>',
			author = user,
			is_published = True,
		)
		response = c.get(reverse(views.page_by_alias) + 'meow')
		self.assertEqual(response.status_code, 200)
