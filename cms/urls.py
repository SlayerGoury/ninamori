from django.conf.urls import include, url
import views


urlpatterns = [
	url(r'^(?P<alias>.+)/scripts.js$', views.scripts_for_page_by_alias, name='scripts for cms page by alias'),
	url(r'^(?P<alias>.+)$', views.page_by_alias, name='cms page by alias'),
	url(r'^$', views.page_by_alias, {'alias': ''}, 'cms index'),
]
