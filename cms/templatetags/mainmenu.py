from __future__ import unicode_literals
from django import template
from django.urls import reverse
from operator import itemgetter
from cms.models import Page, MenuLink

register = template.Library()


@register.assignment_tag(takes_context=True)
def get_items_for_menu (context):
	language_code = context['request'].LANGUAGE_CODE
	pages = Page.objects.only('is_in_menu', 'alias', 'title_en', 'title_ru', 'order').filter(is_in_menu=True)
	links = MenuLink.objects.all()
	result = []
	for page in pages:
		url = reverse('cms page by alias', kwargs={'alias': page.alias})
		target = 'cms index' if page.alias == 'index' else 'cms page'
		result.append((url, page.localized_title(language_code), target, page.order))
	for link in links:
		result.append((link.url, link.localized_title(language_code), link.menu_target,link.order))
	result = sorted(result, key=itemgetter(3))
	return result
