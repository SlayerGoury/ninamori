Upgrading from 0.3-0.5 to 0.6-99.99
------------------------------------

Mail queue management command doesn't need parameters anymore.

Change this:
	manage.py send_queued_messages --limit=100
	manage.py clear_sent_messages --offset=100

To this:
	manage.py send_queued_messages 100
	manage.py clear_sent_messages 100


Comments application has been updated and every comment now stores it's groups alias and is linked to a user instead of email.
Run this script from 'manage.py shell' to update every comment you may already have:

	from comments.models import Comment
	for comment in Comment.objects.all():
		comment.update_group_alias()
		comment.update_user()
