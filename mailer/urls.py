from django.conf.urls import include, url
import views


urlpatterns = [
	url(r'^$', views.index, name='index'),
	url(r'unsubscribe$', views.unsubscribe, name='unsubscribe'),
	url(r'subscribe$', views.subscribe, name='subscribe'),
	url(r'invited$', views.subscribe_invited, name='subscribe_invited'),
	url(r'confirm_list$', views.confirm_list, name='confirm'),
	url(r'confirm$', views.confirm, name='confirm'),
	url(r'manage$', views.manage, name='manage'),
	url(r'change_token$', views.change_token, name='change token'),
	url(r'generate_tokens$', views.generate_tokens, name='mailer_generate_tokens'),
]
