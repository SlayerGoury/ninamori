# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.conf import settings
from django.core.management.base import BaseCommand, CommandError
from django.urls import reverse
from django.template.loader import render_to_string
from django.utils import timezone
from django.utils.translation import activate as activate_translation
from django.utils.translation import ugettext as _
from mailer.models import Message, Subscriber, List
from mailer.models import HTML as default_html
import mailer.views
from polls.utils import token_url_for_email
from mailqueue.models import MailerMessage
import re
from textwrap import fill


class HookException(Exception):
	pass


def hooks (hook):
	if 'token_url_for_email|' in hook:
		if hook.count('|') != 1:
			raise HookException('misconfigured token hook')
		data = hook.split('|')
		return token_url_for_email( settings.PROTOCOL_SCHEME + '://' + settings.SITE_DOMAIN + data[1] )

	raise HookException('misconfigured hook')


def sendmail (subscriber, message):
	locale = subscriber.config_language
	activate_translation(locale)
	title = ''
	body = ''
	html = None
	message_data = {
		'protocol':     settings.PROTOCOL_SCHEME,
		'app_name':     settings.APP_MAILER_NAME[subscriber.config_language],
		'domain':       settings.SITE_DOMAIN,
		'app_manage':   reverse(mailer.views.manage),
		'access_token': subscriber.config_access_token,
	}
	title = message.localized_title(locale)
	body  = message.localized_message_text(locale)
	body += '\n\n'
	body += _('Url for subscription management and unsubscription:')
	body += '\n' + settings.PROTOCOL_SCHEME + '://' + settings.SITE_DOMAIN + reverse(mailer.views.manage) + '?token=' + subscriber.config_access_token

	if subscriber.config_send_html:
		message_data['message'] = message.localized_message_html(locale)
		if message_data['message'] != default_html:
			html = render_to_string('mailer/message.html', message_data)

	for hook in re.findall(r'\{{[^()]*\}}', body):
		hook_name = hook.replace('{{ ', '').replace(' }}', '').replace('{{', '').replace('}}', '')
		hook_cache = hooks(hook_name)
		body = body.replace(hook, hook_cache)
		if html:
			html = html.replace(hook, hook_cache)

	body = fill(body, replace_whitespace=False, expand_tabs=False, break_long_words=False, break_on_hyphens=False)
	body = fill(body, replace_whitespace=False, expand_tabs=False, break_long_words=True, break_on_hyphens=False, width=998)
	if html:
		html = fill(html, replace_whitespace=False, expand_tabs=False, break_long_words=False, break_on_hyphens=False)
		html = fill(html, replace_whitespace=False, expand_tabs=False, break_long_words=True, break_on_hyphens=False, width=998)

	email_message = MailerMessage()
	email_message.subject       = title
	email_message.content       = body
	email_message.from_address  = settings.DEFAULT_FROM_EMAIL
	email_message.to_address    = subscriber.email
	email_message.html_content  = html
	email_message.app           = 'mailer sendmessages'
	email_message.save()


class Command (BaseCommand):
	help = 'Send messages to subscribers. If you want to test it without really doing anything you should use both --fake and --nosave!'

	def add_arguments (self, parser):
		parser.add_argument('--fake',
			action='store_true',
			dest='fake',
			default=False,
			help='Only count, do not perform actual send')
		parser.add_argument('--nosave',
			action='store_true',
			dest='nosave',
			default=False,
			help='Do not save anything to database')
		parser.add_argument('--silent',
			action='store_true',
			dest='silent',
			default=False,
			help='Do not output as much messages')
		parser.add_argument('--supersilent',
			action='store_true',
			dest='supersilent',
			default=False,
			help='Do not output anything')


	def handle (self, *args, **options):
		send = not options['fake']
		save = not options['nosave']
		silent = options['silent'] or options['supersilent']
		supersilent = options['supersilent']
		task_start = timezone.now()
		mailed_subscribers = []
		subscribers = Subscriber.objects.filter(date_unsubscribed=None).exclude(date_confirmed=None)
		if not supersilent: self.stdout.write('Got ' + str(len(subscribers)) + ' active subscribers')
		if not silent: self.stdout.write('')

		for message in Message.objects.filter(ready=True, archived=False):
			if not silent:
				self.stdout.write('Processing message [' + str(message).decode('utf8') + ']:')
			else:
				if not supersilent: self.stdout.write('[' + str(message).decode('utf8') + ']:', ending='')
			uuid = str(message.list.uuid)
			do_not_archive_yet = False
			recipients = subscribers.filter(date_confirmed__lte=message.date_created)

			#go thru subscribers and send messages
			for recipient in recipients:
				if uuid in recipient.list_of_lists.split('|'):
					if not (recipient.date_lastmailed is None or recipient.date_lastmailed <= task_start - recipient.config_mail_delay_interval) and not message.urgent:
						# too soon to send a message
						if not silent: self.stdout.write('Recipient ' + recipient.email.decode('utf8') + ' is not ready yet (too soon since last message)')
						do_not_archive_yet = True
					elif recipient not in message.sent_to.all():
						# sending message
						mailed_subscribers.append(recipient)
						if not silent: self.stdout.write('Sending message to ' + recipient.email.decode('utf8'))
						if send: sendmail(recipient, message)
						if save: message.sent_to.add(recipient)
					else:
						# message was sent in past
						if not silent: self.stdout.write('Sending message to ' + recipient.email.decode('utf8') + ' in not required as it was already done')

			if do_not_archive_yet:
				if not silent:
					self.stdout.write('Message was not sent to every recipient thus will be resent later')
				else:
					if not supersilent: self.stdout.write('')
			else:
				if not silent:
					self.stdout.write('Message was sent to every recipient and going to be archived')
				else:
					if not supersilent: self.stdout.write(' [archived]')
				message.archived = True

			if save: message.save()
			if not silent: self.stdout.write('')

		# updade date_lastmailed for every mailed subscriber
		for subscriber in mailed_subscribers:
			subscriber.date_lastmailed = task_start
			if save: subscriber.save()

		if not supersilent: self.stdout.write(str(len(mailed_subscribers)) + ' messages sent')
