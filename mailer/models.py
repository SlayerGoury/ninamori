from django.conf import settings
from django.db import models
from tinymce import models as tinymce_models
import datetime
import uuid

HTML = '<p></p>'


class Subscriber (models.Model):
	email                       = models.EmailField()
	email_validated             = models.BooleanField(default=False)
	name                        = models.CharField(max_length=254, default='', blank=True)
	gender                      = models.CharField(max_length=2, choices=(("M", "Male"), ("F", "Female"), ("X", "Genderless")), default="X")
	date_subscribed             = models.DateTimeField(auto_now_add=True)
	date_confirmed              = models.DateTimeField(null=True, blank=True)
	date_lastmailed             = models.DateTimeField(null=True, blank=True)
	date_unsubscribed           = models.DateTimeField(null=True, blank=True)
	list_of_lists               = models.TextField(default="|")
	list_of_unconfirmed_lists   = models.TextField(default="|")
	config_send_html            = models.BooleanField(default=True)
	config_mail_delay_interval  = models.DurationField(default=datetime.timedelta(days=0))
	config_language             = models.CharField(max_length=4, choices=(("ru", "Russian"), ("en", "English")), default="ru")
	config_access_token         = models.CharField(max_length=200, default=None, blank=False, null=True)

	def __unicode__ (self):
		return self.email


class List (models.Model):
	name                        = models.CharField(max_length=254)
	public                      = models.BooleanField(default=False)
	default                     = models.BooleanField(default=False)
	description_ru              = models.CharField(max_length=254, default="")
	description_en              = models.CharField(max_length=254, default="")
	uuid                        = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)

	def localized_description (self, value):
		if value == 'ru': return self.description_ru
		if value == 'en': return self.description_en
		return self.localized_description(settings.LANGUAGE_CODE)

	def __unicode__ (self):
		return self.name


class Message (models.Model):
	list                        = models.ForeignKey(List, on_delete=models.CASCADE)
	sent_to                     = models.ManyToManyField(Subscriber, db_table="mailer_tracker", blank=True)
	title                       = models.CharField(max_length=254, default="")
	title_ru                    = models.CharField(max_length=254, default="", null=True, blank=True)
	title_en                    = models.CharField(max_length=254, default="", null=True, blank=True)
	urgent                      = models.BooleanField(default=False)
	message_text_ru             = models.TextField(default="", null=True, blank=True)
	message_text_en             = models.TextField(default="", null=True, blank=True)
	message_html_ru             = tinymce_models.HTMLField(default=HTML, null=True, blank=True)
	message_html_en             = tinymce_models.HTMLField(default=HTML, null=True, blank=True)
	date_created                = models.DateTimeField(auto_now=True)
	ready                       = models.BooleanField(default=False)
	archived                    = models.BooleanField(default=False)

	def localized_title (self, value):
		if value == 'ru' and self.title_ru: return self.title_ru
		if value == 'en' and self.title_en: return self.title_en
		if settings.LANGUAGE_CODE == 'ru' and self.title_ru: return self.title_ru
		if settings.LANGUAGE_CODE == 'en' and self.title_en: return self.title_en
		if self.title_ru: return self.title_ru
		if self.title_en: return self.title_en
		return ''

	def localized_message_text (self, value):
		if value == 'ru' and self.message_text_ru: return self.message_text_ru
		if value == 'en' and self.message_text_en: return self.message_text_en
		if settings.LANGUAGE_CODE == 'ru' and self.message_text_ru: return self.message_text_ru
		if settings.LANGUAGE_CODE == 'en' and self.message_text_en: return self.message_text_en
		if self.message_text_ru: return self.message_text_ru
		if self.message_text_en: return self.message_text_en
		return ''

	def localized_message_html (self, value):
		if value == 'ru' and self.message_html_ru and self.message_html_ru != HTML: return self.message_html_ru
		if value == 'en' and self.message_html_en and self.message_html_en != HTML: return self.message_html_en
		if (value == 'ru' and self.message_text_ru) or (value == 'en' and self.message_text_en): return HTML
		if settings.LANGUAGE_CODE == 'ru' and self.message_html_ru and self.message_html_ru != HTML: return self.message_html_ru
		if settings.LANGUAGE_CODE == 'en' and self.message_html_en and self.message_html_en != HTML: return self.message_html_en
		if self.message_html_ru and self.message_html_ru != HTML: return self.message_html_ru
		if self.message_html_en and self.message_html_en != HTML: return self.message_html_en
		return HTML

	def __unicode__ (self):
		return self.list.name + ' | ' + self.title


# this is one-time-use token for invite-only lists
class Token (models.Model):
	uuid           = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
	purpose        = models.CharField(max_length=30, default='admin')
	list           = models.ForeignKey(List, on_delete=models.CASCADE)

	def __unicode__ (self):
		return str(self.uuid)
