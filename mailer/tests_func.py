# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.auth.models import User
from django.core.management import call_command
from django.test import TestCase, Client
from models import Subscriber, List, Message, Token
import views

c = Client(enforce_csrf_checks=True)


class MailerTests (TestCase):
	def test_index_render (self):
		"""
		Can I render index page?
		"""
		response = c.get('/mailer/')
		self.assertEqual(response.status_code, 200)

	def test_subscribe_render_405 (self):
		"""
		Can I get 405 error from subscribe page?
		"""
		response = c.get('/mailer/subscribe')
		self.assertEqual(response.status_code, 405)

	def test_subscribe_bad_email_redirect (self):
		"""
		Can I get redirected by posting bad email?
		"""
		response_1 = c.get('/mailer/')
		response_2 = c.post('/mailer/subscribe', {'csrfmiddlewaretoken': c.cookies['csrftoken'].value, 'email': 'meow'})
		self.assertEqual(response_2.status_code, 302)

	def test_subscribe_good_render (self):
		"""
		Can I get success page render?
		"""
		response_1 = c.get('/mailer/')
		self.assertEqual(response_1.status_code, 200)
		response_2 = c.post('/mailer/subscribe', {'csrfmiddlewaretoken': c.cookies['csrftoken'].value, 'email': 'meow@meow.me'})
		self.assertEqual(response_2.status_code, 200)

	def test_subscribe_then_confirm_then_unsubscribe_then_resubscribe (self):
		"""
		Can I subscribe, confirm, unsubscribe, resubscribe?
		"""
		response_1 = c.get('/mailer/')
		self.assertEqual(response_1.status_code, 200)
		response_2 = c.post('/mailer/subscribe', {'csrfmiddlewaretoken': c.cookies['csrftoken'].value, 'email': 'meow@meow.me'})
		self.assertEqual(response_2.status_code, 200)
		subscriber = Subscriber.objects.get(email='meow@meow.me')
		response_3 = c.get('/mailer/manage?token=' + subscriber.config_access_token)
		self.assertEqual(response_3.status_code, 200)
		response_4 = c.get('/mailer/confirm?token=nope')
		self.assertEqual(response_4.status_code, 405)
		response_5 = c.get('/mailer/confirm?token=' + subscriber.config_access_token)
		self.assertEqual(response_5.status_code, 302)
		response_6 = c.get('/mailer/unsubscribe?token=nope')
		self.assertEqual(response_6.status_code, 405)
		response_7 = c.get('/mailer/unsubscribe?token=' + subscriber.config_access_token)
		self.assertEqual(response_7.status_code, 200)
		response_8 = c.post('/mailer/unsubscribe?token=' + subscriber.config_access_token, {'csrfmiddlewaretoken': c.cookies['csrftoken'].value, 'token': subscriber.config_access_token})
		self.assertEqual(response_8.status_code, 302)
		response_9 = c.get('/mailer/')
		self.assertEqual(response_9.status_code, 200)
		response_10 = c.post('/mailer/subscribe', {'csrfmiddlewaretoken': c.cookies['csrftoken'].value, 'email': 'meow@meow.me'})
		self.assertEqual(response_10.status_code, 302) # redirect 302 means email is not accepted

	def test_subscribe_then_then_resubscribe (self):
		"""
		Can I subscribe and resubscribe?
		"""
		response_1 = c.get('/mailer/')
		self.assertEqual(response_1.status_code, 200)
		response_2 = c.post('/mailer/subscribe', {'csrfmiddlewaretoken': c.cookies['csrftoken'].value, 'email': 'meow@meow.me', 'list_1234': 'yes'})
		self.assertEqual(response_2.status_code, 200)
		response_3 = c.get('/mailer/')
		self.assertEqual(response_3.status_code, 200)
		response_4 = c.post('/mailer/subscribe', {'csrfmiddlewaretoken': c.cookies['csrftoken'].value, 'email': 'meow@meow.me'})
		self.assertEqual(response_4.status_code, 200)

	def test_subscribe_invited (self):
		"""
		Can I subscribe invited and then confirm invite?
		"""
		list1 = List.objects.create(
			name = 'news',
			public = False,
			default = False,
			description_ru = 'meow',
			description_en = 'мяу',
		)
		list2 = List.objects.create(
			name = 'news',
			public = True,
			default = True,
			description_ru = 'новости',
			description_en = 'news',
		)
		token = Token.objects.create(list = list1)
		response_1 = c.get('/mailer/invited')
		self.assertEqual(response_1.status_code, 403)
		response_2 = c.get('/mailer/invited?token=' + str(token.uuid))
		self.assertEqual(response_2.status_code, 200)
		response_3 = c.get('/mailer/invited?token=' + str(token.uuid) + '&bad_email=yes')
		self.assertEqual(response_3.status_code, 200)
		response_4 = c.post('/mailer/invited?token=' + str(token.uuid), {'csrfmiddlewaretoken': c.cookies['csrftoken'].value, 'email': 'meow'})
		self.assertEqual(response_4.status_code, 302)
		response_5 = c.post('/mailer/invited?token=' + str(token.uuid), {'csrfmiddlewaretoken': c.cookies['csrftoken'].value, 'email': 'meow@meow.me'})
		self.assertEqual(response_5.status_code, 200)

		token = Token.objects.create(list = list1)
		response_1 = c.get('/mailer/')
		self.assertEqual(response_1.status_code, 200)
		response_2 = c.post('/mailer/subscribe', {'csrfmiddlewaretoken': c.cookies['csrftoken'].value, 'email': 'meow2@meow.me'})
		self.assertEqual(response_2.status_code, 200)
		response_3 = c.post('/mailer/invited?token=' + str(token.uuid), {'csrfmiddlewaretoken': c.cookies['csrftoken'].value, 'email': 'meow2@meow.me'})
		self.assertEqual(response_3.status_code, 200)

		subscriber = Subscriber.objects.get(email='meow2@meow.me')
		response_1 = c.get('/mailer/confirm_list')
		self.assertEqual(response_1.status_code, 405)
		response_2 = c.get('/mailer/confirm_list?token=' + subscriber.config_access_token)
		self.assertEqual(response_2.status_code, 405)
		response_3 = c.get('/mailer/manage?token=' + subscriber.config_access_token)
		self.assertEqual(response_3.status_code, 200)
		response_4 = c.get('/mailer/confirm_list?token=' + subscriber.config_access_token + '&list=' + str(list1.uuid))
		self.assertEqual(response_4.status_code, 302)
		response_5 = c.get('/mailer/manage?token=' + subscriber.config_access_token)
		self.assertEqual(response_5.status_code, 200)

	def test_subscribe_invited_after_unsubscribe (self):
		"""
		Can I subscribe invited after unsubscription?
		"""
		list1 = List.objects.create(
			name = 'news',
			public = False,
			default = False,
			description_ru = 'meow',
			description_en = 'мяу',
		)
		token = Token.objects.create(list = list1)
		response_1 = c.get('/mailer/')
		self.assertEqual(response_1.status_code, 200)
		response_2 = c.post('/mailer/subscribe', {'csrfmiddlewaretoken': c.cookies['csrftoken'].value, 'email': 'meow@meow.me'})
		self.assertEqual(response_2.status_code, 200)
		subscriber = Subscriber.objects.get(email='meow@meow.me')
		response_3 = c.get('/mailer/manage?token=' + subscriber.config_access_token)
		self.assertEqual(response_3.status_code, 200)
		response_4 = c.get('/mailer/confirm?token=' + subscriber.config_access_token)
		self.assertEqual(response_4.status_code, 302)
		response_5 = c.get('/mailer/unsubscribe?token=' + subscriber.config_access_token)
		self.assertEqual(response_5.status_code, 200)
		response_6 = c.post('/mailer/unsubscribe?token=' + subscriber.config_access_token, {'csrfmiddlewaretoken': c.cookies['csrftoken'].value, 'token': subscriber.config_access_token})
		self.assertEqual(response_6.status_code, 302) # unsubscribed

		response_7 = c.get('/mailer/invited?token=' + str(token.uuid))
		self.assertEqual(response_7.status_code, 200)
		response_8 = c.post('/mailer/invited?token=' + str(token.uuid), {'csrfmiddlewaretoken': c.cookies['csrftoken'].value, 'email': 'meow@meow.me'})
		self.assertEqual(response_8.status_code, 302)

	def test_subscribe_invited_if_already_invited (self):
		list1 = List.objects.create(
			name = 'news',
			public = False,
			default = False,
			description_ru = 'meow',
			description_en = 'мяу',
		)
		token = Token.objects.create(list = list1)
		response_7 = c.get('/mailer/invited?token=' + str(token.uuid))
		self.assertEqual(response_7.status_code, 200)
		response_8 = c.post('/mailer/invited?token=' + str(token.uuid), {'csrfmiddlewaretoken': c.cookies['csrftoken'].value, 'email': 'meow@meow.me'})
		self.assertEqual(response_8.status_code, 200)
		token = Token.objects.create(list = list1)
		response_8 = c.post('/mailer/invited?token=' + str(token.uuid), {'csrfmiddlewaretoken': c.cookies['csrftoken'].value, 'email': 'meow@meow.me'})
		self.assertEqual(response_8.status_code, 302)

	def test_subscribe_then_set_delay_to_zero_then_get_two_messages (self):
		"""
		Can I subscribe, set delay to zero and get two messages?
		"""
		list = List.objects.create(
			name = 'news',
			public = True,
			default = True,
			description_ru = 'новости',
			description_en = 'news',
		)
		response_1 = c.get('/mailer/')
		self.assertEqual(response_1.status_code, 200)
		response_2 = c.post('/mailer/subscribe', {'csrfmiddlewaretoken': c.cookies['csrftoken'].value, 'email': 'meow@meow.me'})
		self.assertEqual(response_2.status_code, 200)
		subscriber = Subscriber.objects.get(email='meow@meow.me')
		response = c.get('/mailer/manage')
		self.assertEqual(response.status_code, 405)
		response_3 = c.get('/mailer/manage?token=' + subscriber.config_access_token)
		self.assertEqual(response_3.status_code, 200)
		response_4 = c.get('/mailer/confirm?token=' + subscriber.config_access_token)
		self.assertEqual(response_4.status_code, 302)
		response = c.post('/mailer/manage?token=' + subscriber.config_access_token, {'csrfmiddlewaretoken': c.cookies['csrftoken'].value, 'config_mail_delay_interval': '0', 'list_'+str(list.uuid): 'yes'})
		self.assertEqual(response.status_code, 403)
		response_5 = c.post('/mailer/manage?token=' + subscriber.config_access_token, {'csrfmiddlewaretoken': c.cookies['csrftoken'].value, 'token': subscriber.config_access_token, 'config_mail_delay_interval': '65536', 'list_'+str(list.uuid): 'yes'})
		self.assertEqual(response_5.status_code, 200)
		response_6 = c.post('/mailer/manage?token=' + subscriber.config_access_token, {'csrfmiddlewaretoken': c.cookies['csrftoken'].value, 'token': subscriber.config_access_token, 'config_mail_delay_interval': '0', 'list_'+str(list.uuid): 'yes'})
		self.assertEqual(response_6.status_code, 200)
		message_1 = Message.objects.create(
			list                        = list,
			title                       = 'meow 1',
			title_ru                    = 'meow 1',
			title_en                    = 'meow 1',
			message_text_ru             = 'meow 1',
			message_text_en             = 'meow 1',
			message_html_ru             = 'meow 1',
			message_html_en             = 'meow 1',
		)
		message_2 = Message.objects.create(
			list                        = list,
			title                       = 'meow 2',
			title_ru                    = 'meow 2',
			title_en                    = 'meow 2',
			message_text_ru             = 'meow 2',
			message_text_en             = 'meow 2',
			message_html_ru             = 'meow 2',
			message_html_en             = 'meow 2',
		)
		message_1.ready = True
		message_1.save()
		output_1 = call_command('sendmessages', '--supersilent', '--fake')
		message_2.ready = True
		message_2.save()
		output_1 = call_command('sendmessages', '--supersilent', '--fake')
		message_1 = Message.objects.get(title='meow 1')
		message_2 = Message.objects.get(title='meow 2')
		self.assertEqual(message_1.sent_to.all()[0], subscriber)
		self.assertEqual(message_2.sent_to.all()[0], subscriber)
		self.assertEqual(message_1.archived, True)
		self.assertEqual(message_2.archived, True)

	def test_subscribe_to_private_list (self):
		"""
		Can I subscribe to private list?
		"""
		list = List.objects.create(
			name = 'news',
			public = False,
			default = False,
			description_ru = 'новости',
			description_en = 'news',
		)
		response_1 = c.get('/mailer/')
		self.assertEqual(response_1.status_code, 200)
		response_2 = c.post('/mailer/subscribe', {'csrfmiddlewaretoken': c.cookies['csrftoken'].value, 'email': 'meow@meow.me'})
		self.assertEqual(response_2.status_code, 200)
		subscriber = Subscriber.objects.get(email='meow@meow.me')
		response_3 = c.get('/mailer/manage?token=' + subscriber.config_access_token)
		self.assertEqual(response_3.status_code, 200)
		response_4 = c.get('/mailer/confirm?token=' + subscriber.config_access_token)
		self.assertEqual(response_4.status_code, 302)
		response_5 = c.post('/mailer/manage?token=' + subscriber.config_access_token, {'csrfmiddlewaretoken': c.cookies['csrftoken'].value, 'token': subscriber.config_access_token, 'config_mail_delay_interval': '1', 'list_'+str(list.uuid): 'yes'})
		self.assertEqual(response_5.status_code, 200)
		self.assertEqual(str(list.uuid) in subscriber.list_of_lists, False)

	def test_change_access_token (self):
		"""
		Can I change access token?
		"""
		response_1 = c.get('/mailer/')
		self.assertEqual(response_1.status_code, 200)
		response_2 = c.post('/mailer/subscribe', {'csrfmiddlewaretoken': c.cookies['csrftoken'].value, 'email': 'meow@meow.me'})
		self.assertEqual(response_2.status_code, 200)
		subscriber = Subscriber.objects.get(email='meow@meow.me')
		token1 = subscriber.config_access_token
		response_3 = c.post('/mailer/change_token', {'csrfmiddlewaretoken': c.cookies['csrftoken'].value, 'email': 'meow@meow.me'})
		self.assertEqual(response_3.status_code, 403)
		response_4 = c.post('/mailer/change_token', {'csrfmiddlewaretoken': c.cookies['csrftoken'].value, 'email': 'meow@meow.me', 'token': token1})
		self.assertEqual(response_4.status_code, 200)
		subscriber = Subscriber.objects.get(email='meow@meow.me')
		token2 = subscriber.config_access_token
		self.assertNotEqual(token1, token2)


	def test_send_two_normal_messages_and_one_urgent (self):
		"""
		Can I get two normal messages and one urgent?
		"""
		list = List.objects.create(
			name = 'news',
			public = True,
			default = True,
			description_ru = 'новости',
			description_en = 'news',
		)
		response = c.get('/mailer/')
		response = c.post('/mailer/subscribe', {'csrfmiddlewaretoken': c.cookies['csrftoken'].value, 'email': 'meow@meow.me'})
		subscriber = Subscriber.objects.get(email='meow@meow.me')
		response = c.get('/mailer/manage?token=' + subscriber.config_access_token)
		response = c.get('/mailer/confirm?token=' + subscriber.config_access_token)
		response = c.post('/mailer/manage?token=' + subscriber.config_access_token, {'csrfmiddlewaretoken': c.cookies['csrftoken'].value, 'token': subscriber.config_access_token, 'config_mail_delay_interval': '1', 'list_'+str(list.uuid): 'yes'})
		message = Message.objects.create(
			list                        = list,
			title                       = 'meow 1',
			title_ru                    = 'meow',
			title_en                    = 'meow',
			message_text_ru             = 'meow',
			message_text_en             = 'meow',
			message_html_ru             = 'meow',
			message_html_en             = 'meow',
		)
		message.ready = True
		message.save()
		output = call_command('sendmessages', '--supersilent', '--fake')
		message = Message.objects.get(title='meow 1')
		self.assertEqual(message.sent_to.all()[0], subscriber)
		self.assertEqual(message.archived, True)
		message = Message.objects.create(
			list                        = list,
			title                       = 'meow 2',
			title_ru                    = 'meow',
			title_en                    = 'meow',
			message_text_ru             = 'meow',
			message_text_en             = 'meow',
			message_html_ru             = 'meow',
			message_html_en             = 'meow',
		)
		message.ready = True
		message.urgent = True
		message.save()
		output = call_command('sendmessages', '--supersilent', '--fake')
		message = Message.objects.get(title='meow 2')
		self.assertEqual(message.sent_to.all()[0], subscriber)
		self.assertEqual(message.archived, True)
		message = Message.objects.create(
			list                        = list,
			title                       = 'meow 3',
			title_ru                    = 'meow',
			title_en                    = 'meow',
			message_text_ru             = '',
			message_text_en             = '{{token_url_for_email|/}}',
			message_html_ru             = '',
			message_html_en             = '',
		)
		message.ready = True
		message.save()
		output = call_command('sendmessages', '--supersilent', '--fake')
		message = Message.objects.get(title='meow 3')
		self.assertEqual(subscriber in message.sent_to.all(), False)
		self.assertEqual(message.archived, False)
