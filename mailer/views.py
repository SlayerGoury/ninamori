# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.conf import settings
from django.contrib.auth.models import User, Permission
from django.core.management import call_command
from django.shortcuts import render, redirect
from django.urls import reverse
from django.utils import translation
from django.utils.crypto import get_random_string
from django.utils.html import strip_tags
from django.utils.translation import ugettext as _
import accounts.views
from accounts.utils import is_email_access_validated, email_access_validate
from ninamori.utils import require_POST, has_enough_time_passed, uuid4hex
import datetime
from datetime import timedelta
import django.utils.timezone
import logging
import re

from models import Subscriber, List, Message, Token
from utils import get_list_of_lists, send_confirmation_email, send_new_token, send_invitation_email
from ninamori.utils import validate_email

logger = logging.getLogger(__name__)


def index (request):
	message = ''
	if request.user.is_authenticated() and request.user.userprofile.validation_email_valid:
		email = request.user.email
	else:
		email = ''
	lists = get_list_of_lists()
	if request.GET.get('bad_email') == 'yes':
		message = _('I don´t like this email')
	return render(request, 'mailer/index.html', {
		'lists': lists,
		'email': email,
		'message': message,
		'app_name': settings.APP_MAILER_NAME[request.LANGUAGE_CODE],
		'app_subscribe': reverse(subscribe),
	})


def get_subscriber_or_false (request):
	subscribers = Subscriber.objects.filter(config_access_token=request.POST.get('token'))
	if subscribers:
		return subscribers[0]
	else:
		return False


def generate_access_token (subscriber):
	return str(subscriber.id) + get_random_string(length=64)


@require_POST
def subscribe (request):
	if not validate_email(request.POST.get('email')):
		return redirect(reverse(index)+'?bad_email=yes')
	if Subscriber.objects.filter(email=request.POST.get('email')):
		#resend token
		subscriber = Subscriber.objects.get(email=request.POST.get('email'))
		if subscriber.date_unsubscribed:
			return redirect(reverse(index)+'?bad_email=yes')
		send_confirmation_email(subscriber, resubscribe=True)

	else:
		#perform subscribe
		list_of_lists = '|'
		for list_id in request.POST.items():
			if list_id[0][0:5] == 'list_':
				list_of_lists += list_id[0].replace('list_', '', 1) + '|'
		subscriber = Subscriber.objects.create(email=request.POST.get('email'))
		subscriber.list_of_lists = list_of_lists
		subscriber.config_access_token = generate_access_token(subscriber)
		subscriber.config_language = request.LANGUAGE_CODE
		subscriber.save()
		logger.info('[%s] subscribed' % subscriber.email)
		send_confirmation_email(subscriber, resubscribe=False)

	return render(request, 'mailer/success.html', {})


def subscribe_invited (request):
	token_uuid = request.GET.get('token')
	email = request.POST.get('email')
	if not (token_uuid and uuid4hex.match(token_uuid.replace('-', '')) and Token.objects.filter(uuid=token_uuid)):
		return render(request, '403.html', {'message': _('You should not access this page unless invited')}, status=403)
	token = Token.objects.get(uuid=token_uuid)

	if not request.method == 'POST':
		message = ''
		if request.GET.get('bad_email') == 'yes':
			message = _('I don´t like this email')
		return render(request, 'mailer/subscribe_invited.html', {
			'list_name': token.list.localized_description(request.LANGUAGE_CODE),
			'app_subscribe_invited': reverse(subscribe_invited),
			'token': token_uuid,
			'message': message,
		})

	if not validate_email(email):
		return redirect(reverse(index)+'?bad_email=yes')

	if Subscriber.objects.filter(email=email):
		#subscriber exists
		subscriber = Subscriber.objects.get(email=email)
		if subscriber.date_unsubscribed:
			return redirect(reverse(subscribe_invited)+'?token=' + token_uuid + '&bad_email=yes')
		list_of_lists = [list for list in subscriber.list_of_lists.split('|') if list != '']
		list_of_unconfirmed_lists = [list for list in subscriber.list_of_unconfirmed_lists.split('|') if list != '']
		if list_of_lists.count(str(token.list.uuid)) or list_of_unconfirmed_lists.count(str(token.list.uuid)):
			return redirect(reverse(subscribe_invited)+'?token=' + token_uuid + '&bad_email=yes')
		subscriber.list_of_unconfirmed_lists += str(token.list.uuid) + '|'
		subscriber.save()
		token.delete()
		send_invitation_email(subscriber, token=token)
		logger.info('[%s] invited to private list [%s]' % (subscriber.email, token.list.description_en))
	else:
		#new subscriber
		list_of_lists = '|'
		for list_item in List.objects.all():
			if list_item.default:
				list_of_lists += str(list_item.uuid) + '|'
		subscriber = Subscriber.objects.create(email=email)
		subscriber.list_of_lists = list_of_lists
		subscriber.list_of_unconfirmed_lists += str(token.list.uuid) + '|'
		subscriber.config_access_token = generate_access_token(subscriber)
		subscriber.config_language = request.LANGUAGE_CODE
		subscriber.save()
		send_invitation_email(subscriber, token=token, new_subscriber=True)
		logger.info('[%s] subscribed and invited to private list [%s]' % (subscriber.email, token.list.description_en))

	return render(request, 'mailer/success.html', {})


def confirm (request):
	token = request.GET.get('token')
	if not token or not Subscriber.objects.filter(config_access_token=token):
		return render(request, '404.html', {'message': _('You should not access this page directly')}, status=405)
	subscriber = Subscriber.objects.get(config_access_token=token)
	if not subscriber.date_confirmed:
		subscriber.date_confirmed = django.utils.timezone.now()
		subscriber.save()
		logger.info('[%s] confirmed subscription' % subscriber.email)
	return redirect(reverse(manage)+'?token=' + token)


def confirm_list (request):
	token = request.GET.get('token')
	if not token or not Subscriber.objects.filter(config_access_token=token):
		return render(request, '404.html', {'message': _('You should not access this page directly')}, status=405)
	subscriber = Subscriber.objects.get(config_access_token=token)
	list_of_unconfirmed_lists = [list for list in subscriber.list_of_unconfirmed_lists.split('|') if list != '']
	list_uuid = request.GET.get('list')
	if not list_uuid or not list_uuid in list_of_unconfirmed_lists:
		return render(request, '404.html', {'message': _('List uuid is missing')}, status=405)
	subscriber.list_of_lists += list_uuid + '|'
	subscriber.list_of_unconfirmed_lists = subscriber.list_of_unconfirmed_lists.replace(list_uuid + '|', '')
	subscriber.save()
	logger.info('[%s] confirmed subscription to private list [%s]' % (subscriber.email, list_uuid))
	return redirect(reverse(manage)+'?token=' + token)


def unsubscribe (request):
	if (not request.GET.get('token')) or (not Subscriber.objects.filter(config_access_token=request.GET.get('token'))):
		return render(request, '404.html', {'message': _('You should not access this page directly')}, status=405)
	if not request.POST.get('token'):
		return render(request, 'mailer/unsubscribe.html', {
			'token': request.GET.get('token'),
			'app_name': settings.APP_MAILER_NAME[request.LANGUAGE_CODE],
			'app_unsubscribe': reverse(unsubscribe),
			'app_manage': reverse(manage),
		})
	subscriber = Subscriber.objects.get(config_access_token=request.GET.get('token'))
	subscriber.date_unsubscribed = django.utils.timezone.now()
	subscriber.save()
	logger.info('[%s] unsubscribed' % subscriber.email)
	return redirect(reverse(manage)+'?token=' + subscriber.config_access_token)


def manage (request):
	if (not request.GET.get('token')) or (not Subscriber.objects.filter(config_access_token=request.GET.get('token'))):
		return render(request, '404.html', {'message': _('You should not access this page directly')}, status=405)
	if request.method == 'POST':
		if not update(request):
			return render(request, '403.html', status=403)
	subscriber = Subscriber.objects.get(config_access_token=request.GET.get('token'))
	email_access_validate(subscriber.email)

	list_of_lists = [list for list in subscriber.list_of_lists.split('|') if list != '']
	lists = []
	unconfirmed_lists = []
	for list in List.objects.all():
		if list.public:
			active = list_of_lists.count(str(list.uuid))
			lists.append((list.localized_description(request.LANGUAGE_CODE), list.uuid, active))
		elif list_of_lists.count(str(list.uuid)):
			active = 1
			lists.append((list.localized_description(request.LANGUAGE_CODE) + ' [' +_('private list') + ']', list.uuid, active))

	list_of_unconfirmed_lists = [list for list in subscriber.list_of_unconfirmed_lists.split('|') if list != '']
	for list in List.objects.all():
		if list_of_unconfirmed_lists.count(str(list.uuid)):
			unconfirmed_lists.append((list.localized_description(request.LANGUAGE_CODE), list.uuid))

	return render(request, 'mailer/manage.html', {
		'token': request.GET.get('token'),
		'email': subscriber.email,
		'lists': lists,
		'unconfirmed_lists': unconfirmed_lists,
		'config_language': subscriber.config_language,
		'config_mail_delay_interval': str(subscriber.config_mail_delay_interval.days),
		'config_send_html': subscriber.config_send_html,
		'name': subscriber.name,
		'gender': subscriber.gender,
		'date_confirmed': subscriber.date_confirmed,
		'date_unsubscribed': subscriber.date_unsubscribed,
		'updated': True if request.method == 'POST' else False,
		'app_name': settings.APP_MAILER_NAME[request.LANGUAGE_CODE],
		'app_confirm': reverse(confirm),
		'app_change_token': reverse(change_token),
		'app_confirm_list': reverse(confirm_list),
		'app_manage': reverse(manage),
		'app_unsubscribe': reverse(unsubscribe),
	})


def update (request):
	subscriber = get_subscriber_or_false(request)
	if not subscriber:
		return False

	subscriber.config_language = request.POST.get('config_language') if (request.POST.get('config_language') in ['ru', 'en']) else 'ru'
	subscriber.gender = request.POST.get('gender') if (request.POST.get('gender') in ['F', 'M', 'X']) else 'X'
	subscriber.config_send_html = True if request.POST.get('config_send_html') else False
	delay = request.POST.get('config_mail_delay_interval')
	subscriber.config_mail_delay_interval = datetime.timedelta(days=int(delay)) if delay.isdigit() else datetime.timedelta(days=1)
	if subscriber.config_mail_delay_interval > datetime.timedelta(days=30):
		subscriber.config_mail_delay_interval = datetime.timedelta(days=30)
	subscriber.name = strip_tags(request.POST.get('name')) if request.POST.get('name') else ''
	list_of_lists = '|'
	for list_id in request.POST.items():
		if list_id[0][0:5] == 'list_':
			list_uuid = list_id[0].replace('list_', '', 1)
			if uuid4hex.match(list_uuid.replace('-', '')) and List.objects.filter(uuid=list_uuid):
				if List.objects.get(uuid=list_uuid).public or list_uuid in subscriber.list_of_lists:
					list_of_lists += list_id[0].replace('list_', '', 1) + '|'
	subscriber.list_of_lists = list_of_lists
	subscriber.save()
	logger.info('[%s] [%s] updated settings' % (request.META.get('REMOTE_ADDR'), subscriber.email))
	return True


def change_token (request):
	subscriber = get_subscriber_or_false(request)
	if not subscriber:
		return render(request, '403.html', status=403)

	subscriber.config_access_token = generate_access_token(subscriber)
	subscriber.save()
	send_new_token(subscriber)
	return render(request, 'mailer/token_change.html')


# TODO use form for this def, decopypastation pas required
# Sdaly, there is not much to decopypastify unless refactor token generation in general
@require_POST
def generate_tokens (request):
	if not (request.user.has_perm('accounts.mailer_generate_tokens') or request.user.is_staff):
		return render(request, '403.html', {'message': _('You shall not pass')}, status=403)

	time = has_enough_time_passed(request.user.userprofile.mailer_tokens_last_generated, timedelta(hours=1))
	if not time:
		request.session['accounts_admin_index_message'] = _('Try again later')
		return redirect(reverse(accounts.views.admin_index))

	token_count = request.POST.get('token_count')
	purpose = 'Web request by ' + request.user.username
	email = request.user.email
	qr = request.POST.get('qr')
	qr6 = (qr == '6')
	qr24 = (qr == '24')
	uuid = request.POST.get('uuid')

	# TODO unhardcode such strict limitation as you refactor this thing to be async
	if not (token_count and token_count >= 48):
		return render(request, '403.html', {'message': _('Incorrect token count')}, status=400)

	if not is_email_access_validated(email):
		return render(request, '403.html', {'message': _('You should validate your email address first')}, status=403)

	if not uuid or not (uuid4hex.match(uuid.replace('-', '')) and List.objects.filter(uuid=uuid)):
		return render(request, '403.html', {'message': _('Wrong UUID')}, status=400)

	if qr and not (qr == '1' or qr6 or qr24):
		return render(request, '403.html', {'message': _('Incorrect qr per page value')}, status=400)

	request.user.userprofile.mailer_tokens_last_generated = time
	request.user.userprofile.save()
	call_command('generate_mailer_tokens', token_count=token_count, list_uuid=uuid, qr=bool(qr), qr6=qr6, qr24=qr24, email=email, purpose=purpose, supersilent=True)
	request.session['accounts_admin_index_message'] = _('Tokens generated and soon will be sent to you by email')
	return redirect(reverse(accounts.views.admin_index))
