# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.auth.models import User
from django.conf import settings
from django.urls import reverse
from django.middleware.csrf import get_token
from django.template.loader import render_to_string
from django.utils.translation import ugettext as _
from models import List, Token
import mailer.views
from accounts.utils import is_email_access_validated
from mailqueue.models import MailerMessage


def title_header (language):
	return '[' + settings.SITE_NAME[language] + '] [' + settings.APP_MAILER_NAME[language] + '] '


def get_list_of_lists ():
	lists = []
	for list in List.objects.all():
		if list.public:
			yield (list.name, list.uuid, list.default)


def get_subsrcibe_form_html (request):
	return render_to_string ('mailer/subscribe_form.html', {
		'app_subscribe': reverse(mailer.views.subscribe),
		'csrf_token': get_token(request),
		'lists': get_list_of_lists(),
	})


def get_data_for_email_template_render (subscriber):
	return {
		'protocol': settings.PROTOCOL_SCHEME,
		'site_domain': settings.SITE_DOMAIN,
		'app_name': settings.APP_MAILER_NAME[subscriber.config_language],
		'app_manage': reverse(mailer.views.manage),
		'token': subscriber.config_access_token,
		'email': subscriber.email,
	}


def send_confirmation_email (subscriber, resubscribe=False):
	title = title_header(subscriber.config_language)
	if resubscribe:
		template = 'mailer/resubscribe_email.txt'
		title += _('subscription token request')
	else:
		template = 'mailer/subscribe_email.txt'
		title += _('subscription confirmation')

	email_message = MailerMessage()
	email_message.subject       = title
	email_message.content       = render_to_string(template, get_data_for_email_template_render(subscriber))
	email_message.from_address  = settings.DEFAULT_FROM_EMAIL
	email_message.to_address    = subscriber.email
	email_message.app           = 'mailer confirmation email'
	email_message.save()
	return True


def send_invitation_email (subscriber, token, new_subscriber=False):
	title = title_header(subscriber.config_language)
	if new_subscriber:
		template = 'mailer/subscribe_and_invitation_email.txt'
		title += _('subscription confirmation')
	else:
		template = 'mailer/invitation_email.txt'
		title += _('private mailing list invitation')

	data = get_data_for_email_template_render(subscriber)
	data['list_description'] = token.list.localized_description(subscriber.config_language)

	email_message = MailerMessage()
	email_message.subject       = title
	email_message.content       = render_to_string(template, data)
	email_message.from_address  = settings.DEFAULT_FROM_EMAIL
	email_message.to_address    = subscriber.email
	email_message.app           = 'mailer invitation email'
	email_message.save()
	return True


def send_new_token (subscriber):
	template = 'mailer/send_new_token.txt'
	title = title_header(subscriber.config_language) + _('new access token')

	email_message = MailerMessage()
	email_message.subject       = title
	email_message.content       = render_to_string(template, get_data_for_email_template_render(subscriber))
	email_message.from_address  = settings.DEFAULT_FROM_EMAIL
	email_message.to_address    = subscriber.email
	email_message.app           = 'mailer new token'
	email_message.save()
	return True
