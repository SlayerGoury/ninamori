# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib import admin
from models import Message, Room


class MessageInline (admin.TabularInline):
	model = Message
	fieldsets = [
		(None, {'fields': ['author', 'text']}),
	]
	# TODO upgrade django to 1.11 and use this
	# classes = 'superslim'
	extra = 1


class RoomAdmin (admin.ModelAdmin):
	fieldsets = [
		(None, {'fields': ['name', 'motd', 'alias', 'allowed_groups']}),
	]
	list_display = ('name', 'alias')
	# TODO upgrade django to 1.11 and use this
	# inlines = [MessageInline]


admin.site.register(Room, RoomAdmin)
