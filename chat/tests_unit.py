# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.conf import settings
from django.contrib.auth.models import AnonymousUser, User, Permission, Group
from django.contrib.sessions.middleware import SessionMiddleware
from django.test import TestCase, RequestFactory
from ninamori.utils import cache_get, cache_set
from models import Room, Message
import views
import utils
from datetime import datetime, timedelta
import warnings

# TODO fix timezone awareness for messages
warnings.simplefilter("ignore")


class AccountsTests (TestCase):
	def setUp (self):
		self.factory = RequestFactory()
		self.session = SessionMiddleware()
		self.room = Room.objects.create(alias='test', name='test room')
		self.request = self.factory.get('/')
		self.request.user = AnonymousUser()
		self.request.LANGUAGE_CODE = 'en'

		Message.objects.create(room=self.room, text='meow meow')
		Message.objects.create(room=self.room, text='wazup')
		Message.objects.create(room=self.room, text='pew pew pew')
		Message.objects.create(room=self.room, text='nya')
		Message.objects.create(room=self.room, text='(=^_^=)')

		self.disabled_features = settings.DISABLED_FEATURES
		settings.DISABLED_FEATURES = []

	def tearDown(self):
		settings.DISABLED_FEATURES = self.disabled_features

	def test_models_room (self):
		room = Room.objects.create(alias='meow', name='purr')
		self.assertEqual(room.__unicode__(), 'purr')

	def test_models_message (self):
		message = Message.objects.create(room=self.room, text='murr')
		self.assertTrue(message.uuid)

	def test_utils_prepare_messages (self):
		query = Message.objects.order_by('-date_sent').filter(room=self.room)[:50]
		messages = utils.prepare_messages(reversed(query))
		self.assertEqual(len(messages), 5)

	def test_utils_user_have_access_to_room (self ):
		user = User.objects.create(username='meow')
		group = Group.objects.create(name='meow')
		room = Room.objects.create(alias='meow', name='purr')
		self.assertTrue(utils.user_have_access_to_room(AnonymousUser(), room))
		room.allowed_groups.add(group)
		self.assertFalse(utils.user_have_access_to_room(AnonymousUser(), room))
		self.assertFalse(utils.user_have_access_to_room(None, room))
		self.assertFalse(utils.user_have_access_to_room(user, room))
		user.groups.add(group)
		self.assertTrue(utils.user_have_access_to_room(user, room))

	def test_utils_update_and_get_userlist (self):
		userlist = utils.update_and_get_userlist('meow@meow.me', 'meow', 'test')
		self.assertTrue(userlist.get('meow'))
		userlist = utils.update_and_get_userlist('purr@purr.pu', 'purr', 'test')
		self.assertTrue(userlist.get('meow'))
		self.assertTrue(userlist.get('purr'))

		userlist = cache_get('chatroom_userlist_' + 'test')
		userlist['meow meow'] = {'gravatar': 'meow', 'last_seen': datetime.strftime(datetime.now() - timedelta(days=365), utils.CACHE_DATETIME_FORMAT)}
		cache_set('chatroom_userlist_' + 'test', userlist, 10)
		userlist = utils.update_and_get_userlist('purr@purr.pu', 'purr', 'test')
		self.assertFalse(userlist.get('meow meow'))

	def test_utils_nickname (self):
		request = self.factory.get('/')
		request.user = AnonymousUser()
		self.session.process_request(request)
		nickname = utils.nickname(request)
		self.assertTrue('[' in nickname and ']' in nickname)
		nickname = utils.nickname(request, 'meow')
		self.assertEqual(nickname, 'meow')
		nickname = utils.nickname(request)
		self.assertEqual(nickname, 'meow')

	def test_views_index (self):
		response = views.index(self.request)
		self.assertEqual(response.status_code, 200)

	def test_views_room (self):
		response = views.room(self.request, self.room.alias)
		self.assertEqual(response.status_code, 200)          # open room access is not restricted
		group = Group.objects.create(name='meow')
		room = Room.objects.create(alias='meow', name='purr')
		room.allowed_groups.add(group)
		response = views.room(self.request, room.alias)
		self.assertEqual(response.status_code, 403)          # group only room access is restricted
		user = User.objects.create(username='meow')
		user.groups.add(group)
		self.request.user = user
		response = views.room(self.request, room.alias)
		self.assertEqual(response.status_code, 200)          # group only room access is granted

	def test_views_ajax_message (self):
		group = Group.objects.create(name='meow')
		user = User.objects.create(username='meow')
		request = self.factory.post('/')
		request.POST.__dict__['_mutable'] = True
		request.user = user
		request.LANGUAGE_CODE = 'en'
		request.POST['message'] = 'meow'
		self.session.process_request(request)
		request.session['chat_antiflood'] = True             # should be autolifted because there is no reason for it to remain
		response = views.ajax_message(request, self.room.alias)
		message = Message.objects.order_by('-date_sent').filter(room=self.room)[0]
		self.assertEqual(message.text, 'meow')               # message is there
		self.room.allowed_groups.add(group)
		request.POST['message'] = 'meow2'
		response = views.ajax_message(request, self.room.alias)
		self.assertEqual(response.status_code, 403)          # group only room access is restricted
		user.groups.add(group)
		response = views.ajax_message(request, self.room.alias)
		message = Message.objects.order_by('-date_sent').filter(room=self.room)[0]
		self.assertEqual(message.text, 'meow2')              # now user should have access, so message is there
		request.POST['ajax'] = '1'
		request.POST['message'] = 'meow3'
		response = views.ajax_message(request, self.room.alias)
		message = Message.objects.order_by('-date_sent').filter(room=self.room)[0]
		self.assertEqual(message.text, 'meow3')              # ajax branch should also work
		response = views.ajax_message(request, self.room.alias)
		self.assertEqual(response.content, 'flood ban')      # flood ban
		self.assertTrue(request.session.get('chat_antiflood'))
		response = views.ajax_message(request, self.room.alias)
		self.assertEqual(response.content, 'flood ban')      # still flood ban (also for coverage reasons)


	def test_views_ajax_poll (self):
		group = Group.objects.create(name='meow')
		user = User.objects.create(username='meow')
		request = self.factory.post('/')
		request.user = user
		request.LANGUAGE_CODE = 'en'
		self.session.process_request(request)
		response = views.ajax_poll(request, self.room.alias)
		self.assertTrue('(=^_^=)' in response.content)
		self.assertFalse('meow meow' in response.content)
		message = Message.objects.order_by('-date_sent').filter(room=self.room)[3]
		request.GET.__dict__['_mutable'] = True
		request.GET['uuid'] = str(message.uuid)
		response = views.ajax_poll(request, self.room.alias)
		self.assertTrue('pew pew pew' in response.content)
		self.room.allowed_groups.add(group)
		response = views.ajax_poll(request, self.room.alias)
		self.assertEqual(response.status_code, 403)
		user.groups.add(group)
		response = views.ajax_poll(request, self.room.alias)
		self.assertEqual(response.status_code, 200)
		request.GET['uuid'] = 'meow'
		response = views.ajax_poll(request, self.room.alias)
		self.assertEqual(response.content, 'bad uuid')
