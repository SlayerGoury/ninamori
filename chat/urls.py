# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.conf.urls import include, url
import views


urlpatterns = [
	url(r'^$', views.index, name='chat index'),
	url(r'^(?P<alias>.+)/poll$', views.ajax_poll, name='chat ajax poll'),
	url(r'^(?P<alias>.+)/message$', views.ajax_message, name='chat ajax message'),
	url(r'^(?P<alias>.+)$', views.room, name='chat room'),
]
