# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.auth.models import User, Group
from django.db import models
import uuid


class Room (models.Model):
	alias                        = models.CharField(max_length=254, unique=True, blank=False)
	name                         = models.CharField(max_length=254, blank=False)
	motd                         = models.CharField(max_length=4098, blank=True)
	allowed_groups               = models.ManyToManyField(Group, db_table='chat_room_allowed_groups', blank=True)

	def __unicode__ (self):
		return self.name


class Message (models.Model):
	uuid                         = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
	room                         = models.ForeignKey(Room, on_delete=models.CASCADE)
	author                       = models.ForeignKey(User, null=True, on_delete=models.CASCADE)
	date_sent                    = models.DateTimeField(auto_now_add=True)
	text                         = models.CharField(max_length=4098, default='', blank=False)
