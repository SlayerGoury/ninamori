# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.http import HttpResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse
from django.utils.translation import ugettext as _
from ninamori.utils import require_POST, uuid4hex
from models import Message, Room
from utils import user_have_access_to_room, prepare_messages, update_and_get_userlist, nickname
from datetime import datetime, timedelta
import json

CACHE_DATETIME_FORMAT = '%H:%M:%S'


def index (request):
	rooms = Room.objects.all()
	result = []
	for room in rooms:
		if user_have_access_to_room(request.user, room):
			result.append(room)
	return render(request, 'chat/index.html', {'rooms': result})


def room (request, alias):
	room = get_object_or_404(Room, alias=alias)
	if not user_have_access_to_room(request.user, room):
		return render(request, '403.html', {'message': _('You don´t have permission to access this chat room')}, status=403)

	query = Message.objects.order_by('-date_sent').filter(room=room)[:50]
	messages = prepare_messages(reversed(query))

	return render(request, 'chat/room.html', {
		'room': room,
		'messages': messages,
		})


@require_POST
def ajax_message (request, alias):
	flood_ban = HttpResponse('flood ban', content_type='text/plain')
	room_obj = get_object_or_404(Room, alias=alias)
	user = None if request.user.is_anonymous() else request.user
	text = request.POST.get('message')

	if not user_have_access_to_room(user, room_obj):
		return HttpResponse('access denied', content_type='text/plain', status=403)

	# TODO more elegant antiflood
	if request.session.get('chat_antiflood'):
		if Message.objects.filter(date_sent__gte=datetime.today()-timedelta(minutes=5)).filter(room=room_obj, author=user).exists():
			return flood_ban
		else:
			request.session['chat_antiflood'] = False

	if text:
		text = text[:128]+'...' if len(text) > 4098 else text
		if Message.objects.filter(date_sent__gte=datetime.today()-timedelta(minutes=5)).filter(room=room_obj, text=text, author=user).exists():
			request.session['chat_antiflood'] = True
			return flood_ban
		Message.objects.create(author=user, text=text, room=room_obj)
	if request.POST.get('ajax'):
		return HttpResponse('accepted', content_type='text/plain')
	else:
		return redirect(reverse(room, kwargs={'alias': alias}))


def ajax_poll (request, alias):
	room_obj = get_object_or_404(Room, alias=alias)
	if not user_have_access_to_room(request.user, room_obj):
		return HttpResponse('access denied', content_type='text/plain', status=403)

	email = 'anonymous' if request.user.is_anonymous() else request.user.email
	name = nickname(request)
	userlist = update_and_get_userlist(email, name, alias)
	uuid = request.GET.get('uuid')
	if uuid:
		uuid = uuid.replace('-', '')
		if not uuid4hex.match(uuid):
			return HttpResponse('bad uuid', content_type='text/plain')
		message = get_object_or_404(Message, uuid=request.GET.get('uuid'))
		query = Message.objects.filter(room=room_obj, date_sent__gte=message.date_sent).order_by('-date_sent').exclude(uuid=message.uuid)[:50]
	else:
		query = Message.objects.filter(room=room_obj).order_by('-date_sent')[:1]

	return HttpResponse(json.dumps({
		'messages': prepare_messages(reversed(query)),
		'userlist': userlist,
	}), content_type='text/plain')
