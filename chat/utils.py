# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.conf import settings
from django.contrib.auth.models import AnonymousUser
from django.utils.crypto import get_random_string
from django.utils.html import escape
from django.utils.safestring import mark_safe
from accounts.templatetags.accounts_tags import get_avatar
from ninamori.utils import get_name, cache_set, cache_get, cache_delete
from datetime import datetime, timedelta
import bbcode

cache_settings = settings.CACHES['default']
CACHE_TIME_FORMAT = '%H:%M:%S'
CACHE_DATETIME_FORMAT = '%Y %m %d %H %M %S'


def prepare_messages (messages_obj):
	parser = bbcode.Parser(escape_html=False, replace_links=False, replace_cosmetic=False)
	messages = []
	for message in messages_obj:
		author = message.author if message.author else AnonymousUser()
		messages.append((
			str(message.uuid),
			datetime.strftime(message.date_sent, CACHE_TIME_FORMAT),
			escape(get_name(author)),
			mark_safe(parser.format(escape(message.text)).replace('<br />', '\\n')),
		))
	return messages


def user_have_access_to_room (user, room):
	allowed_groups = room.allowed_groups.all()
	if allowed_groups:
		if not user:
			return False
		if not set(user.groups.all()) & set(allowed_groups):
			return False
	return True


def update_and_get_userlist (email, name, alias):
	try:
		userlist = cache_get('chatroom_userlist_' + alias)
		assert(userlist)
	except AssertionError:
		userlist = {}

	gravatar = get_avatar(email, 32)
	userlist[name] = {'gravatar': gravatar, 'last_seen': datetime.strftime(datetime.now(), CACHE_DATETIME_FORMAT)}
	del_list = []
	for u in userlist:
		if datetime.strptime(userlist[u]['last_seen'], CACHE_DATETIME_FORMAT) + timedelta(seconds=10) < datetime.now():
			del_list.append(u)

	for u in del_list:
		del userlist[u]

	cache_set('chatroom_userlist_' + alias, userlist, cache_settings['SHORT_TIMEOUT'])
	return userlist


def nickname (request, nickname=None):
	if nickname or not request.session.get('chat_nickname'):

		if not nickname:
			if request.user.is_anonymous():
				nickname = get_name(request.user)+'_['+get_random_string(4)+']'
			else:
				nickname = get_name(request.user)

		request.session['chat_nickname'] = nickname
		request.session.modified = True
		return nickname

	return escape(request.session['chat_nickname'])
