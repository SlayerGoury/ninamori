from django.db import models


class YandexBalance (models.Model):
	issue            = models.CharField(max_length=254, default="Yet another issue")
	date_created     = models.DateTimeField(auto_now=True)
	amount_collected = models.CharField(max_length=254, default="0")
	test             = models.BooleanField(default=False)
