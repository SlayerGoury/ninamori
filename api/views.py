from __future__ import unicode_literals
from django.conf import settings
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render
from models import YandexBalance
import hashlib
import logging

logger = logging.getLogger(__name__)


@csrf_exempt
def yandex_inform (request):
	logger.debug('access: ' + str(request.POST.urlencode()))
	identificator = request.GET.get('id')
	amount = request.POST.get('amount')
	test = True if request.POST.get('test_notification') == 'true' else False

	if _test_hash(request):
		logger.info('yandex_inform successfull payment for %s RUB (sent by %s %s via %s) [%s], test=%s' % (
			request.POST.get('amount'),
			request.POST.get('sender'),
			request.POST.get('email'),
			request.POST.get('notification_type'),
			request.POST.get('operation_id'),
			str(test),
		))
		_add_money(identificator, amount, test)
		return HttpResponse("Yay, thanks", content_type="text/plain", status=200)

	return HttpResponse("What?", content_type="text/plain", status=403)


def _test_hash (request):
	hashtest = _assemble_hashtest(request)
	if request.POST.get('sha1_hash') == hashlib.sha1(hashtest).hexdigest() and (not request.POST.get('unaccepted') or request.POST.get('unaccepted') == 'false'):
		return True
	return False


def _assemble_hashtest (request):
	try:
		result = unicode(
			request.POST.get('notification_type') +
			'&' + request.POST.get('operation_id') +
			'&' + request.POST.get('amount') +
			'&' + request.POST.get('currency') +
			'&' + request.POST.get('datetime') +
			'&' + request.POST.get('sender') +
			'&' + request.POST.get('codepro') +
			'&' + settings.YANDEX_MONEY_SECRET +
			'&' + request.POST.get('label')
		)
	except TypeError:
		result = ''
	return result


def _add_money (identificator, amount, test):
	issue = None
	if identificator:
		try:
			issue = YandexBalance.objects.get(id=identificator)
		except YandexBalance.DoesNotExist:
			logger.warning('requested isue (id=%s) does not exist' % identificator)
	if not issue:
		issue = YandexBalance.objects.latest('date_created')

	if test:
		if not issue.test:
			return False

	issue.amount_collected = unicode(str( float(issue.amount_collected) + float(amount) ))
	issue.save()
	return True
