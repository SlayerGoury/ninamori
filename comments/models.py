# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.auth.models import User, AnonymousUser
from django.db import models
from ninamori.utils import get_name
from jsonfield import JSONField
import datetime
import uuid

CACHE_DATETIME_FORMAT = '%Y %m %d %H %M'


class Comment (models.Model):
	uuid               = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
	group_alias        = models.CharField(max_length=254, null=True, editable=False)
	approved           = models.BooleanField(default=False)
	deleted            = models.BooleanField(default=False)
	childs             = JSONField(default=[])
	parent             = models.ForeignKey("Comment", null=True, on_delete=models.CASCADE)
	content            = models.TextField()
	user               = models.ForeignKey(User, null=True, on_delete=models.CASCADE)
	email              = models.EmailField(null=True)  # TODO deprecate this (for now email is used as comment authenticity token instead of user for historical reasons)
	email_updates      = models.BooleanField(default=True)
	date_added         = models.DateTimeField(auto_now_add=True)
	http_referrer      = models.CharField(max_length=4096, null=True)
	user_ip            = models.CharField(max_length=46)
	user_agent         = models.CharField(max_length=4096)
	user_language      = models.CharField(max_length=4)
	akismet_spam       = models.NullBooleanField(default=None)
	manual_spam        = models.NullBooleanField(default=None)

	# TODO deprecate this
	def get_commentator_name (self):
		try:
			user = self.user
		except User.DoesNotExist:
			user = AnonymousUser()
		return get_name(user)

	def tupleize_comment (self, level=0):
		if self.akismet_spam is True and self.manual_spam is False:
			self.akismet_spam = None
		return (
			self.approved,
			self.childs,
			self.content,
			self.email,
			level,
			self.date_added.strftime(CACHE_DATETIME_FORMAT),
			self.user_ip,
			self.user_agent,
			self.user_language,
			self.deleted,
			self.email_updates,
			self.get_commentator_name(),
			self.akismet_spam,
			self.manual_spam,
		)

	def update_group_alias (self):
		comment = self
		if comment.group_alias:
			return comment.group_alias
		while comment.parent:
			comment = comment.parent
		for group in CommentsGroup.objects.all():
			if str(comment.uuid) in group.comments:
				self.group_alias = group.alias
				self.save()
				return self.group_alias
		return False

	def update_user (self):
		if not self.email:
			return False
		try:
			user = User.objects.get(email=self.email)
			self.user = user
			self.save()
			return True
		except User.DoesNotExist:
			return False

	def __unicode__ (self):
		return 'comment %s' % self.uuid


class Commentator (models.Model):
	email              = models.EmailField(primary_key=True)
	language           = models.CharField(max_length=4, choices=(("ru", "Russian"), ("en", "English")), default="ru")
	notification_queue = JSONField(default=[])


class CommentsGroup (models.Model):
	alias              = models.CharField(primary_key=True, max_length=254, unique=True, null=False, editable=False)
	allow_anonymous    = models.BooleanField(default=False)
	mode               = models.CharField(max_length=6, choices=(
		('tree', 'Tree'),
		('line', 'Linear'),
	), default='tree')
	premoderation_mode = models.CharField(max_length=5, choices=(
		('off', 'Off'),
		('one', 'Premoderation until user have first approved comment'),
		('evr', 'Premoderation for every comment'),
		('dis', 'Commentary disabled'),
	), default='evr')
	sorting_mode       = models.CharField(max_length=5, choices=(
		('top', 'New comments goes top'),
		('bot', 'New comments goes bottom'),
	), default='top')
	comments           = JSONField(default=[])
	watchers           = models.ManyToManyField(Commentator, db_table="comments_group_watchers", blank=True)

	def __unicode__ (self):
		return 'comments group %s' % self.alias
