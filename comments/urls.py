from django.conf.urls import include, url
import views


urlpatterns = [
	url(r'^$', views.comment, name='comments comment'),
	url(r'moderate$', views.moderate, name='comments moderate'),
	url(r'edit$', views.edit, name='comments edit'),
	url(r'watch$', views.watch, name='comments watch'),
	url(r'watch_group$', views.watch_group, name='comments watch group'),
	url(r'unfold$', views.unfold, name='comments unfold'),
	url(r'unfold_ajax$', views.unfold_ajax, name='comments unfold ajax'),
	url(r'ajax_branch$', views.ajax_branch, name='comments ajax branch'),
]
