# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.auth.models import Permission, User
from django.core.exceptions import ValidationError
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, redirect
from django.utils import timezone
from django.utils.translation import ugettext as _
from forms import comment_form, edit_form
from models import Comment, CommentsGroup, Commentator
from cms.models import Page
from ninamori.utils import cache_delete, require_POST
from ninamori import akismet
from utils import add_comment, edit_comment, get_comments_branch_html
from datetime import timedelta
import json
from urllib import quote_plus


def user_have_access_to_page (user, alias):
	try:
		page = Page.objects.get(alias=alias)
		allowed_groups = page.allowed_groups.all()
		if allowed_groups:
			if not set(user.groups.all()) & set(allowed_groups):
				return False
	except Page.DoesNotExist:
		pass
	return True


@require_POST
def comment (request):
	form = comment_form(request.POST or None)
	if not form.is_valid():
		return render(request, '403.html', {'message': _('Comment form is invalid')}, status=400)

	data = form.cleaned_data
	comments_group_alias = data['comments_group_alias']
	parent_uuid = data['parent_uuid']
	content = data['content']
	email_updates = data['email_updates']
	user = request.user
	parent = Comment.objects.get(uuid=parent_uuid) if parent_uuid else None

	try:
		comments_group = CommentsGroup.objects.get(alias=comments_group_alias)
	except CommentsGroup.DoesNotExist:
		return render(request, '403.html', {'message': _('Commentary not allowed here')}, status=400)
	if comments_group.premoderation_mode == 'dis':
		return render(request, '403.html', {'message': _('Commentary is disabled')}, status=403)
	if user.is_authenticated() and not user.userprofile.validation_email_valid:
		return render(request, '403.html', {'message': _('Commentary is disabled until email address is validated')}, status=403)

	if not user_have_access_to_page(request.user, comments_group.alias):
		return render(request, '403.html', {'message': _('You shall not pass')}, status=400)

	if comments_group.mode == 'line':
		email_updates = False

	comment = add_comment(user, comments_group, parent, content, request.META.get('REMOTE_ADDR'), request.META.get('HTTP_USER_AGENT'), request.LANGUAGE_CODE, email_updates, request.META.get('HTTP_REFERER'))
	if not request.session.get('comments_own'):
		request.session['comments_own'] = []
	request.session['comments_own'].append(str(comment.uuid))
	request.session.modified = True
	url = comments_group_alias + '#comment_' + str(comment.uuid)
	result = redirect(url)

	if comments_group.mode == 'tree':
		uuid = str(parent_uuid)
		if uuid in comments_group.comments:
			if not request.session.get('comments_branch_'+uuid+'unfolded'):
				request.session['comments_branch_'+uuid+'unfolded'] = True

	return result


@require_POST
def edit (request):
	form = edit_form(request.POST or None)
	if form.is_valid():
		data = form.cleaned_data
		comments_group_alias = data['comments_group_alias']
		content = data['content']
		email_updates = data['email_updates']
		user = request.user

		try:
			comments_group = CommentsGroup.objects.get(alias=comments_group_alias)
			comment = Comment.objects.get(uuid=data['comment_uuid'])
		except (CommentsGroup.DoesNotExist, Comment.DoesNotExist):
			return render(request, '404.html', {'message': _('Bad request')}, status=400)
		comments_own = request.session.get('comments_own')

		if (comments_own and ((str(data['comment_uuid']) not in comments_own) and request.user.is_anonymous()) \
		or (request.user.is_anonymous() and comment.email is not None) \
		or not (user.email and comment.email == user.email and user.userprofile.validation_email_valid)) \
		and not user.is_superuser:
			return render(request, '403.html', {'message': _('This might be not yours')}, status=403)

		if timezone.now() - comment.date_added > timedelta(hours=1) and not user.is_superuser:
			return render(request, '404.html', {'message': _('It is too late to edit this comment')}, status=418)
		if comments_group.premoderation_mode == 'dis':
			return render(request, '403.html', {'message': _('Commentary is disabled and thus can no longer be edited')}, status=403)

		edit_comment(comment, comments_group, content, email_updates)
		url = comments_group_alias + '#comment_' + str(comment.uuid)
		result = redirect(url)
		return result

	return render(request, '404.html', {'message': _('Comment form is invalid')}, status=400)


@require_POST
def watch (request):
	# TODO decopypastation required
	try:
		comment = Comment.objects.get(uuid=request.POST.get('uuid'))
		comments_group = CommentsGroup.objects.get(alias=request.POST.get('alias'))
		action = request.POST.get('action')
		assert(action in ['watch', 'unwatch'])
	except (Comment.DoesNotExist, CommentsGroup.DoesNotExist, ValueError, AssertionError):
		return HttpResponse('{"uuid": "%s", "status": "error"}' % str(comment.uuid), content_type='text/plain')

	if request.user.has_perm('accounts.comments_moderate') or not request.user.has_perm('accounts.comments_delete'):
		if not request.user.is_staff:
			if not request.user.is_authenticated() or not request.user.email == comment.email:
				return render(request, '403.html', {'message': _('You should go watch somewhere else')}, status=403)

	if comments_group.mode == 'line':
		return render(request, '404.html', {'message': _('Linear comments is not wathcable')}, status=400)

	if action == 'watch':
		return watch_watch(comment, comments_group)
	if action == 'unwatch':
		return watch_unwatch(comment, comments_group)


@require_POST
def watch_group (request):
	try:
		comments_group = CommentsGroup.objects.get(alias=request.POST.get('alias'))
		action = request.POST.get('action')
		assert(action in ['watch', 'unwatch'])
	except (CommentsGroup.DoesNotExist, ValueError, AssertionError):
		return HttpResponse('{"group": "%s", "status": "error"}' % request.POST.get('alias'), content_type='text/plain')

	if not request.user.userprofile.validation_email_valid:
		return render(request, '403.html', {'message': _('You should go watch yourself')}, status=403)

	commentator = Commentator.objects.get_or_create(email=request.user.email)[0]

	if action == 'watch':
		return watch_group_watch(commentator, comments_group)
	if action == 'unwatch':
		return watch_group_unwatch(commentator, comments_group)


@require_POST
def moderate (request):
	if not request.user.has_perm('accounts.comments_moderate') and not request.user.is_staff:
		return render(request, '403.html', {'message': _('You should go hack somewhere else')}, status=403)

	try:
		comment = Comment.objects.get(uuid=request.POST.get('uuid'))
		comments_group = CommentsGroup.objects.get(alias=request.POST.get('alias'))
		action = request.POST.get('action')
		assert(action in ['approve', 'unapprove', 'delete', 'undelete', 'spam', 'unspam'])
	except (Comment.DoesNotExist, CommentsGroup.DoesNotExist, ValueError, AssertionError):
		return HttpResponse('{"uuid": "%s", "status": "error"}' % str(comment.uuid), content_type='text/plain')

	if not request.user.has_perm('accounts.comments_delete') and not request.user.is_staff and action in ['delete', 'undelete']:
		# TODO this should be reported
		return render(request, '403.html', {'message': _('You should go hack somewhere else')}, status=403)

	if action == 'approve':
		return approve(comment, comments_group, request.user.id)
	if action == 'unapprove':
		return unapprove(comment, comments_group)
	if action == 'delete':
		return delete(comment, comments_group)
	if action == 'undelete':
		return undelete(comment, comments_group)
	if action == 'spam':
		return spam(comment, comments_group)
	if action == 'unspam':
		return unspam(comment, comments_group)


@require_POST
def unfold (request):
	if not request.POST.get('alias'):
		return render(request, '404.html', {'message': _('Alias is missing')}, status=400)
	alias=request.POST.get('alias')
	response = redirect(alias)
	if not request.session.get('comments_'+alias+'unfolded'):
		request.session['comments_'+alias+'unfolded'] = True
		response.set_cookie(quote_plus('comments_'+alias+'unfolded'), True, max_age=3*24*60*60)
	else:
		request.session['comments_'+alias+'unfolded'] = False
		response.delete_cookie(quote_plus('comments_'+alias+'unfolded'))
	return response


@require_POST
def unfold_ajax (request):
	uuid = request.POST.get('uuid')
	response = HttpResponse(content_type='text/plain')
	try:
		comment = Comment.objects.get(uuid=uuid)
		comments_group = CommentsGroup.objects.get(alias=request.POST.get('alias'))
		assert(comments_group.mode == 'tree')
		assert(comment.group_alias == comments_group.alias)
	except (Comment.DoesNotExist, CommentsGroup.DoesNotExist, ValueError, AssertionError, ValidationError):
		response.content = '{"branch": "%s", "status": "error"}' % uuid
		return response

	if request.session.get('comments_branch_'+uuid+'unfolded'):
		request.session['comments_branch_'+uuid+'unfolded'] = False
		response.delete_cookie(quote_plus('comments_branch_'+uuid+'unfolded'))
		response.content = '{"branch": "%s", "status": "folded"}' % uuid
	else:
		request.session['comments_branch_'+uuid+'unfolded'] = True
		response.set_cookie(quote_plus('comments_branch_'+uuid+'unfolded'), True, max_age=3*24*60*60)
		response.content = '{"branch": "%s", "status": "unfolded"}' % uuid

	return response


@require_POST
def ajax_branch (request):
	user = request.user
	try:
		comment = Comment.objects.get(uuid=request.POST.get('uuid'))
		comments_group = CommentsGroup.objects.get(alias=request.POST.get('alias'))
		assert(comments_group.mode == 'tree')
		assert(comment.group_alias == comments_group.alias)
	except (Comment.DoesNotExist, CommentsGroup.DoesNotExist, ValueError, AssertionError, ValidationError):
		return render(request, '404.html', {'message': _('Bad request')}, status=400)
	if not user_have_access_to_page(user, comments_group.alias):
		return render(request, '403.html', {'message': _('You shall not pass')}, status=400)
	user_email = user.email if user.is_authenticated() else None
	user_email_valid = user.userprofile.validation_email_valid if user.is_authenticated() else False
	return get_comments_branch_html(comments_group, comment, user_email, user_email_valid, request)


def approve (comment, comments_group, user_id):
	if comment.approved:
		status = 'approved'
	elif comment.parent and not comment.parent.approved:
		status = 'parent unapproved'
	else:
		status = 'approved'
		comment.approved = True
		comment.save()
		cache_delete('comments_' + comments_group.alias)
		if comment.email:
			try:
				user = User.objects.get(email=comment.email)
			except User.DoesNotExist:
				user = None
			if user and not user.has_perm('accounts.comments_approved'):
				permission = Permission.objects.get(codename='comments_approved')
				user.user_permissions.add(permission)
				user.approved_by = user_id
				user.save()
	return HttpResponse('{"uuid": "%s", "status": "%s"}' % (str(comment.uuid), status), content_type='text/plain')


def unapprove (comment, comments_group):
	if not comment.approved:
		status = 'unapproved'
	elif comment.childs != []:
		status = 'have childs'
	else:
		status = 'unapproved'
		comment.approved = False
		comment.save()
		cache_delete('comments_' + comments_group.alias)
	return HttpResponse('{"uuid": "%s", "status": "%s"}' % (str(comment.uuid), status), content_type='text/plain')


def spam (comment, comments_group):
	if not comment.akismet_spam and not comment.manual_spam:
		status = 'spam'
		comment.manual_spam = True
		if comment.akismet_spam is False:
			akismet.submit_false_comment(comment, comments_group.alias, spam=True)
			comment.akismet_spam = None
		comment.save()
		cache_delete('comments_' + comments_group.alias)
	else:
		status = 'error'
	return HttpResponse('{"uuid": "%s", "status": "%s"}' % (str(comment.uuid), status), content_type='text/plain')


def unspam (comment, comments_group):
	if comment.akismet_spam or comment.manual_spam:
		status = 'unspam'
		comment.manual_spam = False
		if comment.akismet_spam:
			akismet.submit_false_comment(comment, comments_group.alias, ham=True)
			comment.akismet_spam = None
		comment.save()
		cache_delete('comments_' + comments_group.alias)
	else:
		status = 'error'
	return HttpResponse('{"uuid": "%s", "status": "%s"}' % (str(comment.uuid), status), content_type='text/plain')


def delete (comment, comments_group):
	deleted_childs = []
	if comment.deleted:
		status = 'deleted'
	if comment.childs != []:
		deleted_childs = delete_childs(comment)
	status = 'deleted'
	comment.deleted = True
	comment.email_updates = False
	comment.save()
	cache_delete('comments_' + comments_group.alias)
	return HttpResponse('{"uuid": "%s", "status": "%s", "deleted_childs": "%s"}' % (str(comment.uuid), status, json.dumps(deleted_childs).replace('"', '\\"')), content_type='text/plain')


def undelete (comment, comments_group):
	if not comment.deleted:
		status = 'undeleted'
	if comment.parent and comment.parent.deleted:
		status = 'parent deleted'
	else:
		status = 'undeleted'
		comment.deleted = False
		comment.save()
		cache_delete('comments_' + comments_group.alias)
	return HttpResponse('{"uuid": "%s", "status": "%s"}' % (str(comment.uuid), status), content_type='text/plain')


def delete_childs (comment):
	deleted_childs = []
	for child_uuid in comment.childs:
		child = Comment.objects.get(uuid=child_uuid)
		child.deleted = True
		child.email_updates = False
		child.save()
		deleted_childs.extend([str(child.uuid)])
		if child.childs != []:
			extra_childs = delete_childs(child)
			deleted_childs.extend(extra_childs)
	return deleted_childs


def watch_watch(comment, comments_group):
	if not comment.email_updates:
		comment.email_updates = True
		comment.save()
	status = 'watched'
	return HttpResponse('{"uuid": "%s", "status": "%s"}' % (str(comment.uuid), status), content_type='text/plain')


def watch_unwatch(comment, comments_group):
	if comment.email_updates:
		comment.email_updates = False
		comment.save()
	status = 'unwatched'
	return HttpResponse('{"uuid": "%s", "status": "%s"}' % (str(comment.uuid), status), content_type='text/plain')


def watch_group_watch (commentator, comments_group):
	if not comments_group.watchers.filter(email=commentator.email):
		comments_group.watchers.add(commentator)
		return HttpResponse('{"group": "%s", "status": "%s"}' % (comments_group.alias, 'watched'), content_type='text/plain')
	else:
		return HttpResponse('{"group": "%s", "status": "%s"}' % (comments_group.alias, 'watched'), content_type='text/plain')


def watch_group_unwatch (commentator, comments_group):
	if comments_group.watchers.filter(email=commentator.email):
		comments_group.watchers.remove(commentator)
		return HttpResponse('{"group": "%s", "status": "%s"}' % (comments_group.alias, 'unwatched'), content_type='text/plain')
	else:
		return HttpResponse('{"group": "%s", "status": "%s"}' % (comments_group.alias, 'unwatched'), content_type='text/plain')
