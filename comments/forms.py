# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django import forms
from django.conf import settings
from django.utils.translation import ugettext as _

error_messages = {
	'comments_group_alias': {
		'required': _('Comments group alias is missing'),
	},
	'content': {
		'required': _('Commentary text is required'),
	},
	'parent_uuid': {
		'invalid': _('Parent comment uuid is invalid')
	},
	'comment_uuid': {
		'required': _('Comment uuid is required'),
		'invalid': _('Comment uuid is invalid')
	},
}


class comment_form (forms.Form):
	comments_group_alias  = forms.CharField(error_messages=error_messages['comments_group_alias'],  required=True,  widget=forms.HiddenInput())
	content               = forms.CharField(error_messages=error_messages['content'],               required=True,  widget=forms.Textarea( attrs={'class': 'comment-textarea', 'rows': '1', 'onkeyup': 'adjust_textarea_height(this);'} ))
	email_updates         = forms.BooleanField(                                                     required=False, widget=forms.CheckboxInput())
	parent_uuid           = forms.UUIDField(error_messages=error_messages['parent_uuid'],           required=False, widget=forms.HiddenInput( attrs={'class': 'parent_uuid'} ))

class edit_form (forms.Form):
	comments_group_alias  = forms.CharField(error_messages=error_messages['comments_group_alias'],  required=True,  widget=forms.HiddenInput( attrs={'id': 'id_edit_comments_group_alias'} ))
	comment_uuid          = forms.UUIDField(error_messages=error_messages['comment_uuid'],          required=True,  widget=forms.HiddenInput( attrs={'id': 'id_edit_comment_uuid'} ))
	content               = forms.CharField(error_messages=error_messages['content'],               required=True,  widget=forms.Textarea( attrs={'id': 'id_edit_content', 'class': 'comment-textarea', 'rows': '1', 'onkeyup': 'adjust_textarea_height(this);'} ))
	email_updates         = forms.BooleanField(                                                     required=False, widget=forms.CheckboxInput( attrs={'id': 'id_edit_email_updates'} ))
