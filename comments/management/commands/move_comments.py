# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.core.management.base import BaseCommand, CommandError
from comments.models import Comment, CommentsGroup
from comments.utils import move_comments, move_all_coments


class Command (BaseCommand):
	help = 'Move branch of comments or entire tree somewhere else'

	def add_arguments (self, parser):
		parser.add_argument('--comment',
			dest='comment_uuid',
			default=None,
			help='UUID of first comment in branch to be moved')
		parser.add_argument('--group',
			dest='group_alias',
			default=None,
			help='Alias of group to be moved')
		parser.add_argument('--destination_comment',
			dest='destination_uuid',
			default=None,
			help='UUID of comment for branch to be moved to')
		parser.add_argument('--destination_group',
			dest='destination_alias',
			default=None,
			help='Alias of group for branch to be moved to')

	def handle(self, *args, **options):
		if options['comment_uuid'] and options['group_alias']:
			raise CommandError('Move one thing at time')
		if options['destination_uuid'] and options['destination_alias']:
			raise CommandError('You can only move comments to one place')
		if not options['comment_uuid'] and not options['group_alias']:
			raise CommandError('Please specify what to move')
		if not options['destination_uuid'] and not options['destination_alias']:
			raise CommandError('Please specify where to move')

		if options['comment_uuid']:
			source = Comment.objects.get(uuid=options['comment_uuid'])
		else:
			source = CommentsGroup.objects.get(alias=options['group_alias'])

		if options['destination_uuid']:
			destination = Comment.objects.get(uuid=options['destination_uuid'])
		else:
			destination = CommentsGroup.objects.get(alias=options['destination_alias'])

		if options['comment_uuid']:
			result = move_comments(source, destination)
		else:
			result = move_all_coments(source, destination)

		if result:
			self.stdout.write('Moved %s to %s' % (source, destination))
		else:
			self.stdout.write('Failed moving %s to %s' % (source, destination))
