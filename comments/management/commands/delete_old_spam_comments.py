# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.core.management.base import BaseCommand, CommandError
from django.db import transaction
from django.db.models import Q
from datetime import datetime, timedelta
from comments.models import CommentsGroup, Comment, Commentator
from ninamori.utils import cache_delete


class Command (BaseCommand):
	help = 'Check every unckecked comment for spam by sending them to Akismet service'

	def add_arguments (self, parser):
		parser.add_argument('--silent',
			action='store_true',
			dest='silent',
			default=False,
			help='Do not output as much messages')
		parser.add_argument('--verbose',
			action='store_true',
			dest='verbose',
			default=False,
			help='Output content')
		parser.add_argument('--days',
			dest='days',
			default='30',
			help='Comment older that this to be deleted')

	def handle(self, *args, **options):
		silent = options['silent']
		days = options['days']
		verbose = options['verbose']
		comments_groups_aliases = []
		comments_list = Comment.objects.filter(date_added__lte=datetime.now()-timedelta(days=int(days)))
		comments_list = comments_list.filter(Q(akismet_spam=True)|Q(manual_spam=True))
		counter_comments = len(comments_list)
		for comment in comments_list:
			if not comment.group_alias in comments_groups_aliases:
				comments_groups_aliases.append(comment.group_alias)
			if not silent:
				self.stdout.write('Deleting ['+ str(comment.uuid) +']')
			if verbose:
				self.stdout.write(comment.content)
			# TODO manage parentless childs
			if comment.parent:
				parent.childs.remove(str(comment.uuid))
				parent.save()
			else:
				group = CommentsGroup.objects.get(alias=comment.group_alias)
				try:
					group.comments.remove(str(comment.uuid))
					group.save()
				except ValueError:
					pass
			comment.delete()

		for alias in comments_groups_aliases:
			cache_delete('comments_' + alias)

		self.stdout.write('Processed '+ str(counter_comments) +' comments')
