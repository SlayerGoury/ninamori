# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.core.management.base import BaseCommand, CommandError
from django.db import transaction
from comments.models import CommentsGroup, Comment, Commentator
from ninamori import akismet
from ninamori.utils import cache_delete


class Command (BaseCommand):
	help = 'Check every unckecked comment for spam by sending them to Akismet service'

	def add_arguments (self, parser):
		parser.add_argument('--fake',
			action='store_true',
			dest='fake',
			default=False,
			help='Do not perform actual sending')
		parser.add_argument('--silent',
			action='store_true',
			dest='silent',
			default=False,
			help='Do not output as much messages')
		parser.add_argument('--verbose',
			action='store_true',
			dest='verbose',
			default=False,
			help='Output content')
		parser.add_argument('--uuid',
			dest='uuid',
			default='',
			help='process just one comment')

	def handle(self, *args, **options):
		if not akismet.verify_key:
			self.stdout.write('Akismet API key verification failed')
			return False

		update = not options['fake']
		silent = options['silent']
		uuid = options['uuid']
		verbose = options['verbose']
		counter_comments = 0

		comments_list = [Comment.objects.get(uuid=uuid)] if uuid else Comment.objects.filter(akismet_spam=None, manual_spam=None)
		comments_for_threaded_akismet = []
		comments_groups_aliases = []

		for comment in comments_list:
			counter_comments += 1
			comments_for_threaded_akismet.append(comment)
			if comment.group_alias not in comments_groups_aliases:
				comments_groups_aliases.append(comment.group_alias)

		akismet_results = akismet.threaded_akismet(comments_for_threaded_akismet, silent, verbose)

		for comment_uuid, akismet_spam in akismet_results.items():
			comment = Comment.objects.get(uuid=comment_uuid)
			comment.akismet_spam = akismet_spam
			if update:
				comment.save()

		for alias in comments_groups_aliases:
			cache_delete('comments_' + alias)

		if not silent:
			self.stdout.write('Processed '+ str(counter_comments) +' comments')
