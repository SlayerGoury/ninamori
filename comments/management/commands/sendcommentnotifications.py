# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.conf import settings
from django.core.management.base import BaseCommand, CommandError
from django.db import transaction
from django.template.loader import render_to_string
from django.utils import timezone
from django.utils.translation import activate as activate_translation
from django.utils.translation import ugettext as _
from accounts.models import UserProfile
from comments.models import CommentsGroup, Comment, Commentator
from copy import copy
from mailqueue.models import MailerMessage
from textwrap import fill
import datetime


def compose_notification (comment, group_alias):
	composition = {
		'uuid': comment.uuid,
		'deleted': comment.deleted,
		'approved': comment.approved,
		'content': comment.content,
		# TODO add user timezone support
		'date': comment.date_added.strftime('%A, %-d %b %Y, %H:%M'),
		'name': comment.get_commentator_name(),
		'group_alias': group_alias,
	}
	return composition


def group_compositions (compositions):
	sorted_compositions = sorted(compositions, key=lambda k:k['group_alias'])

	groups = []
	for position in sorted_compositions:
		if not position['group_alias'] in groups:
			groups.append(position['group_alias'])

	grouped_compositions = {}
	for group in groups:
		grouped_compositions[group] = []
		for position in compositions:
			if position['group_alias'] == group:
				grouped_compositions[group].append(position)

	return grouped_compositions


def add_parents (grouped_compositions):
	for alias, group in grouped_compositions.items():
		comments_group = CommentsGroup.objects.get(alias=alias)
		if comments_group.mode == 'tree':
			for composition in copy(group):
				comment = Comment.objects.get(uuid = composition['uuid'])
				if comment.parent:
					if not check_if_comment_in_group(comment.parent, group):
						group.insert(group.index(composition), compose_notification(comment.parent, alias))

	return grouped_compositions


def check_if_comment_in_group (comment, group):
	for composition in group:
		if composition['uuid'] == comment.uuid:
			return True
	return False


def send_composed_notifications (compositions, commentator):
	locale = commentator.language
	activate_translation(locale)
	grouped_compositions = add_parents(group_compositions(compositions))
	message_data = {
		'protocol':     settings.PROTOCOL_SCHEME,
		'domain':       settings.SITE_DOMAIN,
	}
	title = _('[%(site_name)s] new comments notification' % {'site_name': settings.SITE_NAME[locale]})
	body = render_to_string('comments/notification.txt', {'compositions': grouped_compositions, 'message_data': message_data})
	body = fill(body, replace_whitespace=False, expand_tabs=False, break_long_words=False, break_on_hyphens=False)
	body = fill(body, replace_whitespace=False, expand_tabs=False, break_long_words=True, break_on_hyphens=False, width=998)
	body=body.replace('~', '')  # this is placeholder for newline in template

	email_message = MailerMessage()
	email_message.subject       = title
	email_message.content       = body
	email_message.from_address  = settings.DEFAULT_FROM_EMAIL
	email_message.to_address    = commentator.email
	email_message.app           = 'comments notification'
	email_message.save()


class Command (BaseCommand):
	help = 'Send notifications for all subscribers about all new comments'

	def add_arguments (self, parser):
		parser.add_argument('--fake',
			action='store_true',
			dest='fake',
			default=False,
			help='Do not perform actual send')
		parser.add_argument('--nosave',
			action='store_true',
			dest='nosave',
			default=False,
			help='Do not save anything to database')
		parser.add_argument('--silent',
			action='store_true',
			dest='silent',
			default=False,
			help='Do not output as much messages')

	def handle (self, *args, **options):
		send = not options['fake']
		save = not options['nosave']
		silent = options['silent']

		with transaction.atomic():
			profiles = UserProfile.objects.select_for_update().filter(comments_next_notification_date__lte=timezone.now(), settings_comments_enable_notifying=True)
			if not silent: self.stdout.write(str(len(profiles)) + ' users is ready to receive notification')
			commentators_emails = []
			for profile in profiles:
				if profile.user.email:
					commentator = Commentator.objects.get_or_create(email=profile.user.email)[0]
					if commentator.notification_queue:
						if not silent: self.stdout.write(profile.user.email + ' will get notification')
						commentators_emails.append(profile.user.email)
						if save: profile.comments_next_notification_date = timezone.now() + profile.settings_comments_delay_interval
					#should be saved anyway to release lock
					profile.save()

		with transaction.atomic():
			commentators = Commentator.objects.select_for_update().filter(email__in=commentators_emails)
			for commentator in commentators:
				composed_notifications = []
				unapproved = 0
				deleted = 0
				for comment_uuid, group_alias in commentator.notification_queue:
					if not silent: self.stdout.write(commentator.email + ' should be notified about comment ' + comment_uuid + ' from ' + group_alias)
					composition = compose_notification(Comment.objects.get(uuid=comment_uuid), group_alias)
					unapproved += 1 if not composition['approved'] else 0
					deleted += 1 if composition['deleted'] else 0
					composed_notifications.append(composition)
				if save: commentator.notification_queue = []
				commentator.save()
				if send and len(composed_notifications) > deleted: send_composed_notifications(composed_notifications, commentator)

		profiles = UserProfile.objects.filter(settings_comments_enable_notifying=False)
		commentators_emails = []
		for profile in profiles:
			if profile.user.email:
				if not silent: self.stdout.write(profile.user.email + ' do not want notifications')
				Commentator.objects.get_or_create(email=profile.user.email)
				commentators_emails.append(profile.user.email)

		with transaction.atomic():
			commentators = Commentator.objects.select_for_update().filter(email__in=commentators_emails)
			for commentator in commentators:
				if not silent: self.stdout.write(commentator.email + ' queue cleaned')
				if save: commentator.notification_queue = []
				commentator.save()

		if not silent: self.stdout.write('Done')
