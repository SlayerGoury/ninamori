# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from datetime import datetime, timedelta
from django.contrib.auth.models import Permission, User, AnonymousUser, Group
from django.contrib.sessions.backends.db import SessionStore
from django.core.management import call_command
from django.utils import timezone
from django.test import TestCase, RequestFactory
from ninamori import akismet
from models import Comment, Commentator, CommentsGroup
from accounts.utils import email_access_validate
from cms.models import Page
import json
import utils, views
import templatetags.comments_tags


class commentsTests (TestCase):
	def setUp (self):
		self.factory = RequestFactory()
		self.user1 = User.objects.create_user(username='meow1', email='1m@e.ow', password='purr')
		self.user2 = User.objects.create_user(username='meow2', email='2m@e.ow', password='purr')
		self.user3 = User.objects.create_user(username='meow3', email='3m@e.ow', password='purr')
		email_access_validate('1m@e.ow')
		email_access_validate('2m@e.ow')
		self.user1.userprofile.refresh_from_db()
		self.user2.userprofile.refresh_from_db()
		self.tree_group1 = CommentsGroup.objects.create(alias='/1', allow_anonymous=True, mode='tree', sorting_mode='top')
		self.tree_group2 = CommentsGroup.objects.create(alias='/3', allow_anonymous=True, mode='tree', sorting_mode='bot')
		self.tree_group3 = CommentsGroup.objects.create(alias='nya', allow_anonymous=True, mode='tree', sorting_mode='bot')
		self.line_group1 = CommentsGroup.objects.create(alias='/2', allow_anonymous=True, mode='line', sorting_mode='top')
		self.line_group2 = CommentsGroup.objects.create(alias='/4', allow_anonymous=True, mode='line', sorting_mode='bot')
		self.line_group3 = CommentsGroup.objects.create(alias='/5', allow_anonymous=True, mode='line', sorting_mode='bot', premoderation_mode='dis')
		self.group = Group.objects.create(name='group1')
		self.page = Page.objects.create(alias='nya', author=self.user1, is_published=True)
		self.page.allowed_groups.add(self.group)
		self.page.save()

	def test_utils_add_comment (self):
		comment = utils.add_comment(self.user1, self.tree_group1, None, 'meow', '127.0.0.1', 'meow', 'en', True)
		comment.refresh_from_db()
		self.assertEqual(comment.content, 'meow')
		comment = utils.add_comment(self.user1, self.line_group1, None, 'meow', '127.0.0.1', 'meow', 'en', True)
		comment.refresh_from_db()
		self.assertEqual(comment.content, 'meow')

	def test_utils_update_tree_commentators_subscriptions (self):
		comment = utils.add_comment(self.user1, self.tree_group1, None, 'meow', '127.0.0.1', 'meow', 'en', True)
		comment = utils.add_comment(self.user2, self.tree_group1, comment, 'meow', '127.0.0.1', 'meow', 'en', True)
		utils.update_tree_commentators_subscriptions(comment, self.tree_group1)
		comment = utils.add_comment(self.user1, self.tree_group2, None, 'meow', '127.0.0.1', 'meow', 'en', True)
		comment = utils.add_comment(self.user2, self.tree_group2, comment, 'meow', '127.0.0.1', 'meow', 'en', True)
		utils.update_tree_commentators_subscriptions(comment, self.tree_group2)

	def test_utils_update_group_subscriptions (self):
		request = self.factory.post('/1', {})
		request.LANGUAGE_CODE = 'en'
		request.user = self.user1
		request.POST = {'alias': '/2', 'action': 'watch'}
		views.watch_group(request)
		request.POST = {'alias': '/4', 'action': 'watch'}
		views.watch_group(request)
		commentator = Commentator.objects.get(email='1m@e.ow')
		comment = utils.add_comment(self.user2, self.line_group1, None, 'meow', '127.0.0.1', 'meow', 'en', True)
		utils.update_group_subscriptions(comment, self.line_group1)
		commentator.refresh_from_db()
		self.assertIn(str(comment.uuid), str(commentator.notification_queue))
		comment = utils.add_comment(self.user2, self.line_group2, None, 'meow', '127.0.0.1', 'meow', 'en', True)
		utils.update_group_subscriptions(comment, self.line_group2)
		commentator.refresh_from_db()
		self.assertIn(str(comment.uuid), str(commentator.notification_queue))

	def test_models_comment_get_commentator_name (self):
		comment = utils.add_comment(self.user1, self.line_group1, None, 'meow', '127.0.0.1', 'meow', 'en', True)
		self.assertEqual(comment.get_commentator_name(), 'meow1')
		comment = utils.add_comment(AnonymousUser(), self.line_group1, None, 'meow', '127.0.0.1', 'meow', 'en', True)
		self.assertEqual(comment.get_commentator_name(), 'Anonymous')
		comment = utils.add_comment(AnonymousUser(), self.line_group1, None, 'meow', '127.0.0.1', 'meow', 'en', True)
		comment.email = 'm.e.o.w'
		self.assertEqual(comment.get_commentator_name(), 'Anonymous')

	def test_utils_edit_comment (self):
		comment = utils.add_comment(self.user1, self.line_group1, None, 'meow', '127.0.0.1', 'meow', 'en', True)
		self.assertTrue(utils.edit_comment(comment, self.line_group1, 'purr', True))
		comment.refresh_from_db()
		self.assertEqual(comment.content, 'purr')

	def test_utils_get_top_level_comments_html (self):
		comment1 = utils.add_comment(self.user1, self.tree_group1, None, 'meow', '127.0.0.1', 'meow', 'en', True)
		comment1.approved = True
		comment1.save()
		comment2 = utils.add_comment(self.user1, self.tree_group1, None, 'purr', '127.0.0.1', 'meow', 'en', True)
		comment2.approved = True
		comment2.save()
		comment3 = utils.add_comment(self.user1, self.tree_group1, comment2, 'murrpurr', '127.0.0.1', 'meow', 'en', True)
		comment3.approved = True
		comment3.save()
		request = self.factory.post('/1', {})
		request.LANGUAGE_CODE = 'en'
		request.user = self.user1
		request.session = {}
		result = utils.get_top_level_comments_html(request)
		self.assertIn('meow', result[0])
		self.assertIn('purr', result[0])
		self.assertNotIn('meowpurr', result[0])

		request = self.factory.post('/1/scripts.js', {})
		request.LANGUAGE_CODE = 'en'
		request.user = self.user1
		request.session = {}
		result = utils.get_top_level_comments_html(request)
		self.assertTrue(result[0] is '')

	def test_utils_get_comments_branch_html (self):
		comment1 = utils.add_comment(self.user1, self.tree_group1, None, 'I am the meow that should not be in the testing branch', '127.0.0.1', 'meow', 'en', True)
		comment1.approved = True
		comment1.save()
		comment2 = utils.add_comment(self.user1, self.tree_group1, comment1, 'purr', '127.0.0.1', 'meow', 'en', True)
		comment2.approved = True
		comment2.save()
		comment3 = utils.add_comment(self.user1, self.tree_group1, comment2, 'murrpurr', '127.0.0.1', 'meow', 'en', True)
		comment3.approved = True
		comment3.save()
		request = self.factory.post('/1', {})
		request.LANGUAGE_CODE = 'en'
		request.user = self.user1
		request.session = {}
		result = utils.get_comments_branch_html(self.tree_group1, comment1, self.user1.email, True, request)
		self.assertIn(str(comment1.uuid), result.content)
		self.assertIn('purr', result.content.decode('utf8'))
		self.assertIn('murrpurr', result.content.decode('utf8'))
		self.assertNotIn('I am the meow that should not be in the testing branch', result.content.decode('utf8'))
		result = utils.get_comments_branch_html(self.tree_group2, None, self.user1.email, True, request)
		self.assertEqual(result.status_code, 400)  # empty group
		comment4 = utils.add_comment(self.user1, self.tree_group2, None, 'nya', '127.0.0.1', 'meow', 'en', True)
		comment5 = utils.add_comment(self.user1, self.tree_group2, None, 'nya', '127.0.0.1', 'meow', 'en', True)
		result = utils.get_comments_branch_html(self.tree_group2, comment4, self.user1.email, True, request)
		self.assertEqual(result.status_code, 200)  # empty branch

	def test_utils_coment_is_related_to_branch (self):
		comment = utils.add_comment(self.user1, self.tree_group1, None, 'meow', '127.0.0.1', 'meow', 'en', True)
		self.assertTrue(utils.coment_is_related_to_branch(comment, self.tree_group1))
		comment = utils.add_comment(self.user1, self.tree_group1, comment, 'meow', '127.0.0.1', 'meow', 'en', True)
		self.assertTrue(utils.coment_is_related_to_branch(comment, self.tree_group1))
		comment = utils.add_comment(self.user1, self.tree_group1, comment, 'meow', '127.0.0.1', 'meow', 'en', True)
		comment = utils.add_comment(self.user1, self.tree_group1, comment, 'meow', '127.0.0.1', 'meow', 'en', True)
		comment = utils.add_comment(self.user1, self.tree_group1, comment, 'meow', '127.0.0.1', 'meow', 'en', True)
		comment = utils.add_comment(self.user1, self.tree_group1, comment, 'meow', '127.0.0.1', 'meow', 'en', True)
		comment = utils.add_comment(self.user1, self.tree_group1, comment, 'meow', '127.0.0.1', 'meow', 'en', True)
		comment = utils.add_comment(self.user1, self.tree_group1, comment, 'meow', '127.0.0.1', 'meow', 'en', True)
		self.assertFalse(utils.coment_is_related_to_branch(comment, self.tree_group1))  # too much of recursion

	def test_utils_move_comments (self):
		comment1 = utils.add_comment(self.user1, self.tree_group1, None, 'meow', '127.0.0.1', 'meow', 'en', True)
		utils.move_comments(comment1, self.tree_group2)
		self.assertEqual(Comment.objects.get(uuid=comment1.uuid).group_alias, self.tree_group2.alias)
		comment2 = utils.add_comment(self.user1, self.tree_group1, None, 'meow', '127.0.0.1', 'meow', 'en', True)
		comment3 = utils.add_comment(self.user1, self.tree_group1, comment2, 'meow', '127.0.0.1', 'meow', 'en', True)
		utils.move_comments(comment2, self.tree_group2)
		self.assertEqual(Comment.objects.get(uuid=comment2.uuid).group_alias, self.tree_group2.alias)
		self.assertEqual(Comment.objects.get(uuid=comment3.uuid).group_alias, self.tree_group2.alias)
		comment4 = utils.add_comment(self.user1, self.tree_group1, None, 'meow', '127.0.0.1', 'meow', 'en', True)
		comment5 = utils.add_comment(self.user1, self.tree_group1, comment4, 'meow', '127.0.0.1', 'meow', 'en', True)
		utils.move_comments(comment4, comment1)
		self.assertEqual(Comment.objects.get(uuid=comment4.uuid).group_alias, self.tree_group2.alias)
		self.assertEqual(Comment.objects.get(uuid=comment5.uuid).group_alias, self.tree_group2.alias)
		comment6 = utils.add_comment(self.user1, self.tree_group1, comment4, 'meow', '127.0.0.1', 'meow', 'en', True)
		comment7 = utils.add_comment(self.user1, self.tree_group1, comment4, 'meow', '127.0.0.1', 'meow', 'en', True)
		del comment6.parent.childs[comment6.parent.childs.index(comment6.uuid)]
		failed_attempt = utils.move_comments(comment6, comment7)
		self.assertFalse(failed_attempt)
		failed_attempt = utils.move_comments(comment7, 'nowhere')
		self.assertFalse(failed_attempt)

	def test_utils_move_all_coments (self):
		comment = utils.add_comment(self.user1, self.tree_group1, None, 'meow', '127.0.0.1', 'meow', 'en', True)
		failed_attempt = utils.move_all_coments(self.tree_group1, 'nowhere')
		self.assertFalse(failed_attempt)
		comment1 = utils.add_comment(self.user1, self.tree_group1, None, 'meow', '127.0.0.1', 'meow', 'en', True)
		comment2 = utils.add_comment(self.user1, self.tree_group1, comment1, 'meow', '127.0.0.1', 'meow', 'en', True)
		comment3 = utils.add_comment(self.user1, self.tree_group1, comment2, 'meow', '127.0.0.1', 'meow', 'en', True)
		comment4 = utils.add_comment(self.user1, self.tree_group1, comment3, 'meow', '127.0.0.1', 'meow', 'en', True)
		comment5 = utils.add_comment(self.user1, self.tree_group1, comment4, 'meow', '127.0.0.1', 'meow', 'en', True)
		utils.move_all_coments(self.tree_group1, self.tree_group2)
		self.assertEqual(Comment.objects.get(uuid=comment1.uuid).group_alias, self.tree_group2.alias)
		self.assertEqual(Comment.objects.get(uuid=comment2.uuid).group_alias, self.tree_group2.alias)
		self.assertEqual(Comment.objects.get(uuid=comment3.uuid).group_alias, self.tree_group2.alias)
		self.assertEqual(Comment.objects.get(uuid=comment4.uuid).group_alias, self.tree_group2.alias)
		self.assertEqual(Comment.objects.get(uuid=comment5.uuid).group_alias, self.tree_group2.alias)

	def test_models_comment (self):
		comment = utils.add_comment(self.user1, self.tree_group1, None, 'meow', '127.0.0.1', 'meow', 'en', True)
		uuid = str(comment.uuid)
		self.assertEqual(comment.__unicode__(), 'comment '+uuid)

	def test_models_comment_tupleize_comment (self):
		comment = utils.add_comment(self.user1, self.tree_group1, None, 'meow', '127.0.0.1', 'meow', 'en', True)
		self.assertIn('meow', comment.tupleize_comment(1))
		comment.akismet_spam = True
		comment.manual_spam = False
		self.assertTrue(comment.tupleize_comment(1)[12] is None)  # akismet_spam flag is reset

	def test_models_comment_update_group_alias (self):
		comment = utils.add_comment(self.user1, self.tree_group1, None, 'meow', '127.0.0.1', 'meow', 'en', True)
		self.assertEqual(comment.update_group_alias(), '/1')
		comment = utils.add_comment(self.user1, self.tree_group1, comment, 'meow', '127.0.0.1', 'meow', 'en', True)
		comment.group_alias = None
		self.assertEqual(comment.update_group_alias(), '/1')
		comment = utils.add_comment(self.user1, self.tree_group1, None, 'meow', '127.0.0.1', 'meow', 'en', True)
		comment.group_alias = None
		del self.tree_group1.comments[self.tree_group1.comments.index(str(comment.uuid))]
		self.tree_group1.save()
		self.assertFalse(comment.update_group_alias())

	def test_models_commentsgroup (self):
		self.assertEqual(self.tree_group3.__unicode__(), 'comments group nya')

	def test_utils_untuple_childs (self):
		comment = utils.add_comment(self.user1, self.tree_group1, None, 'meow', '127.0.0.1', 'meow', 'en', True)
		child_comment = utils.add_comment(self.user1, self.tree_group1, comment, 'meow', '127.0.0.1', 'meow', 'en', True)
		child_comment_uuid = child_comment.uuid
		comment.refresh_from_db()
		tupled_comment = comment.tupleize_comment(1)
		self.assertTrue(utils.untuple_childs(tupled_comment)[1])

	def test_utils_get_comments (self):
		comment = utils.add_comment(self.user1, self.tree_group1, None, 'meow', '127.0.0.1', 'meow', 'en', True)
		comment = utils.add_comment(self.user1, self.tree_group1, comment, 'meow', '127.0.0.1', 'meow', 'en', True)
		utils.get_comments(self.tree_group1)

	def test_views_comment (self):
		request = self.factory.get('/')
		request.LANGUAGE_CODE = 'en'
		request.META['HTTP_USER_AGENT'] = 'meow'
		request.user = self.user1
		request.session = SessionStore()
		self.assertEqual(views.comment(request).status_code, 405)  # POST request expected
		request.method = 'POST'
		self.assertEqual(views.comment(request).status_code, 400)  # invalid form
		request.POST = {'comments_group_alias': '/97', 'content': 'meow', 'email_updates': True}
		self.assertEqual(views.comment(request).status_code, 400)  # group does not exist
		request.POST = {'comments_group_alias': '/5', 'content': 'meow', 'email_updates': True}
		self.assertEqual(views.comment(request).status_code, 403)  # comments disabled
		request.user = self.user3
		request.POST = {'comments_group_alias': '/1', 'content': 'meow', 'email_updates': True}
		self.assertEqual(views.comment(request).status_code, 403)  # email is not validated
		request.user = self.user1
		request.POST = {'comments_group_alias': '/2', 'content': 'meow', 'email_updates': True}
		self.assertEqual(views.comment(request).status_code, 302)  # normal return
		comment = utils.add_comment(self.user1, self.tree_group2, None, 'meow', '127.0.0.1', 'meow', 'en', True)
		request.POST = {'comments_group_alias': '/3', 'content': 'meow', 'email_updates': True, 'parent_uuid': str(comment.uuid)}
		self.assertEqual(views.comment(request).status_code, 302)  # normal return, unfolded branch
		request.POST = {'comments_group_alias': 'nya', 'content': 'meow', 'email_updates': True}
		self.assertEqual(views.comment(request).status_code, 400)  # user have no access to page

	def test_views_edit (self):
		comment1 = utils.add_comment(self.user1, self.line_group1, None, 'meow', '127.0.0.1', 'meow', 'en', True)
		str_uuid1 = str(comment1.uuid)
		comment2 = utils.add_comment(self.user1, self.line_group3, None, 'meow', '127.0.0.1', 'meow', 'en', True)
		str_uuid2 = str(comment2.uuid)
		request = self.factory.get('/')
		request.LANGUAGE_CODE = 'en'
		request.META['HTTP_USER_AGENT'] = 'meow'
		request.user = self.user2
		request.session = SessionStore()
		self.assertEqual(views.edit(request).status_code, 405)  # POST request expected
		request.method = 'POST'
		self.assertEqual(views.edit(request).status_code, 400)  # invalid form
		request.POST = {'comment_uuid': str_uuid1, 'comments_group_alias': '/97', 'content': 'purr', 'email_updates': True}
		self.assertEqual(views.edit(request).status_code, 400)  # group does not exist
		request.POST = {'comment_uuid': str_uuid1, 'comments_group_alias': '/2', 'content': 'purr', 'email_updates': True}
		self.assertEqual(views.edit(request).status_code, 403)  # cant edit comment if it is not yours
		request.user = self.user1
		self.assertEqual(views.edit(request).status_code, 302)  # normal return
		comment1.refresh_from_db()
		self.assertEqual(comment1.content, 'purr')
		request.POST = {'comment_uuid': str_uuid2, 'comments_group_alias': '/5', 'content': 'purr', 'email_updates': True}
		self.assertEqual(views.edit(request).status_code, 403)  # cant edit comment in disabled group
		comment1.date_added = timezone.now() - timedelta(days=365)
		comment1.save()
		request.POST = {'comment_uuid': str_uuid1, 'comments_group_alias': '/2', 'content': 'murr', 'email_updates': True}
		self.assertEqual(views.edit(request).status_code, 418)  # too late to edit this comment
		request.user = AnonymousUser()
		request.session = SessionStore()
		self.assertEqual(views.edit(request).status_code, 403)  # comment is not of this user

	def test_views_unfold (self):
		comment1 = utils.add_comment(self.user1, self.tree_group1, None, 'meow', '127.0.0.1', 'meow', 'en', True)
		comment1.approved = True
		comment1.save()
		comment2 = utils.add_comment(self.user1, self.tree_group1, comment1, 'meow', '127.0.0.1', 'meow', 'en', True)
		comment2.approved = True
		comment2.save()
		request = self.factory.post('/')
		request.LANGUAGE_CODE = 'en'
		request.META['HTTP_USER_AGENT'] = 'meow'
		request.user = self.user1
		request.session = SessionStore()
		self.assertEqual(views.unfold(request).status_code, 400)  # alias is missing
		request.POST = {'alias': '/1'}
		self.assertEqual(views.unfold(request).status_code, 302)  # unfolded
		self.assertTrue(request.session['comments_/1unfolded'])
		self.assertEqual(views.unfold(request).status_code, 302)  # folded back
		self.assertFalse(request.session['comments_/1unfolded'])

	def test_views_unfold_ajax (self):
		comment1 = utils.add_comment(self.user1, self.tree_group1, None, 'meow', '127.0.0.1', 'meow', 'en', True)
		comment1.approved = True
		comment1.save()
		comment2 = utils.add_comment(self.user1, self.tree_group1, comment1, 'meow', '127.0.0.1', 'meow', 'en', True)
		comment2.approved = True
		comment2.save()
		comment3 = utils.add_comment(self.user1, self.line_group1, None, 'meow', '127.0.0.1', 'meow', 'en', True)
		comment3.approved = True
		comment3.save()
		request = self.factory.post('/')
		request.LANGUAGE_CODE = 'en'
		request.META['HTTP_USER_AGENT'] = 'meow'
		request.user = self.user1
		request.session = SessionStore()
		self.assertEqual(json.loads(views.unfold_ajax(request).content)['status'], 'error')     # Comment.DoesNotExist
		request.POST = {'uuid': str(comment1.uuid)}
		self.assertEqual(json.loads(views.unfold_ajax(request).content)['status'], 'error')     # CommentsGroup.DoesNotExist
		request.POST = {'uuid': 'meow'}
		self.assertEqual(json.loads(views.unfold_ajax(request).content)['status'], 'error')     # ValueError
		request.POST = {'uuid': str(comment3.uuid), 'alias': self.line_group1.alias}
		self.assertEqual(json.loads(views.unfold_ajax(request).content)['status'], 'error')     # comments group mode is not tree
		request.POST = {'uuid': str(comment3.uuid), 'alias': self.tree_group1.alias}
		self.assertEqual(json.loads(views.unfold_ajax(request).content)['status'], 'error')     # comment not in this tree
		request.POST = {'uuid': str(comment1.uuid), 'alias': self.tree_group1.alias}
		self.assertEqual(json.loads(views.unfold_ajax(request).content)['status'], 'unfolded')  # comment branch unfolded
		request.POST = {'uuid': str(comment1.uuid), 'alias': self.tree_group1.alias}
		self.assertEqual(json.loads(views.unfold_ajax(request).content)['status'], 'folded')    # comment branch folded back

	def test_views_ajax_branch (self):
		comment1 = utils.add_comment(self.user1, self.tree_group1, None, 'meow', '127.0.0.1', 'meow', 'en', True)
		comment1.approved = True
		comment1.save()
		comment2 = utils.add_comment(self.user1, self.tree_group1, comment1, 'meow', '127.0.0.1', 'meow', 'en', True)
		comment2.approved = True
		comment2.save()
		comment3 = utils.add_comment(self.user1, self.line_group1, None, 'meow', '127.0.0.1', 'meow', 'en', True)
		comment3.approved = True
		comment3.save()
		comment4 = utils.add_comment(self.user2, self.tree_group3, None, 'meow', '127.0.0.1', 'meow', 'en', True)
		comment4.approved = True
		comment4.save()
		request = self.factory.post('/')
		request.LANGUAGE_CODE = 'en'
		request.META['HTTP_USER_AGENT'] = 'meow'
		request.user = self.user1
		request.session = SessionStore()
		self.assertEqual(views.ajax_branch(request).status_code, 400)                   # Comment.DoesNotExist
		request.POST = {'uuid': str(comment1.uuid)}
		self.assertEqual(views.ajax_branch(request).status_code, 400)                   # CommentsGroup.DoesNotExist
		request.POST = {'uuid': 'meow'}
		self.assertEqual(views.ajax_branch(request).status_code, 400)                   # ValueError
		request.POST = {'uuid': str(comment3.uuid), 'alias': self.line_group1.alias}
		self.assertEqual(views.ajax_branch(request).status_code, 400)                   # comments group mode is not tree
		request.POST = {'uuid': str(comment4.uuid), 'alias': self.tree_group3.alias}
		self.assertEqual(views.ajax_branch(request).status_code, 400)                   # user have no access to tree of this branch
		request.POST = {'uuid': str(comment1.uuid), 'alias': self.tree_group1.alias}
		test_string = '<!-- attach me after '+str(comment1.uuid)+' -->'
		self.assertIn(test_string, views.ajax_branch(request).content.decode('utf-8'))  # rendered a branch

	def test_views_watch (self):
		comment1 = utils.add_comment(self.user1, self.line_group1, None, 'meow', '127.0.0.1', 'meow', 'en', False)
		str_uuid1 = str(comment1.uuid)
		comment2 = utils.add_comment(self.user1, self.tree_group2, None, 'meow', '127.0.0.1', 'meow', 'en', False)
		str_uuid2 = str(comment2.uuid)
		self.assertFalse(comment2.email_updates)
		request = self.factory.get('/')
		request.LANGUAGE_CODE = 'en'
		request.META['HTTP_USER_AGENT'] = 'meow'
		request.user = self.user2
		self.assertEqual(views.watch(request).status_code, 405)                            # POST request expected
		request.method = 'POST'
		request.POST = {'alias': '/2', 'uuid': str_uuid1, 'action': 'meow'}
		self.assertEqual(json.loads(views.watch(request).content)['status'], 'error')      # wrong action
		request.POST = {'alias': '/2', 'uuid': str_uuid1, 'action': 'watch'}
		self.assertEqual(views.watch(request).status_code, 403)                            # should watch somewhere else
		request.user = self.user1
		self.assertEqual(views.watch(request).status_code, 400)                            # comments in linear groups is not wathchable
		request.POST = {'alias': '/3', 'uuid': str_uuid2, 'action': 'watch'}
		self.assertEqual(json.loads(views.watch(request).content)['status'], 'watched')    # comment is watched
		self.assertEqual(json.loads(views.watch(request).content)['status'], 'watched')    # comment is still watched
		comment2.refresh_from_db()
		self.assertTrue(comment2.email_updates)
		request.POST = {'alias': '/3', 'uuid': str_uuid2, 'action': 'unwatch'}
		self.assertEqual(json.loads(views.watch(request).content)['status'], 'unwatched')  # comment is unwatched
		self.assertEqual(json.loads(views.watch(request).content)['status'], 'unwatched')  # comment is still unwatched
		comment2.refresh_from_db()
		self.assertFalse(comment2.email_updates)

	def test_views_watch_group (self):
		comment = utils.add_comment(self.user1, self.line_group1, None, 'meow', '127.0.0.1', 'meow', 'en', False)
		request = self.factory.get('/')
		request.LANGUAGE_CODE = 'en'
		request.META['HTTP_USER_AGENT'] = 'meow'
		request.user = self.user3
		self.assertEqual(views.watch_group(request).status_code, 405)                            # POST request expected
		request.method = 'POST'
		request.POST = {'alias': '/2', 'action': 'meow'}
		self.assertEqual(json.loads(views.watch_group(request).content)['status'], 'error')      # wrong action
		request.POST = {'alias': '/2', 'action': 'watch'}
		self.assertEqual(views.watch_group(request).status_code, 403)                            # should go watch yourself because email is not validated yet
		request.user = self.user2
		self.assertEqual(json.loads(views.watch_group(request).content)['status'], 'watched')    # group is watched
		self.assertEqual(json.loads(views.watch_group(request).content)['status'], 'watched')    # group is still watched
		self.assertTrue(Commentator.objects.get(email='2m@e.ow') in self.line_group1.watchers.all())
		request.POST = {'alias': '/2', 'action': 'unwatch'}
		self.assertEqual(json.loads(views.watch_group(request).content)['status'], 'unwatched')  # group is unwatched
		self.assertEqual(json.loads(views.watch_group(request).content)['status'], 'unwatched')  # group is still unwatched
		self.assertFalse(Commentator.objects.get(email='2m@e.ow') in self.line_group1.watchers.all())

	def test_views_moderate (self):
		comment = utils.add_comment(self.user1, self.line_group1, None, 'meow', '127.0.0.1', 'meow', 'en', False)
		comment_t1 = utils.add_comment(self.user1, self.tree_group1, None, 'meow', '127.0.0.1', 'meow', 'en', False)
		comment_t2 = utils.add_comment(self.user1, self.tree_group1, comment_t1, 'meow', '127.0.0.1', 'meow', 'en', False)
		comment_t3 = utils.add_comment(self.user1, self.tree_group1, None, 'meow', '127.0.0.1', 'meow', 'en', False)
		comment_t4 = utils.add_comment(self.user1, self.tree_group1, comment_t3, 'meow', '127.0.0.1', 'meow', 'en', False)
		comment_t5 = utils.add_comment(self.user1, self.tree_group1, comment_t4, 'meow', '127.0.0.1', 'meow', 'en', False)
		comment_t2.email = 'm.e.o.w'
		comment_t2.save()
		str_uuid = str(comment.uuid)
		str_uuid_t1 = str(comment_t1.uuid)
		str_uuid_t2 = str(comment_t2.uuid)
		str_uuid_t3 = str(comment_t3.uuid)
		str_uuid_t4 = str(comment_t4.uuid)
		str_uuid_t5 = str(comment_t5.uuid)
		request = self.factory.get('/')
		request.LANGUAGE_CODE = 'en'
		request.META['HTTP_USER_AGENT'] = 'meow'
		request.user = self.user3
		self.assertEqual(views.moderate(request).status_code, 405)                            # POST request expected
		request.method = 'POST'
		self.assertEqual(views.moderate(request).status_code, 403)                            # user is not moderator nor staff
		self.user3.user_permissions.add(Permission.objects.get(codename='comments_moderate'))
		request.user = User.objects.get(username='meow3')
		request.POST = {'alias': '/2', 'uuid': str_uuid, 'action': 'meow'}
		self.assertEqual(json.loads(views.moderate(request).content)['status'], 'error')      # wrong action
		request.POST = {'alias': '/2', 'uuid': str_uuid, 'action': 'delete'}
		self.assertEqual(views.moderate(request).status_code, 403)                            # should go hack somewhere else because no perm to delete comments
		self.user3.user_permissions.add(Permission.objects.get(codename='comments_delete'))
		request.user = User.objects.get(username='meow3')
		self.assertEqual(json.loads(views.moderate(request).content)['status'], 'deleted')    # comment is deleted
		self.assertEqual(json.loads(views.moderate(request).content)['status'], 'deleted')    # comment is still deleted
		comment.refresh_from_db()
		self.assertTrue(comment.deleted)
		request.POST = {'alias': '/1', 'uuid': str_uuid_t3, 'action': 'delete'}
		self.assertIn(str_uuid_t4, json.loads(json.loads(views.moderate(request).content)['deleted_childs']))  # comment is deleted with childs
		request.POST = {'alias': '/2', 'uuid': str_uuid, 'action': 'undelete'}
		self.assertEqual(json.loads(views.moderate(request).content)['status'], 'undeleted')    # comment is undeleted
		self.assertEqual(json.loads(views.moderate(request).content)['status'], 'undeleted')    # comment is still undeleted
		comment.refresh_from_db()
		self.assertFalse(comment.deleted)
		request.POST = {'alias': '/2', 'uuid': str_uuid, 'action': 'approve'}
		self.assertEqual(json.loads(views.moderate(request).content)['status'], 'approved')    # comment is approved
		self.assertEqual(json.loads(views.moderate(request).content)['status'], 'approved')    # comment is still approved
		comment.refresh_from_db()
		self.assertTrue(comment.approved)
		request.POST = {'alias': '/2', 'uuid': str_uuid, 'action': 'unapprove'}
		self.assertEqual(json.loads(views.moderate(request).content)['status'], 'unapproved')    # comment is unapproved
		self.assertEqual(json.loads(views.moderate(request).content)['status'], 'unapproved')    # comment is still unapproved
		comment.refresh_from_db()
		self.assertFalse(comment.approved)
		request.POST = {'alias': '/1', 'uuid': str_uuid_t2, 'action': 'approve'}
		self.assertEqual(json.loads(views.moderate(request).content)['status'], 'parent unapproved')    # comments parent is not yet approved
		request.POST = {'alias': '/1', 'uuid': str_uuid_t1, 'action': 'approve'}
		views.moderate(request)
		request.POST = {'alias': '/1', 'uuid': str_uuid_t2, 'action': 'approve'}
		self.assertEqual(json.loads(views.moderate(request).content)['status'], 'approved')    # comment is approved
		request.POST = {'alias': '/1', 'uuid': str_uuid_t1, 'action': 'unapprove'}
		self.assertEqual(json.loads(views.moderate(request).content)['status'], 'have childs')    # comment already have childs
		request.POST = {'alias': '/1', 'uuid': str_uuid_t4, 'action': 'undelete'}
		self.assertEqual(json.loads(views.moderate(request).content)['status'], 'parent deleted')    # comment is child of deleted parent
		request.POST = {'alias': '/1', 'uuid': str_uuid_t5, 'action': 'spam'}
		self.assertEqual(json.loads(views.moderate(request).content)['status'], 'spam')    # marked as spam
		request.POST = {'alias': '/1', 'uuid': str_uuid_t5, 'action': 'spam'}
		self.assertEqual(json.loads(views.moderate(request).content)['status'], 'error')    # can't spam spam
		request.POST = {'alias': '/1', 'uuid': str_uuid_t5, 'action': 'unspam'}
		self.assertEqual(json.loads(views.moderate(request).content)['status'], 'unspam')    # unmarked as spam
		request.POST = {'alias': '/1', 'uuid': str_uuid_t5, 'action': 'unspam'}
		self.assertEqual(json.loads(views.moderate(request).content)['status'], 'error')    # can't unspam not spam
		comment_t5.akismet_spam = False
		comment_t5.save()
		request.POST = {'alias': '/1', 'uuid': str_uuid_t5, 'action': 'spam'}
		self.assertEqual(json.loads(views.moderate(request).content)['status'], 'spam')    # marked false positive as spam
		comment_t5.akismet_spam = True
		comment_t5.save()
		request.POST = {'alias': '/1', 'uuid': str_uuid_t5, 'action': 'unspam'}
		self.assertEqual(json.loads(views.moderate(request).content)['status'], 'unspam')    # marked false negative as not spam

	def test_templatetags_get_user_device_icon (self):
		self.assertIn('linux.png',        templatetags.comments_tags.get_user_device_icon('Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9a3pre) Gecko/20070330'))
		self.assertIn('blackberry.png',   templatetags.comments_tags.get_user_device_icon('Mozilla/5.0 (BlackBerry; U; BlackBerry 9900; en) AppleWebKit/534.11+ (KHTML, like Gecko) Version/7.1.0.346 Mobile Safari/534.11+'))
		self.assertIn('windows.png',      templatetags.comments_tags.get_user_device_icon('Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8a3) Gecko/20040817'))
		self.assertIn('apple.png',        templatetags.comments_tags.get_user_device_icon('Mozilla/5.0 (iPod; U; CPU iPhone OS 4_3_1 like Mac OS X; zh-cn) AppleWebKit/533.17.9 (KHTML, like Gecko) Version/5.0.2 Mobile/8G4 Safari/6533.18.5'))
		self.assertIn('apple.png',        templatetags.comments_tags.get_user_device_icon('Mozilla/5.0 (Macintosh; U; PPC Mac OS X Mach-O; en-US; rv:1.8.1a2) Gecko/20060512'))
		self.assertIn('chrome.png',       templatetags.comments_tags.get_user_device_icon('Mozilla/5.0 (X11; CrOS armv7l 4537.56.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.38 Safari/537.36'))
		self.assertIn('android.png',      templatetags.comments_tags.get_user_device_icon('Mozilla/5.0 (Linux; U; Android 4.0.3; ko-kr; LG-L160L Build/IML74K) AppleWebkit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30'))
		self.assertIn('nokia.png',        templatetags.comments_tags.get_user_device_icon('Mozilla/5.0 (Mozilla/5.0 (Series40; NokiaX2-02/10.90; Profile/MIDP-2.1 Configuration/CLDC-1.1) Gecko/20100401 S40OviBrowser/1.0.2.26.11'))
		self.assertIn('nokia.png',        templatetags.comments_tags.get_user_device_icon('Mozilla/5.0 (Mozilla/5.0 (SymbianOS/9.4; Series60/5.0 Nokia5800d-1/60.0.003; Profile/MIDP-2.1 Configuration/CLDC-1.1 ) AppleWebKit/533.4 (KHTML, like Gecko) NokiaBrowser/7.3.1.33 Mobile Safari/533.4'))
		self.assertIn('playstation.png',  templatetags.comments_tags.get_user_device_icon('Mozilla/5.0 (PLAYSTATION 3; 3.55)'))
		self.assertIn('unknown.png',      templatetags.comments_tags.get_user_device_icon('meow'))

	def test_templatetags_edit_button (self):
		comment = utils.add_comment(self.user1, self.line_group1, None, 'meow', '127.0.0.1', 'meow', 'en', False)
		self.assertTrue(templatetags.comments_tags.edit_button(datetime.utcnow(), comment.uuid, False))
		self.assertFalse(templatetags.comments_tags.edit_button(datetime.utcnow()-timedelta(days=65536), comment.uuid, False))

	# TODO not sure if this test belongs here or there
	def test_ninamori_akismet_threaded_akismet (self):
		comment = utils.add_comment(self.user1, self.tree_group1, None, 'meow', '127.0.0.1', 'meow', 'en', True)
		comment = utils.add_comment(self.user1, self.tree_group1, comment, 'meow', '127.0.0.1', 'meow', 'en', True)
		comment = utils.add_comment(self.user1, self.tree_group1, comment, 'meow', '127.0.0.1', 'meow', 'en', True)
		comment = utils.add_comment(self.user1, self.tree_group1, comment, 'meow', '127.0.0.1', 'meow', 'en', True)
		comment = utils.add_comment(self.user1, self.tree_group1, comment, 'meow', '127.0.0.1', 'meow', 'en', True)
		comment = utils.add_comment(self.user1, self.tree_group1, comment, 'meow', '127.0.0.1', 'meow', 'en', True)

		comments_list = Comment.objects.filter(akismet_spam=None, manual_spam=None)
		result = akismet.threaded_akismet(comments_list, True, False)
		self.assertEqual(len(result), 6)


	def test_management_command_akismet_comments (self):
		comment = utils.add_comment(self.user1, self.tree_group1, None, 'meow', '127.0.0.1', 'meow', 'en', True)
		call_command('akismet_comments', '--silent')
		self.assertFalse(Comment.objects.get(uuid=comment.uuid).akismet_spam is None)
