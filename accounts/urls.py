from django.conf import settings
from django.conf.urls import include, url
import views


if 'accounts' in settings.DISABLED_FEATURES:
	urlpatterns = [
		url(r'^', views.disabled, name='index'),
	]
else:
	urlpatterns = [
		url(r'^$', views.index, name='accounts index'),
		url(r'admin$', views.admin_index, name='admin_index'),
		url(r'admin_create_user$', views.web_create_user_by_admin, name='create_user_by_admin'),
		url(r'manage$', views.manage, name='manage'),
		url(r'key_authenticate$', views.web_key_authenticate, name='key_authenticate'),
		url(r'key/(?P<key>.+)$', views.web_key_authenticate, name='key_url_authenticate'),
		url(r'login$', views.login, name='login'),
		url(r'logout$', views.logout, name='logout'),
		url(r'validate_email$', views.validate_email, name='validate_email'),
		url(r'check_username$', views.check_username, name='check_username'),
		url(r'register$', views.web_register, name='register'),
		url(r'make_authorization_key$', views.web_make_authorization_key, name='make_authorization_key'),
		url(r'remove_authorization_key$', views.web_remove_authorization_key, name='remove_authorization_key'),
		url(r'restore$', views.web_restore, name='accounts restore'),
		url(r'restore_request$', views.restore_request, name='restore_request'),
	]
