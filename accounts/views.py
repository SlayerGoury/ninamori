# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib import auth
from django.contrib.auth import authenticate
from django.contrib.auth.decorators import permission_required
from django.contrib.auth.hashers import check_password
from django.contrib.auth.models import User, Group
from django.conf import settings
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.urls import reverse
from django.utils.translation import ugettext as _
from django.utils import timezone
from django.utils.crypto import get_random_string
from forms import *
from models import UserProfile
from accounts.utils import is_email_access_validated, email_access_validate
from mailer.models import List
from ninamori.utils import require_POST
import datetime
import hashlib
import utils

EMAIL_VALIDATION_RESEND_DELAY = 600
AUTHORIZATION_KEY_REMAKE_DELAY = 3600
ACCOUNT_RESTORATION_DELAY = 3600
TIME_FORMAT = '%d-%m-%Y %H:%M:%S'


def is_new_key_available (userprofile):
	return not userprofile.authorization_key_removed or (timezone.now() - userprofile.authorization_key_removed).total_seconds() > AUTHORIZATION_KEY_REMAKE_DELAY


def get_data_for_index (user):
	new_key_available = is_new_key_available(user.userprofile) if user.is_authenticated() else False
	staff_area = user.is_staff or user.has_perm('accounts.polls_generate_tokens') or user.has_perm('accounts.mailer_generate_tokens') or user.has_perm('accounts.create_user_with_key')
	return {
		'url_web_register': reverse(web_register),
		'url_validate_email': reverse(validate_email),
		'url_manage': reverse(manage),
		'url_make_authorization_key': reverse(web_make_authorization_key),
		'url_remove_authorization_key': reverse(web_remove_authorization_key),
		'new_key_available': new_key_available,
		'buttons': {'staff_area': staff_area},
	}


def index (request):
	render_data = get_data_for_index(request.user)
	render_data.update({'registration_form': registration_form})
	if request.session.get('account_updated'):
		request.session['account_updated'] = False
		render_data.update({'updated': True})
	code_status = request.GET.get('code')
	if code_status:
		render_data.update({'code': code_status})
	user = request.user
	initial = {
		'settings_comments_default_depth': user.userprofile.settings_comments_default_depth,
		'settings_comments_max_top_level_preload': user.userprofile.settings_comments_max_top_level_preload,
		'settings_comments_delay_interval': user.userprofile.settings_comments_delay_interval.days,
		'settings_comments_enable_notifying': user.userprofile.settings_comments_enable_notifying,
		'first_name': user.first_name,
		'last_name': user.last_name,
	} if user.is_authenticated() else {}

	message = request.session.get('accounts_index_message')
	if message:
		request.session['accounts_index_message'] = None

	render_data.update({
		'message': message,
		'manage_form': manage_form(initial=initial),
		'manage_form_errors': request.session.get('accounts_manage_form_errors'),
		'key_auth': request.GET.get('key_auth'),
		'url_key_authenticate': reverse(web_key_authenticate),
		'redirect': request.META.get('HTTP_REFERER') or '/',
	})
	return render(request, 'accounts/index.html', render_data)


def admin_index (request):
	user = request.user

	if user.has_perm('accounts.mailer_generate_tokens') or user.is_staff:
		last_generation = user.userprofile.mailer_tokens_last_generated or timezone.now() - datetime.timedelta(days=365)
		mailer_tokens_available = not (last_generation > timezone.now() - datetime.timedelta(hours=1))
	else:
		mailer_tokens_available = False

	if user.has_perm('accounts.polls_generate_tokens') or user.is_staff:
		last_generation = user.userprofile.polls_tokens_last_generated or timezone.now() - datetime.timedelta(days=365)
		polls_tokens_available = not (last_generation > timezone.now() - datetime.timedelta(hours=1))
	else:
		polls_tokens_available = False

	mailer_lists = {}
	if mailer_tokens_available:
		lists = List.objects.all()
		for l in lists:
			mailer_lists[str(l.uuid)] = l.name

	groups = {}
	if user.has_perm('accounts.create_user_with_key'):
		for group in Group.objects.all():
			groups[str(group.id)] = group.name

	message = request.session.get('accounts_admin_index_message')
	if message:
		request.session['accounts_admin_index_message'] = None

	render_data = {
		'message': message,
		'url_request_mailer_tokens': reverse('mailer_generate_tokens'),
		'url_request_polls_tokens': reverse('polls_generate_tokens'),
		'url_create_user_with_key': reverse(web_create_user_by_admin),
		'mailer_tokens_available': mailer_tokens_available,
		'polls_tokens_available': polls_tokens_available,
		'mailer_lists': mailer_lists,
		'groups': groups,
	}

	return render(request, 'accounts/admin_index.html', render_data)


@require_POST
def login (request):
	form = login_form(request.POST or None)
	if form.is_valid():
		data = form.cleaned_data
		user = authenticate(username=data['username'], password=data['password'])
		if user is not None and user.is_active:
			auth.login(request, user)
	return redirect (request.META.get('HTTP_REFERER') or '/')


@require_POST
def logout (request):
	if request.user.is_authenticated():
		auth.logout(request)
		response = redirect(request.META.get('HTTP_REFERER') or '/')
		if request.COOKIES.get('mod_buttons'):
			response.set_cookie('mod_buttons', '')
		return response
	else:
		return render(request, '404.html', {'message': _('Logging out is only for those who logged in')}, status=400)


def web_key_authenticate (request, key=None):
	key = request.POST.get('key') or key
	path = request.POST.get('redirect') or request.GET.get('redirect') or request.META.get('HTTP_REFERER') or '/'
	if not key:
		return render(request, '404.html', {'message': _('No entry for you')}, status=400)
	try:
		authorization_sha = key[0:40]
		userprofile = UserProfile.objects.get(authorization_sha=authorization_sha)
		assert check_password(key, userprofile.authorization_key) == True
	except (UserProfile.DoesNotExist, ValueError, AssertionError):
		return render(request, '403.html', {'message': _('Go away')}, status=403)

	user = userprofile.user
	if not hasattr(user, 'backend'):
		for backend in settings.AUTHENTICATION_BACKENDS:
			if user == auth.load_backend(backend).get_user(user.pk):
				user.backend = backend
				break
	auth.login(request, user)

	return redirect(path)


# redirect won't work here because different form should be rendered
# TODO handle errors with form.errors
@require_POST
def web_register (request):
	form = registration_form(request.POST or None)
	render_data = get_data_for_index(request.user)
	render_data.update({'registration_form': form})

	if not form.is_valid():  # this also should made impossible to get invalid email error from api_register
		return render(request, 'accounts/index.html', render_data)
	data = form.cleaned_data

	if not data['password'] == data['password_confirm']:
		render_data.update({'message': _('Passwords mismatch')})
		return render(request, 'accounts/index.html', render_data)

	result = utils.api_register(data['username'], data['email'], data['password'])

	if result['status'] == 'ok':
		user = authenticate(username=data['username'], password=data['password'])
		auth.login(request, user)
		return redirect(reverse(index))
	if result['status'] == 'error':
		if result['result'] == 'username exists':
			render_data.update({'message': _('Username already exists')})
			return render(request, 'accounts/index.html', render_data)
		if result['result'] == 'email exists':
			render_data.update({'message': _('Email is already registered')})
			return render(request, 'accounts/index.html', render_data)


@require_POST
def web_create_user_by_admin (request):
	if not request.user.has_perm('accounts.create_user_with_key'):
		return render(request, '403.html', {'message': _('You shall not pass')}, status=403)

	form = admin_create_user_form (request.POST or None)
	if not form.is_valid():
		request.session['accounts_admin_index_message'] = _('Form is invalid')
		return redirect(reverse(admin_index))
	data = form.cleaned_data
	try:
		group = Group.objects.get(id=data['group'])
	except Group.DoesNotExist:
		return render(request, '403.html', {'message': _('Get out and come back with a proper request')}, status=400)

	if User.objects.filter(username=data['username']):
		return render(request, '403.html', {'message': _('Username is not unique')}, status=400)

	user = User.objects.create_user(data['username'])
	user.groups.add(group)
	user.save()
	utils.make_and_send_authorization_key(user, request.user.email)

	request.session['accounts_admin_index_message'] = _('User %(username)s created') % {'username': data['username']}
	return redirect(reverse(admin_index))


def validate_email (request):
	if request.method == 'POST':
		if not ((request.user.is_authenticated() and request.user.userprofile.validation_email_code is None)
		    or (timezone.now() - request.user.userprofile.validation_email_sent).total_seconds() > EMAIL_VALIDATION_RESEND_DELAY):
			return redirect(reverse(index) + '?code=no')
		utils.make_and_send_validation_code(request.user)
		return redirect(reverse(index) + '?code=yes')
	if not request.GET.get('validation_email_code'):
		return render(request, '404.html', {'message': _('Get out and come back with a proper request')}, status=400)
	try:
		profile = UserProfile.objects.get(validation_email_code=request.GET.get('validation_email_code'))
	except UserProfile.DoesNotExist:
		return render(request, '403.html', {'message': _('Bad code')}, status=403)
	email_access_validate(profile.user.email)
	return redirect(reverse(index))


def check_username (request):
	if not request.GET.get('username'):
		return render(request, '404.html', {'message': _('You shall not pass')}, status=405)
	if User.objects.filter(username=request.GET.get('username').encode('utf-8')).exists():
		return HttpResponse('/static/img/accounts/check_username_bad.png', content_type='text/plain')
	return HttpResponse('/static/img/accounts/check_username_ok.png', content_type='text/plain')


@require_POST
def manage (request):
	user = request.user
	if not user.is_authenticated():
		return render(request, '404.html', {'message': _('This is for authenticated users only')}, status=403)
	form = manage_form(request.POST or None)
	if form.is_valid():
		data = form.cleaned_data
		password = data['password']
		passwords_match = data['new_password'] == data['new_password_confirm']

		if password:
			if user.check_password(password) and passwords_match:
				user.set_password(data['new_password'])
				data['password_updated'] = True
			elif not user.check_password(password):
				form.add_error('password', _('Invalid password'))
			elif not passwords_match:
				form.add_error('new_password_confirm', _('Passwords mismatch'))

		delay_interval = datetime.timedelta( days=data['settings_comments_delay_interval'] )
		if user.userprofile.settings_comments_delay_interval != delay_interval:
			utils.adjust_comments_next_notification_date(user.userprofile, delay_interval)
			user.userprofile.refresh_from_db()
		user.userprofile.settings_comments_default_depth  = data['settings_comments_default_depth']
		user.userprofile.settings_comments_max_top_level_preload  = data['settings_comments_max_top_level_preload']
		user.userprofile.settings_comments_enable_notifying  = data['settings_comments_enable_notifying']
		user.userprofile.save()
		user.first_name = data['first_name']
		user.last_name = data['last_name']
		user.save()
		if data.get('password_updated'):
			user = authenticate(username=user.username, password=data['new_password'])
			auth.login(request, user)
		request.session['account_updated'] = True

	request.session['accounts_manage_form_errors'] = form.errors
	return redirect(reverse(index))


@require_POST
def web_make_authorization_key (request):
	user = request.user
	if not request.user.is_authenticated():
		return render(request, '404.html', {'message': _('This is for authenticated users only')}, status=403)

	if not user.userprofile.authorization_key and is_email_access_validated(user.email):
		if is_new_key_available(user.userprofile):
			utils.make_and_send_authorization_key(user)
			request.session['accounts_index_message'] = _('Authorization key sent to your email')

	return redirect(reverse(index))


def web_remove_authorization_key (request):
	user = request.user
	if not request.user.is_authenticated():
		return render(request, '404.html', {'message': _('This is for authenticated users only')}, status=403)
	if not request.method == 'POST':
		if user.userprofile.authorization_key:
			return render(request, 'accounts/remove_key_confirmation.html', {'delay': AUTHORIZATION_KEY_REMAKE_DELAY})
		else:
			return render(request, '404.html', {'message': _('You can not remove what you do not have')}, status=400)

	if user.userprofile.authorization_key:
		user.userprofile.authorization_key = None
		user.userprofile.authorization_key_removed = timezone.now()
		user.userprofile.save()
	return redirect(reverse(index))


def web_restore (request):
	if request.user.is_authenticated():
		return render(request, '404.html', {'message': _('You should not restore what you have not lost yet')}, status=400)
	if (request.session.get('accounts_restore_requested')
	    and (datetime.datetime.now() - datetime.datetime.strptime(request.session.get('accounts_restore_requested'), TIME_FORMAT)).total_seconds() > ACCOUNT_RESTORATION_DELAY):
		request.session['accounts_restore_requested'] = None
		request.session['accounts_restore_message'] = None
	return render(request, 'accounts/restore.html', {
		'form1': restore_form,
		'form1_errors': request.session.get('accounts_restore_form_errors'),
		'form2': restore_code_form,
		'form2_errors': request.session.get('accounts_restore_form_errors'),
		'form3': restore_set_password_form,
		'form3_errors': request.session.get('accounts_restore_set_password_form_errors'),
		'message': request.session.get('accounts_restore_message'),
		'url_restore': reverse(restore_request),
		'restore_requested': request.session.get('accounts_restore_requested'),
		'restore_approved': request.session.get('accounts_restore_approved'),
	})


@require_POST
def restore_request (request):
	if request.session.get('accounts_restore_approved') and request.GET.get('step') != '3':
		return render(request, '404.html', {'message': _('You are doing it wrong')}, status=400)

	# step 1 is where the user requests the code
	if request.GET.get('step') == '1':
		form = restore_form(request.POST or None)
		if not form.is_valid():
			request.session['accounts_restore_form_errors'] = form.errors
		else:
			request.session['accounts_restore_form_errors'] = {}
			data = form.cleaned_data
			### this is unneeded because empty form is not valid so this code will never work
			# if not data['username'] and not data['email']:
			# 	request.session['accounts_restore_message'] = _('You should provide something')
			# else:
			user_id = utils.restore(data['username'], data['email'], request, ACCOUNT_RESTORATION_DELAY)['user_id']
			if user_id:
				request.session['accounts_restore_user_id'] = str(user_id)
			request.session['accounts_restore_message'] = _('We will look for the account and send a message with instructions on how to restore it')
			request.session['accounts_restore_requested'] = datetime.datetime.now().strftime(TIME_FORMAT)

	# step 2 is where te user submits back the code
	if request.GET.get('step') == '2':
		if not request.session.get('accounts_restore_requested'):
			return render(request, '403.html', {'message': _('Go huck yourself')}, status=403)
		if not (datetime.datetime.now() - datetime.timedelta(seconds=ACCOUNT_RESTORATION_DELAY)) < datetime.datetime.strptime(request.session.get('accounts_restore_requested'), TIME_FORMAT):
			return render(request, '404.html', {'message': _('It is too late')}, status=400)

		form = restore_code_form(request.POST or None)
		if not form.is_valid():
			request.session['accounts_restore_code_form_errors'] = form.errors
		else:
			request.session['accounts_restore_code_form_errors'] = {}
			data = form.cleaned_data

			if utils.veryfy_code(data['code'], request.session.get('accounts_restore_user_id'), ACCOUNT_RESTORATION_DELAY):
				request.session['accounts_restore_message'] = _('You have successfully proved your ownership of this account')
				request.session['accounts_restore_approved'] = datetime.datetime.now().strftime(TIME_FORMAT)

	# step 3 is where the user submith new password
	if request.GET.get('step') == '3':
		if not request.session.get('accounts_restore_approved'):
			return render(request, '403.html', {'message': _('Go huck yourself')}, status=403)
		if not (datetime.datetime.now() - datetime.timedelta(seconds=ACCOUNT_RESTORATION_DELAY)) < datetime.datetime.strptime(request.session.get('accounts_restore_approved'), TIME_FORMAT):
			return render(request, '404.html', {'message': _('It is too late')}, status=400)

		form = restore_set_password_form(request.POST or None)
		if not form.is_valid():
			request.session['accounts_restore_set_password_form_errors'] = form.errors
		else:
			request.session['accounts_restore_set_password_form_errors'] = {}
			data = form.cleaned_data
			if not data['password'] == data['password_confirm']:
				request.session['accounts_restore_message'] = _('Passwords mismatch')
			else:
				request.session['accounts_restore_message'] = None
				request.session['accounts_restore_approved'] = None
				request.session['accounts_restore_requested'] = None
				user = User.objects.get(id=request.session.get('accounts_restore_user_id'))
				user.set_password(data['password'])
				user.save()
				user = authenticate(username=user.username, password=data['password'])
				if user is not None and user.is_active:
					auth.login(request, user)
				return redirect(reverse(index))

	return redirect(web_restore)


def disabled (request):
	return render(request, '403.html', {'message': _('This application is disabled')}, status=403)
