# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
import datetime


class UserProfile (models.Model):
	user                                = models.OneToOneField(User, on_delete=models.CASCADE)
	validation_email_sent               = models.DateTimeField(blank=True, null=True)
	validation_email_code               = models.CharField(max_length=200, default=None, blank=True, null=True)
	validation_email_valid              = models.BooleanField(default=False)

	authorization_sha                   = models.CharField(max_length=41, default=None, blank=True, null=True)
	authorization_key                   = models.CharField(max_length=255, default=None, blank=True, null=True)
	authorization_key_removed           = models.DateTimeField(blank=True, null=True)

	settings_comments_default_depth     = models.PositiveSmallIntegerField(default=5)
	settings_comments_max_top_level_preload = models.PositiveSmallIntegerField(default=20)
	comments_next_notification_date     = models.DateTimeField(auto_now_add=True)
	settings_comments_delay_interval    = models.DurationField(default=datetime.timedelta(days=0))
	settings_comments_enable_notifying  = models.BooleanField(default=True)

	polls_tokens_last_generated         = models.DateTimeField(blank=True, null=True)
	mailer_tokens_last_generated        = models.DateTimeField(blank=True, null=True)

	restore_code                        = models.CharField(max_length=255, default=None, blank=True, null=True)
	restore_request_made                = models.DateTimeField(blank=True, null=True)

	def validation_email_valid_admin (self):
		return self.validation_email_valid

	validation_email_valid_admin.short_description = 'Valid'
	validation_email_valid_admin.boolean = True

	approved_by                         = models.IntegerField(blank=True, null=True, default=None)

	class Meta:
		permissions = (
			('access_accounts_management', 'User is authorized to access account management page'),
			('create_user_with_key', 'User is permitted to create accounts with auth keys'),
			('comments_approved', 'User is approved to add comments without premoderation'),
			('comments_moderate', 'User is permitted to moderate unapproved comments'),
			('comments_delete', 'User is permitted to delete comments'),
			('mailer_generate_tokens', 'User is permitted to generate mailer tokens'),
			('polls_generate_tokens', 'User is permitted to generate polls tokens'),
		)

	def __str__ (self):
		return self.user.username


@receiver(post_save, sender=User)
def create_user_profile (sender, instance, created, **kwargs):
	if created:
		UserProfile.objects.create(user=instance)
