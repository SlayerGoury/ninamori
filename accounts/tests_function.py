# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.auth.models import User
from django.test import TestCase, Client
from django.urls import reverse
from django.utils.crypto import get_random_string
from models import UserProfile
import views
import utils

c = Client()


class AccountsTests (TestCase):
	def test_api_register_can_register (self):
		"""
		api_register should create new account, create validation code and send an email
		"""
		self.assertEqual(
			utils.api_register('user', 'test@email.localhost', 'pass'),
			{'status': 'ok', 'result': 'user user created'}
		)

	def test_api_register_wont_register_existing_username (self):
		"""
		api_register should return an error if username is already taken
		"""
		utils.api_register('user', 'test@email.localhost', 'pass')
		self.assertEqual(
			utils.api_register('user', 'test@email.localhost', 'pass'),
			{'status': 'error', 'result': 'username exists'}
		)

	def test_api_register_wont_accept_bad_email (self):
		"""
		api_register should return an error if email is incorrect
		"""
		self.assertEqual(
			utils.api_register('user2', 'this@is@not@a.valid.email', 'pass'),
			{'status': 'error', 'result': 'invalid email'}
		)

	def test_api_register_userprofile (self):
		"""
		api_register should create UserProfile for user
		UserProfile should contain validation_email_code key and validation_email_valid should be false
		"""
		utils.api_register('user', 'test@email.localhost', 'pass')
		user = User.objects.get(username='user')
		self.assertEqual(len(user.userprofile.validation_email_code)>64, True)
		self.assertEqual(user.userprofile.validation_email_valid, False)

	def test_user_can_validate_email (self):
		"""
		User should be able to validate his email address
		"""
		utils.api_register('user', 'test@email.localhost', 'pass')
		user = User.objects.get(username='user')
		response = c.get(reverse('validate_email') + '?validation_email_code=' + user.userprofile.validation_email_code)
		user = User.objects.get(username='user')
		self.assertEqual(user.userprofile.validation_email_valid, True)

	def test_user_can_register_and_login (self):
		"""
		User should be able to log in after registering
		"""
		password = get_random_string(length=32)
		utils.api_register('user', 'test@email.localhost', password)
		response = c.login(username='user', password=password)
		self.assertEqual(response, True)
