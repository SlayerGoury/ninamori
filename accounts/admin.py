# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib import admin
from models import UserProfile


class UserProfileAdmin (admin.ModelAdmin):
	fieldsets = [
		(None,                {'fields': []}),
		('Email validation',  {'fields': ['validation_email_valid', 'validation_email_code', 'validation_email_sent'], 'classes': ['collapse']}),
		('Security codes',    {'fields': ['authorization_sha', 'authorization_key', 'authorization_key_removed', 'restore_code', 'restore_request_made'], 'classes': ['collapse']}),
		('Comments',          {'fields': ['settings_comments_delay_interval', 'settings_comments_enable_notifying', 'approved_by'], 'classes': ['collapse']}),
	]
	list_display = ('user', 'validation_email_valid_admin')


admin.site.register(UserProfile, UserProfileAdmin)
