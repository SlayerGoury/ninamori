# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django import forms
from django.conf import settings
from django.utils.translation import ugettext as _

error_messages = {
	'username': {
		'required': _('Username is required'),
	},
	'password': {
		'required': _('Password is required'),
	},
	'password_confirm': {
		'required': _('Confirmation is required'),
	},
	'email': {
		'required': _('Email address is required'),
		'invalid': _('Email address is invalid'),
	},
	'interval': {
		'invalid': _('delay interval value is invalid'),
		'max_value': _('interval value is too large (limit is %(limit_value)s)'),
		'min_value': _('interval value is too small (limit is %(limit_value)s)'),
	},
	'first_last_name': {
		'invalid': _('This is too long (limit is %(limit_value)s)'),
	},
	'code': {
		'requered': _('Code is requered'),
	},
	'comment_integers': {
		'invalid': _('Amount of comments should be Integer'),
		'requered': _('Amount of comments is requered'),
		'min_value': _('Should be positive'),
		'max_value': _('Limited to 100'),
	},
}


class registration_form (forms.Form):
	username                            = forms.CharField(error_messages=error_messages['username'],          widget=forms.TextInput( attrs={'onblur': 'username_checker();', 'required': True} ), required=True)
	password                            = forms.CharField(error_messages=error_messages['password'],          required=True, widget=forms.PasswordInput( attrs={'required': True} ))
	password_confirm                    = forms.CharField(error_messages=error_messages['password_confirm'],  required=True, widget=forms.PasswordInput( attrs={'required': True} ))
	email                               = forms.EmailField(error_messages=error_messages['email'],            required=True, widget=forms.TextInput( attrs={'required': True} ))

class admin_create_user_form (forms.Form):
	username                            = forms.CharField(error_messages=error_messages['username'],          required=True, widget=forms.TextInput( attrs={'required': True} ))
	group                               = forms.CharField(                                                    required=True)

class login_form (forms.Form):
	username                            = forms.CharField(error_messages=error_messages['username'],          required=True)
	password                            = forms.CharField(error_messages=error_messages['password'],          required=True, widget=forms.PasswordInput)

class manage_form (forms.Form):
	first_name                          = forms.CharField(error_messages=error_messages['first_last_name'],   widget=forms.TextInput( attrs={} ), required=False, max_length=30)
	last_name                           = forms.CharField(error_messages=error_messages['first_last_name'],   widget=forms.TextInput( attrs={} ), required=False, max_length=30)
	settings_comments_default_depth     = forms.IntegerField(error_messages=error_messages['comment_integers'], required=True, min_value=1, max_value=100)
	settings_comments_max_top_level_preload = forms.IntegerField(error_messages=error_messages['comment_integers'], required=True, min_value=1, max_value=100)
	settings_comments_delay_interval    = forms.IntegerField(error_messages=error_messages['interval'],       widget=forms.TextInput( attrs={} ), required=False, max_value=30, min_value=0)
	settings_comments_enable_notifying  = forms.BooleanField(required=False)
	password                            = forms.CharField(widget=forms.PasswordInput( attrs={} ), required=False)
	new_password                        = forms.CharField(widget=forms.PasswordInput( attrs={} ), required=False)
	new_password_confirm                = forms.CharField(widget=forms.PasswordInput( attrs={} ), required=False)

class restore_form (forms.Form):
	username                            = forms.CharField(widget=forms.TextInput( attrs={'class': 'smallinput'} ), required=False)
	email                               = forms.EmailField(error_messages=error_messages['email'],            widget=forms.TextInput( attrs={} ), required=False)

class restore_code_form (forms.Form):
	code                                = forms.CharField(error_messages=error_messages['code'],              widget=forms.TextInput( attrs={'class': 'p-input-2-3', 'required': True} ), required=True)

class restore_set_password_form(forms.Form):
	password                            = forms.CharField(error_messages=error_messages['password'],          required=True, widget=forms.PasswordInput( attrs={'required': True} ))
	password_confirm                    = forms.CharField(error_messages=error_messages['password_confirm'],  required=True, widget=forms.PasswordInput( attrs={'required': True} ))
