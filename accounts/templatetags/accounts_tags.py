from accounts import utils_gravatar
from django import template
from django.conf import settings
from os import path

register = template.Library()


@register.simple_tag
def get_avatar (email, size):
	email_hash = utils_gravatar.get_gravatar_hash(email)
	if path.exists(utils_gravatar.get_gravatar_file_path(email_hash, size)):
		avatar_url = settings.MEDIA_URL + 'avatars/gravatar_cache/' + email_hash + '_' + str(size) + '.png'
	else:
		# placeholder for not yet cached avatars
		avatar_url = '/static/img/anonymous.png'
	return avatar_url
