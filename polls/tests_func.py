# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.auth.models import User
from django.core.management import call_command
from django.urls import reverse
from django.test import TestCase, Client
from models import Poll, Option, Token
import json
import views
import utils

c = Client(enforce_csrf_checks=True)
VOTE_REVERSE = reverse(views.vote)
ACTIVATE_REVERSE = reverse(views.activate_token)

class PollTests (TestCase):
	def test_activate_token_and_vote (self):
		"""
		Can I activate token and vote?
		"""
		poll_1 = Poll.objects.create(
			title           = 'meow1',
			text            = 'meow1',
			token_required  = True,
		)
		option_1 = Option.objects.create(
			poll = poll_1,
			value = 'meow1',
		)
		poll_2 = Poll.objects.create(
			title           = 'meow2',
			text            = 'meow2',
		)
		option_2 = Option.objects.create(
			poll = poll_2,
			value = 'meow2',
		)
		token = Token.objects.create(purpose='test')
		response = c.get(VOTE_REVERSE)
		self.assertEqual(response.status_code, 405)
		response = c.get('/')
		response = c.post(VOTE_REVERSE, {
			'csrfmiddlewaretoken': c.cookies['csrftoken'].value,
			'uuid': str(poll_1.uuid).replace('-', ''),
			'option': option_1.id,
		})
		self.assertEqual(response.status_code, 403) # tokes is required but given none
		response = c.get(ACTIVATE_REVERSE + '?redirect_url=/')
		self.assertEqual(response.status_code, 400) # failed to request activation
		response = c.get(ACTIVATE_REVERSE + '?token='+str(token.uuid))
		self.assertEqual(response.status_code, 400) # failed to request activation
		response = c.get(ACTIVATE_REVERSE + '?token=meow&redirect_url=/')
		self.assertEqual(response.status_code, 403) # bad token
		response = c.get(ACTIVATE_REVERSE + '?token='+str(token.uuid)+'&redirect_url=/')
		self.assertEqual(response.status_code, 302) # token activated
		response = c.get('/')
		response = c.post(VOTE_REVERSE, {
			'csrfmiddlewaretoken': c.cookies['csrftoken'].value,
			'option': option_1.id,
		})
		self.assertEqual(response.status_code, 400) # missing uuid
		response = c.post(VOTE_REVERSE, {
			'csrfmiddlewaretoken': c.cookies['csrftoken'].value,
			'uuid': 'meow',
			'option': option_1.id,
		})
		self.assertEqual(response.status_code, 400) # bad uuid
		response = c.post(VOTE_REVERSE, {
			'csrfmiddlewaretoken': c.cookies['csrftoken'].value,
			'uuid': str(poll_1.uuid).replace('-', ''),
			'option': option_1.id,
		})
		self.assertEqual(response.status_code, 302) # successfully voted with token
		response = c.get('/')
		response = c.post(VOTE_REVERSE, {
			'csrfmiddlewaretoken': c.cookies['csrftoken'].value,
			'uuid': str(poll_1.uuid).replace('-', ''),
			'option': option_1.id,
		})
		self.assertEqual(response.status_code, 403) # token already used
		response = c.get('/')
		response = c.post(VOTE_REVERSE, {
			'csrfmiddlewaretoken': c.cookies['csrftoken'].value,
			'uuid': str(poll_2.uuid).replace('-', ''),
			'option': option_1.id,
		})
		self.assertEqual(response.status_code, 400) # option mismatch

		response = c.get('/')
		response = c.post(VOTE_REVERSE, {
			'csrfmiddlewaretoken': c.cookies['csrftoken'].value,
			'uuid': str(poll_2.uuid).replace('-', ''),
			'option': option_2.id,
		})
		self.assertEqual(response.status_code, 302) # successfully voted

		option_1 = Option.objects.get(value='meow1')
		option_2 = Option.objects.get(value='meow2')
		tokens_count = len(Token.objects.filter(purpose='test'))
		self.assertEqual(option_1.votes_counter, 1)
		self.assertEqual(option_2.votes_counter, 1)
		self.assertEqual(tokens_count, 0)

	def test_activate_5_tokens_and_vote_10_times (self):
		"""
		Can I activate multiple tokens and vote multiple times?
		"""
		poll_1 = Poll.objects.create(
			title           = 'meow1',
			text            = 'meow1',
			token_required  = True,
		)
		option_1 = Option.objects.create(
			poll = poll_1,
			value = 'meow1',
		)
		response = c.get(VOTE_REVERSE)
		token = Token.objects.create(purpose='test')
		response = c.get(ACTIVATE_REVERSE + '?token='+str(token.uuid)+'&redirect_url=/')
		token = Token.objects.create(purpose='test')
		response = c.get(ACTIVATE_REVERSE + '?token='+str(token.uuid)+'&redirect_url=/')
		token = Token.objects.create(purpose='test')
		response = c.get(ACTIVATE_REVERSE + '?token='+str(token.uuid)+'&redirect_url=/')
		self.assertEqual(len(json.loads(c.cookies['token'].value)), 3)

		response = c.post(VOTE_REVERSE, {
			'csrfmiddlewaretoken': c.cookies['csrftoken'].value,
			'uuid': str(poll_1.uuid).replace('-', ''),
			'option': option_1.id,
		})
		response = c.post(VOTE_REVERSE, {
			'csrfmiddlewaretoken': c.cookies['csrftoken'].value,
			'uuid': str(poll_1.uuid).replace('-', ''),
			'option': option_1.id,
		})
		response = c.post(VOTE_REVERSE, {
			'csrfmiddlewaretoken': c.cookies['csrftoken'].value,
			'uuid': str(poll_1.uuid).replace('-', ''),
			'option': option_1.id,
		})
		self.assertEqual(len(json.loads(c.cookies['token'].value)), 0) # last token used

		response = c.post(VOTE_REVERSE, {
			'csrfmiddlewaretoken': c.cookies['csrftoken'].value,
			'uuid': str(poll_1.uuid).replace('-', ''),
			'option': option_1.id,
		})
		self.assertEqual(response.status_code, 403) # no tokens left

		response = c.post(VOTE_REVERSE, {
			'csrfmiddlewaretoken': c.cookies['csrftoken'].value,
			'uuid': str(poll_1.uuid).replace('-', ''),
			'option': option_1.id,
		})
		self.assertEqual(response.status_code, 403) # no tokens left

		token = Token.objects.create(purpose='test')
		response = c.get(ACTIVATE_REVERSE + '?token='+str(token.uuid)+'&redirect_url=/')
		self.assertEqual(len(json.loads(c.cookies['token'].value)), 1) # one token
		response = c.post(VOTE_REVERSE, {
			'csrfmiddlewaretoken': c.cookies['csrftoken'].value,
			'uuid': str(poll_1.uuid).replace('-', ''),
			'option': option_1.id,
		})
		self.assertEqual(response.status_code, 302) # successfully voted with token
		response = c.post(VOTE_REVERSE, {
			'csrfmiddlewaretoken': c.cookies['csrftoken'].value,
			'uuid': str(poll_1.uuid).replace('-', ''),
			'option': option_1.id,
		})
		self.assertEqual(response.status_code, 403) # no tokens left

		token = Token.objects.create(purpose='test')
		response = c.get(ACTIVATE_REVERSE + '?token='+str(token.uuid)+'&redirect_url=/')
		response = c.get(ACTIVATE_REVERSE + '?token='+str(token.uuid)+'&redirect_url=/')
		response = c.get(ACTIVATE_REVERSE + '?token='+str(token.uuid)+'&redirect_url=/')
		response = c.get(ACTIVATE_REVERSE + '?token='+str(token.uuid)+'&redirect_url=/')
		response = c.get(ACTIVATE_REVERSE + '?token='+str(token.uuid)+'&redirect_url=/')
		response = c.get(ACTIVATE_REVERSE + '?token='+str(token.uuid)+'&redirect_url=/')
		response = c.get(ACTIVATE_REVERSE + '?token='+str(token.uuid)+'&redirect_url=/')
		self.assertEqual(len(json.loads(c.cookies['token'].value)), 1) # one token, no matter how many times you try to cativate it
		response = c.post(VOTE_REVERSE, {
			'csrfmiddlewaretoken': c.cookies['csrftoken'].value,
			'uuid': str(poll_1.uuid).replace('-', ''),
			'option': option_1.id,
		})
		self.assertEqual(response.status_code, 302) # successfully voted with token
		response = c.post(VOTE_REVERSE, {
			'csrfmiddlewaretoken': c.cookies['csrftoken'].value,
			'uuid': str(poll_1.uuid).replace('-', ''),
			'option': option_1.id,
		})
		self.assertEqual(response.status_code, 403) # no tokens left
		self.assertEqual(len(json.loads(c.cookies['token'].value)), 0) # all tokens used

	def test_suggestion (self):
		"""
		Can I suggest an option?
		"""
		poll = Poll.objects.create(
			title           = 'meow1',
			text            = 'meow1',
			token_required  = False,
			mode            = 'sg',
		)
		response = c.get('/')
		response = c.post(VOTE_REVERSE, {
			'csrfmiddlewaretoken': c.cookies['csrftoken'].value,
			'uuid': str(poll.uuid).replace('-', ''),
		})
		self.assertEqual(response.status_code, 400) # option is missing
		response = c.post(VOTE_REVERSE, {
			'csrfmiddlewaretoken': c.cookies['csrftoken'].value,
			'uuid': str(poll.uuid).replace('-', ''),
			'option': 'suggestion',
			'suggestion': 'meow purr',
		})
		self.assertEqual(response.status_code, 302)


	def test_utils (self):
		"""
		Is every util works?
		"""
		poll = Poll.objects.create(
			title           = 'meow',
			text            = 'meow',
			token_required  = 'True',
		)
		option = Option.objects.create(
			poll = poll,
			value = 'meow',
		)
		request = c.get('/')
		result1 = utils.assemble_poll_body(poll, '')
		result2 = utils.get_poll_results_html(str(poll.uuid).replace('-', ''))
		result3 = utils.token_url_for_email ('/')
		self.assertIn('<div class="p-controls">', result1)
		self.assertIn('<div class="votebar"', result2)
		self.assertIn(ACTIVATE_REVERSE + '?token=', result3)

	def test_token_links_generation (self):
		"""
		Does mass token generation works?
		"""
		call_command('generate_polls_tokens', '--token_count', '3', '--redirect', '/', '--supersilent')
		token_count = len(Token.objects.all())
		self.assertEqual(token_count, 3)
