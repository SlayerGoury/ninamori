# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.conf import settings
from django.urls import reverse
from django.middleware.csrf import get_token
from django.template.loader import render_to_string
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext as _
import json
import urllib
from ninamori.utils import uuid4hex
from models import Poll, Option, Token


def assemble_poll_body (poll, token):
	if not poll.is_open:
		if poll.show_results:
			return assemble_poll_options(poll, 'results')
		else:
			return '<p>' + _('Poll closed') + '</p>'

	if poll.token_required and (not token or token == '[]'):
		if poll.show_results:
			return assemble_poll_options(poll, 'results')
		else:
			return assemble_poll_options(poll, 'disabled')

	return assemble_poll_options(poll, 'enabled')


def assemble_poll_options (poll, mode):
	options = []
	for option in Option.objects.filter(poll=poll):
		options.append((
			str(option.id),
			option.value,
			str(option.votes_counter),
			option.votes_tokens,
		))
	return render_to_string('polls/form_body.html', {
		'options': options,
		'can_vote': (mode == 'enabled'),
		'show_results': (mode == 'results' or poll.show_results),
		'disabled': (mode == 'disabled'),
		'suggestions': (poll.mode == 'sg'),
	})


def get_poll_by_uuid (poll_uuid):
	if not uuid4hex.match(poll_uuid):
		return Poll(title='this is not uuid', is_open=False)
	if not Poll.objects.filter(uuid=poll_uuid):
		return Poll(title='wrong poll uuid', is_open=False)
	return Poll.objects.get(uuid=poll_uuid)


def get_poll_form_html (request, poll_uuid, back_url):
	poll = get_poll_by_uuid(poll_uuid)
	return render_to_string('polls/form.html', {
		'csrf_token': get_token(request),
		'poll_uuid': poll_uuid,
		'app_vote': reverse('polls_views_vote'),
		'back_url': back_url,
		'poll_title': poll.title,
		'poll_text': mark_safe(poll.text),
		'poll_body': mark_safe(assemble_poll_body(poll, request.COOKIES.get('token'))),
	})


def get_poll_results_html (poll_uuid):
	poll = get_poll_by_uuid(poll_uuid)
	return render_to_string('polls/form.html', {
		'results': True,
		'poll_title': poll.title,
		'poll_text': mark_safe(poll.text),
		'poll_body': mark_safe(assemble_poll_options(poll, 'results')),
	})


def token_url_for_email (back_url):
	token = Token.objects.create(purpose='email')
	result = settings.PROTOCOL_SCHEME + '://' + settings.SITE_DOMAIN + reverse('activate token') + '?token=' + str(token.uuid) + '&redirect_url=' + urllib.quote(back_url)
	return result


def get_tokens_from_cookies (cookies):
	result = []
	if cookies.get('token'):
		try:
			result = json.loads(cookies.get('token'))
		except ValueError, e:
			pass
	if type(result) != list: result = []
	return result


def count_cow_powers (request):
	cow_powers_tokens = get_tokens_from_cookies(request.COOKIES)
	cow_powers = len(cow_powers_tokens)
	return {'cow_powers': cow_powers, 'cow_powers_tokens': cow_powers_tokens}
