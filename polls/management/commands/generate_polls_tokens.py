from django.conf import settings
from django.core.files import File
from django.core.management.base import BaseCommand, CommandError
from django.urls import reverse
from django.utils.crypto import get_random_string
from accounts.utils import is_email_access_validated
from polls.models import Token
import polls.views
from ninamori.utils import mkdir, montage_qrcode, montage_qrlist
from mailqueue.models import MailerMessage
import urllib
from subprocess import call
from os import devnull

NULL = open(devnull, 'w')


class Command (BaseCommand):
	help = 'Generate tokens with bulk purpose. Use --nosave if you dont want to save tokens to database and just testing.'

	def add_arguments (self, parser):
		parser.add_argument('--token_count',
			dest='token_count',
			default='1',
			help='How many tokens you want to make')
		parser.add_argument('--redirect',
			dest='redirect',
			default='/',
			help='Where to redirect after activation')
		parser.add_argument('--nosave',
			action='store_true',
			dest='nosave',
			default=False,
			help='Do not save anything to database')
		parser.add_argument('--urls',
			action='store_true',
			dest='urls',
			default=False,
			help='Output just urls, nothing else')
		parser.add_argument('--qr',
			action='store_true',
			dest='qr',
			default=False,
			help='Generate QR coodes')
		parser.add_argument('--qr6',
			action='store_true',
			dest='qr6',
			default=False,
			help='QR codes will be grouped in lists by 6, looks good on A4')
		parser.add_argument('--qr24',
			action='store_true',
			dest='qr24',
			default=False,
			help='QR codes will be grouped in lists by 24, looks good on A2')
		parser.add_argument('--email',
			dest='email',
			default='',
			help='Codes will be sent to given email, if QR option is emabled, images will be sent too')
		parser.add_argument('--purpose',
			dest='purpose',
			default='bulk command',
			help='Purpose that will be written to database for administrative reasons')
		parser.add_argument('--supersilent',
			action='store_true',
			dest='supersilent',
			default=False,
			help='Do not output anything')

	def handle (self, *args, **options):
		if options['email'] and not is_email_access_validated(options['email']):
			if not options['supersilent']: self.stdout.write('Given email is not validated, no emails for you')
			return False

		token_count = int(options['token_count']) if options['token_count'].isdigit() else 1
		qr_dir = settings.QR_TEMP+'/'+get_random_string(48) if options['qr'] else False
		qr_per_page = 24 if options['qr24'] else 6 if options['qr6'] else False
		if qr_per_page and (token_count % qr_per_page):
			token_count += qr_per_page-(token_count % qr_per_page)

		if not options['urls']:
			if not options['supersilent']: self.stdout.write('Making ' + str(token_count) + ' tokens')

		qr_image = 1
		qr_page = 1 if qr_per_page else 0
		urls = []
		while token_count > 0:
			url = settings.PROTOCOL_SCHEME + '://' + settings.SITE_DOMAIN + reverse(polls.views.activate_token) + '?token=<< The Token >>&redirect_url=' + urllib.quote(options['redirect'])
			if not options['nosave']:
				token = Token.objects.create(purpose=options['purpose'])
				url = url.replace('<< The Token >>', str(token.uuid))
			else:
				url = url.replace('<< The Token >>', str(Token(purpose=options['purpose'])))
			if not options['supersilent']: self.stdout.write(' ' + url)
			urls.append(url)
			token_count -= 1

			if qr_dir:
				mkdir(qr_dir)
				montage_qrcode(qr_image, url, qr_dir)

				if qr_per_page and qr_image == qr_per_page:
					montage_qrlist(qr_page, qr_per_page, qr_dir)
					qr_image = 0
					qr_page += 1

				qr_image += 1

		if qr_per_page:
			for i in xrange(1, qr_per_page+1):
				call(['rm', qr_dir+'/qrcode_'+str(i)+'.png'], stdout=NULL)

		if options['email']:
			email_message = MailerMessage()
			email_message.subject       = 'Polls app cow power tokens'
			email_message.content       = '\n'.join(urls)
			email_message.from_address  = settings.DEFAULT_FROM_EMAIL
			email_message.to_address    = options['email']
			email_message.app           = 'polls tokens generation command'
			if qr_dir:
				if qr_per_page:
					for i in xrange(1, qr_page):
						email_message.add_attachment(File(open(qr_dir+'/qr_page_'+str(i)+'.png', 'r')))
				else:
					for i in xrange(1, qr_image):
						email_message.add_attachment(File(open(qr_dir+'/qrcode_'+str(i)+'.png', 'r')))
				call(['rm', '-r', qr_dir], stdout=NULL)
			email_message.save()

		if not options['urls']:
			if not options['supersilent']: self.stdout.write('All requested tokens generated')
