# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.auth.models import User, Permission
from django.contrib.sessions.middleware import SessionMiddleware
from django.http import HttpRequest
from django.test import TestCase
from django.shortcuts import redirect
from models import Poll, Option, Token
from accounts.utils import email_access_validate
import json
import views, utils


class PollTests (TestCase):
	def setUp (self):
		self.session = SessionMiddleware()

	def test_models_poll (self):
		poll = Poll.objects.create(title='meow')
		uuid = poll.uuid
		poll = Poll.objects.get(title='meow')
		self.assertEqual(uuid, poll.uuid)
		self.assertEqual(poll.title, 'meow')
		self.assertEqual(poll.__unicode__(), 'meow')

	def test_models_option (self):
		poll = Poll.objects.create(title='meow')
		option = Option.objects.create(poll=poll, value='meow')
		options = Option.objects.get(poll=poll)
		self.assertTrue(option == options or option in options)
		self.assertEqual(option.__unicode__(), 'meow | meow')

	def test_models_token (self):
		token = Token.objects.create()
		self.assertEqual(len(Token.objects.all()), 1)
		self.assertEqual(token.__unicode__(), str(token.uuid))

	def test_utils_assemble_poll_body (self):
		poll = Poll.objects.create(title='meow')
		token = Token.objects.create()
		self.assertTrue(utils.assemble_poll_body(poll, token))
		poll = Poll.objects.create(title='meow', is_open=False, show_results=False)
		self.assertTrue(utils.assemble_poll_body(poll, token))
		poll = Poll.objects.create(title='meow', is_open=False)
		self.assertTrue(utils.assemble_poll_body(poll, token))
		poll = Poll.objects.create(title='meow', show_results=False)
		self.assertTrue(utils.assemble_poll_body(poll, token))
		poll = Poll.objects.create(title='meow', token_required=True, show_results=False)
		self.assertTrue(utils.assemble_poll_body(poll, '[]'))


	def test_utils_assemble_poll_options (self):
		poll = Poll.objects.create(title='meow')
		self.assertTrue(utils.assemble_poll_options(poll, 'enabled'))
		self.assertTrue(utils.assemble_poll_options(poll, 'disabled'))
		self.assertTrue(utils.assemble_poll_options(poll, 'results'))

	def test_utils_get_poll_by_uuid (self):
		poll = Poll.objects.create(title='meow')
		uuid = str(poll.uuid).replace('-', '')
		self.assertTrue(utils.get_poll_by_uuid(uuid))
		poll = Poll.objects.get(title='meow')
		poll.delete()
		self.assertIn('wrong poll uuid', utils.get_poll_by_uuid(uuid).title)

	def test_utils_get_poll_form_html (self):
		poll = Poll.objects.create(title='meow')
		self.assertTrue(utils.get_poll_form_html(HttpRequest(), str(poll.uuid), '/'))

	def test_utils_get_poll_results_html (self):
		poll = Poll.objects.create(title='meow')
		self.assertTrue(utils.get_poll_results_html(str(poll.uuid)))

	def test_utils_token_url_for_email (self):
		self.assertTrue(utils.token_url_for_email('/'))

	def test_utils_get_tokens_from_cookies (self):
		cookies = {'token': json.dumps(['meow', 'meowmeow'])}
		self.assertEqual(utils.get_tokens_from_cookies(cookies), ['meow', 'meowmeow'])
		cookies = {'token': 'purr'}
		self.assertEqual(utils.get_tokens_from_cookies(cookies), [])


	def test_utils_count_cow_powers (self):
		request = HttpRequest()
		request.COOKIES = {'token': json.dumps(['meow', 'meowmeow'])}
		cow_powers = utils.count_cow_powers(request)
		self.assertEqual(cow_powers['cow_powers'], 2)
		self.assertEqual(cow_powers['cow_powers_tokens'], ['meow', 'meowmeow'])

	def test_views_select_token (self):
		token1 = Token.objects.create()
		token1.save
		token2 = Token.objects.create()
		token2.save
		cookies = {'token': json.dumps([str(token1.uuid).replace('-', ''), str(token2.uuid).replace('-', '')])}
		self.assertEqual(views.select_token(cookies), {'leftovers': [unicode(str(token1.uuid).replace('-', '')), unicode(str(token2.uuid).replace('-', ''))], 'token': unicode(str(token2.uuid).replace('-', ''))})

	def test_views_remove_token (self):
		token1 = Token.objects.create()
		token1.save
		token2 = Token.objects.create()
		token2.save
		token = {'leftovers': [unicode(str(token1.uuid).replace('-', '')), unicode(str(token2.uuid).replace('-', ''))], 'token': unicode(str(token2.uuid).replace('-', ''))}
		response = redirect('/')
		response = views.remove_token(response, token)
		self.assertTrue(str(token1.uuid).replace('-', '') in response.cookies.get('token').value)

	def test_views_generate_tokens (self):
		request = HttpRequest()
		request.user = User.objects.create(email='meow@meow.meow')
		request.LANGUAGE_CODE = 'en'
		self.session.process_request(request)
		response = views.generate_tokens(request)
		self.assertEqual(response.status_code, 405)  # POST method required
		request.method = 'POST'
		response = views.generate_tokens(request)
		self.assertEqual(response.status_code, 403)  # user do not have permission
		request.user.user_permissions.add(Permission.objects.get(codename='polls_generate_tokens'))
		request.user = User.objects.get(email='meow@meow.meow')
		response = views.generate_tokens(request)
		self.assertEqual(response.status_code, 400)  # Incorrect token count
		request.POST = {'token_count': '1'}
		response = views.generate_tokens(request)
		self.assertEqual(response.status_code, 403)  # should validate email
		email_access_validate('meow@meow.meow')
		request.user.userprofile.refresh_from_db()
		response = views.generate_tokens(request)
		self.assertEqual(response.status_code, 400)  # Redirect is missing
		request.POST = {'token_count': '1', 'redirect': '/', 'qr': '-1'}
		response = views.generate_tokens(request)
		self.assertEqual(response.status_code, 400)  # Incorrect qr per page value
		request.POST = {'token_count': '1', 'redirect': '/', 'qr': '24'}
		response = views.generate_tokens(request)
		self.assertEqual(response.status_code, 302)  # generated tokens
		response = views.generate_tokens(request)
		self.assertEqual(response.status_code, 302)  # Try again later
