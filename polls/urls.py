from django.conf.urls import include, url
import views


urlpatterns = [
	url(r'^$', views.vote, name='polls_views_vote'),
	url(r'activate_token$', views.activate_token, name='activate token'),
	url(r'generate_tokens$', views.generate_tokens, name='polls_generate_tokens'),
]
