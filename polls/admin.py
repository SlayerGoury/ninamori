from django.contrib import admin
from django.http import HttpResponseRedirect
from models import Poll, Option, Token


class OptionInline (admin.TabularInline):
	model = Option
	extra = 5


class PollAdmin (admin.ModelAdmin):
	def changelist_view(self, request, extra_context=None):
		if not request.META['QUERY_STRING'] and \
		   not request.META.get('HTTP_REFERER', '').startswith(request.build_absolute_uri()):
			return HttpResponseRedirect(request.path + "?is_open__exact=1")
		return super(PollAdmin,self).changelist_view(request, extra_context=extra_context)
	list_display = ('title', 'uuid', 'mode', 'date_created', 'is_open', 'show_results', 'token_required')
	list_editable = ('is_open', 'show_results')
	fields = ['title', 'mode', 'text', 'is_open', 'show_results', 'token_required']
	list_filter = (
		('is_open', admin.BooleanFieldListFilter),
		('token_required', admin.BooleanFieldListFilter),
	)
	inlines = [OptionInline]
	ordering = ['-date_created']


class TokenAdmin (admin.ModelAdmin):
	readonly_fields = ('uuid', 'purpose')
	list_display = ('uuid', 'purpose')
	list_display_links = None
	search_fields = ['purpose']


admin.site.register(Poll, PollAdmin)
admin.site.register(Token, TokenAdmin)
